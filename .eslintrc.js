module.exports = {
  plugins: [
    "react-hooks"
  ],
  parser: 'babel-eslint',
  extends: [
    '@comocapital',
  ],
  env: {
    browser: true,
    node: true,
  },
  settings: {
    'import/resolver': 'webpack',
  },
  rules: {
    'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
    'react/prefer-stateless-function': ['error', {'ignorePureComponents': true}],
    'react/forbid-prop-types': 'off',
    'no-underscore-dangle': 'off',
    'valid-jsdoc': 'off',
    'require-jsdoc': 'off',
    'react/no-multi-comp': 'off',
    'no-return-assign': 'off',
    'no-use-before-define': 'off',
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "jsx-a11y/click-events-have-key-events": "off",
    "no-nested-ternary":"off",
    "import/prefer-default-export":"off",
    "jsx-a11y/no-static-element-interactions": "off"
  },
  overrides: [
    {
      files: [ 'src/**' ],
      rules: {
        'unicorn/filename-case': 'off',
        'no-return-assign': 'off',
        'react/jsx-wrap-multilines': 'off',
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "jsx-a11y/click-events-have-key-events": "off",
        "no-nested-ternary":"off",
        "import/prefer-default-export":"off",
        "jsx-a11y/no-static-element-interactions": "off"
      }
    }
  ]
}