## Установка

* Установить модули

```sh
npm i
```

* Запустить проект

```sh
npm start
```

По умолчанию frontend смотрит на локальный backend (как установить и настроить backend смотри README соответствующего репозитория)


Если нужно настроить frontend на удаленный backend:

```sh
В файле ./package.json

Замени  <localhost:8000> и <localhost:3000>  в строке: "start": "BACKEND_URL=http://localhost:8000 WEBSOCKET_URL=http://localhost:3000 webpack-dev-server" на  нужные адреса.

```
