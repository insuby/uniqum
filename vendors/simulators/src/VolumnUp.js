import React, { useState, useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faVolumeOff,
  faVolumeUp,
  faVolumeDown,
} from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';

library.add(faVolumeOff, faVolumeUp, faVolumeDown);

const VolumnUp = () => {
  const [icon, setIcon] = useState('volume-off');

  useEffect(() => {
    let timeoutUpID;
    let timeoutDownID;

    const id = setInterval(() => {
      setIcon('volume-off');
      timeoutDownID = setTimeout(() => {
        setIcon('volume-down');
      }, 333);
      timeoutUpID = setTimeout(() => {
        setIcon('volume-up');
      }, 667);
    }, 1000);
    return () => {
      clearTimeout(timeoutUpID);
      clearTimeout(timeoutDownID);
      clearInterval(id);
    };
  }, []);

  return (
    <FontAwesomeIcon
      icon={icon}
      color={'white'}
      style={{
        fontSize: '10vw',
        position: 'absolute',
      }}
    />
  );
};

export default VolumnUp;
