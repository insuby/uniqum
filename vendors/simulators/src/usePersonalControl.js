import { useContext } from 'react';
import { PersonalPanelContext, types } from './PersonalPanelContext';

const usePersonalControl = () => {
  const [state, dispatch] = useContext(PersonalPanelContext);
  const { controls } = state;

  function start() {
    dispatch({
      type: types.ON_START,
    });
  }

  function stop() {
    state.onStop();
    // dispatch({
    //   type: types.ON_STOP,
    // });
  }

  function increment(key) {
    dispatch({
      type: types.ON_INCREMENT,
      meta: key,
    });
  }

  function change(key, value) {
    dispatch({
      type: types.ON_CHANGE,
      meta: key,
      payload: value,
    });
  }

  function purse() {
    dispatch({
      type: types.ON_PURSE,
    });
  }

  function check() {
    dispatch({
      type: types.ON_CHECK,
    });
  }

  function begin() {
    dispatch({
      type: types.ON_BEGIN,
    });
  }

  function progress() {
    dispatch({
      type: types.ON_PROGRESS,
    });
  }

  function end() {
    dispatch({
      type: types.ON_END,
    });
  }

  function toggleControls(name) {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: { ...controls, [name]: !controls[name] },
    });
  }

  function patchControls(name, payload) {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: { ...controls, [name]: payload },
    });
  }

  function putControls(payload) {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload,
    });
  }

  function onDictationStart() {
    dispatch({
      type: types.ON_DICTATION_START,
      // meta: 'controls',
      // payload,
    });
  }

  return {
    toggleControls,
    putControls,
    patchControls,
    onDictationStart,
    start,
    stop,
    increment,
    change,
    purse,
    state,
    check,
    begin,
    end,
    progress,
  };
};

export default usePersonalControl;
