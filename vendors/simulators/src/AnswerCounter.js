import React from 'react';
import PropTypes from 'prop-types';
// import {
//   faCog,
//   faThumbsUp,
//   faThumbsDown,
// faPlayCircle,
// faPauseCircle,
//   faSpinner,
//   faTimesCircle,
// } from '@fortawesome/free-solid-svg-icons';
// import Grid from '@material-ui/core/Grid';
// import IconButton from '@material-ui/core/IconButton';

// import Badge from '@material-ui/core/Badge';
// import { withStyles } from '@material-ui/core/styles';
// import Button from '@material-ui/core/Button';
// import MenuItem from '@material-ui/core/MenuItem';
// import Menu from '@material-ui/core/Menu';
// import Checkbox from '@material-ui/core/Checkbox';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
import usePersonalControl from './usePersonalControl';

// const StyledBadge = withStyles(theme => ({
//   badge: {
//     right: -5,
//     border: `2px solid ${theme.palette.grey[200]}`,
//   },
// }))(Badge);

// const Play = () => {
//   const {
//     start,
//     onDictationStart,
//     stop,
//     state: {
//       controls,
//       iconmin,
//       // onStop,
//     },
//     render,
//   } = usePersonalControl();

//   const handleStart = () => {
//     start();
//     onDictationStart();
//   };

//   const handleStop = () => {
//     stop();
//   };

//   let component = (
//     <Button
//       color="primary"
//       variant="contained"
//       onClick={handleStart}
//       startIcon={<FontAwesomeIcon icon={faPlayCircle} />}
//     >
//       {!iconmin && 'Старт'}
//     </Button>
//   );

//   if (controls.start) {
//     component = (
//       <Button
//         color="primary"
//         variant="contained"
//         onClick={handleStop}
//         startIcon={<FontAwesomeIcon icon={faPauseCircle} />}
//       >
//         {!iconmin && 'Стоп'}
//       </Button>
//     );
//   }

//   return component;
// };

const AnswerCounter = ({
  visible,
  // onShowParamsBlock,
  // onToggleSetting,
  // room,
  render,
}) => {
  const { state, start, onDictationStart, stop } = usePersonalControl();

  // const {
  // request,
  // controls: { sound, success, failure, open, abacus, showAnswers, sample },
  // iconmin,
  // } = state;

  // const [AnchorEl, setAnchorEl] = useState(null);

  // const handleMenu = event => {
  //   setAnchorEl(event.currentTarget);
  // };

  // const handleMenuClose = () => {
  //   setAnchorEl(null);
  // };

  // const handleParamsBlock = () => {
  //   onShowParamsBlock();
  // };

  // const openEl = Boolean(AnchorEl);

  return (
    <>
      {render({
        state,
        visible,
        start,
        onDictationStart,
        stop,
      })}
    </>
  );
};

AnswerCounter.propTypes = {
  visible: PropTypes.object,
  render: PropTypes.func.isRequired,
  // room: PropTypes.string.isRequired,
  // onShowParamsBlock: PropTypes.func.isRequired,
  // onToggleSetting: PropTypes.func.isRequired,
};

AnswerCounter.defaultProps = {
  visible: {
    play: true,
    params: true,
    sample: false,
    settings: true,
    sound: true,
  },
};

export default AnswerCounter;
// <Grid
//   container
//   item
//   xs={12}
//   lg={12}
//   sm={12}
//   md={12}
//   style={{ padding: '10px' }}
// >
//   <Grid item xs={6} lg={3} sm={3} md={3}>
//     {visible.play && (
//       <>
//         {request && request.loading ? (
//           <Button
//             color="primary"
//             variant="contained"
//             disabled
//             endIcon={<FontAwesomeIcon spin icon={faSpinner} />}
//           >
//             {!iconmin && 'Загрузка'}
//           </Button>
//         ) : (
//           <Play />
//         )}
//       </>
//     )}
//   </Grid>
//   <Grid item xs={2} lg={3} sm={3} md={3}>
//     <IconButton>
//       <StyledBadge max={999} badgeContent={success} color="primary">
//         <FontAwesomeIcon icon={faThumbsUp} color="green" />
//       </StyledBadge>
//     </IconButton>
//   </Grid>
//   <Grid item xs={2} lg={3} sm={3} md={3}>
//     <IconButton>
//       <StyledBadge max={999} badgeContent={failure} color="primary">
//         <FontAwesomeIcon icon={faThumbsDown} color="red" />
//       </StyledBadge>
//     </IconButton>
//   </Grid>
//   <Grid item xs={2} lg={3} sm={3} md={3}>
//     <>
//       <IconButton
//         aria-owns={openEl ? 'menu-appbar' : null}
//         aria-haspopup="true"
//         color="inherit"
//         onClick={handleMenu}
//       >
//         <FontAwesomeIcon icon={faCog} />
//       </IconButton>
//       <Menu
//         id="menu-appbar"
//         anchorEl={AnchorEl}
//         anchorOrigin={{
//           vertical: 'bottom',
//           horizontal: 'right',
//         }}
//         getContentAnchorEl={null}
//         transformOrigin={{
//           vertical: 'top',
//           horizontal: 'left',
//         }}
//         open={openEl}
//         onClose={handleMenuClose}
//       >
//         <MenuItem key={0}>
//           <IconButton onClick={handleMenuClose}>
//             <FontAwesomeIcon icon={faTimesCircle} />
//           </IconButton>
//         </MenuItem>
//         {visible.settings && (
//           <MenuItem key={1}>
//             <FormControlLabel
//               value="end"
//               control={
//                 <Checkbox
//                   color="primary"
//                   checked={open}
//                   onChange={handleParamsBlock}
//                 />
//               }
//               label="Параметры"
//               labelPlacement="end"
//             />
//           </MenuItem>
//         )}
//         {visible.sample && (
//           <MenuItem key={2}>
//             <FormControlLabel
//               value="end"
//               control={
//                 <Checkbox
//                   color="primary"
//                   checked={sample}
//                   onChange={event => onToggleSetting(event, 'sample')}
//                 />
//               }
//               label="Показать пример(ы)"
//               labelPlacement="end"
//             />
//           </MenuItem>
//         )}
//         {visible.showAnswers && (
//           <MenuItem key={3}>
//             <FormControlLabel
//               value="end"
//               control={
//                 <Checkbox
//                   color="primary"
//                   checked={showAnswers}
//                   onChange={event => onToggleSetting(event, 'showAnswers')}
//                 />
//               }
//               label="Показать ответ(ы)"
//               labelPlacement="end"
//             />
//           </MenuItem>
//         )}
//         {visible.abacus && (
//           <MenuItem key={4}>
//             <FormControlLabel
//               value="end"
//               control={
//                 <Checkbox
//                   color="primary"
//                   checked={abacus}
//                   onChange={event => onToggleSetting(event, 'abacus')}
//                 />
//               }
//               label="Виртуальный соробан"
//               labelPlacement="end"
//             />
//           </MenuItem>
//         )}
//         {room === 'roomA' && visible.sound && (
//           <MenuItem key={5}>
//             <FormControlLabel
//               value="end"
//               control={
//                 <Checkbox
//                   color="primary"
//                   checked={sound}
//                   onChange={event => onToggleSetting(event, 'sound')}
//                 />
//               }
//               label="Звук"
//               labelPlacement="end"
//             />
//           </MenuItem>
//         )}
//       </Menu>
//     </>
//   </Grid>
// </Grid>
