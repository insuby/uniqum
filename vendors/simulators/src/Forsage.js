import React, { useContext, Fragment, useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { interval as rxInterval, concat } from 'rxjs';
import {
  takeWhile,
  takeUntil,
  scan,
  startWith,
  tap,
  switchMap,
  delay,
  endWith,
  filter,
} from 'rxjs/operators';
import { useEventCallback } from 'rxjs-hooks';
import { PersonalPanelContext } from './PersonalPanelContext';
import usePersonalControl from './usePersonalControl';

import { SOUND_URL } from './utils/const';

function tmrEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'timer'),
    tap(action => action.deps.progress()),
    switchMap(action =>
      concat(
        rxInterval(1000 * Number(action.payload.interval)).pipe(
          startWith(0),
          tap(a => console.log(a)),
          tap(() => action.deps.setColor('white')),
          delay(10),
          tap(() => action.deps.setColor('black')),
          scan(time => time + 1),
          tap(
            idx =>
              action.payload.audios[idx] && action.payload.audios[idx].play()
          ),

          takeWhile(time => time < action.payload.length),
          endWith(() => action.deps.end()),
          takeUntil(events$.pipe(filter(e => e.type === 'stop')))
        )
      )
    )
  );
}

function onceEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'once'),
    tap(action => action.deps.begin())
  );
}

// https://codesandbox.io/s/epic-ardinghelli-6okrp
// true https://codesandbox.io/s/objective-pare-69o6w

const Forsage = () => {
  // TODO упростить компонент
  const [state] = useContext(PersonalPanelContext);
  const {
    params: { interval },
    args,
    request,
    workout,
    controls: { start, check, value, sound, stop },
  } = state;

  const { begin, progress, end } = usePersonalControl();

  const [color, setColor] = useState('black');
  const [audios, setAudios] = useState([]);

  useEffect(() => {
    setColor('black');
    setAudios([]);
  }, [args]);

  const sum = args.reduce((acc, next) => acc + next, 0);
  const size = Math.round(30 / String(sum).length);

  const [onEvent, index] = useEventCallback(tmrEpic);

  useEffect(() => {
    if (check) {
      setColor('green');
      if (Number(value) !== sum) {
        setColor('red');
      }
    }
  }, [check, value, sum]);

  const [onOnce] = useEventCallback(onceEpic);

  useEffect(() => {
    if (request.success && args.length > 0 && start) {
      onOnce({
        type: 'once',
        payload: { workout },
        deps: { begin },
      });
    }
  }, [args.length, start, request.success]);

  useEffect(() => {
    if (workout.begin) {
      setAudios([]);
      onEvent({
        type: 'timer',
        payload: { length: args.length, interval, audios },
        deps: { progress, end, setColor },
      });
    }
  }, [args.length, audios, interval, onEvent, workout.begin]);

  useEffect(() => {
    if (sound) {
      // TODO вынести отсюда в более подходящее место
      const audiosList = [];
      const numbers = args.map(i => (i < 0 ? i : `${i}`));
      for (let i = 0; i < numbers.length; i += 1) {
        const audio = new Audio();
        audio.src = `${SOUND_URL}/assets/sounds/numbers/${numbers[i]}.ogg`;
        audio.preload = 'auto';
        audio.playbackRate = Number(interval) < 1.5 ? 2.0 : 1.0;
        audiosList.push(audio);
      }
      setAudios(audiosList);
    }
  }, [args, interval, sound]);

  useEffect(() => {
    if (stop) {
      onEvent({ type: 'stop' });
    }
  }, [onEvent, stop]);

  return (
    <Grid item xs={12} lg={12} sm={12} md={12} style={{ minHeight: `25vh` }}>
      <div style={{ color, fontSize: `calc(4vw + 4vh + 2vmin)` }}>
        {check ? (
          <div style={{ color, fontSize: `${size}vh` }}>{sum}</div>
        ) : (
          <Fragment>
            {workout.progress ? (
              <div>{args[index]}</div>
            ) : (
              <>{workout.end && <>?</>}</>
            )}
          </Fragment>
        )}
      </div>
    </Grid>
  );
};

export default Forsage;
