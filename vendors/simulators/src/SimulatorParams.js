import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ModeField from './simulatorparams/ModeField';
import NumbersField from './simulatorparams/NumbersField';
import BoundField from './simulatorparams/BoundField';
import SignField from './simulatorparams/SignField';
import RangeField from './simulatorparams/RangeField';
import SectionField from './simulatorparams/SectionField';
import WrapTextField from './simulatorparams/WrapTextField';
import { xs, sm, md, lg, coefficient } from './utils/const';
// import { PersonalPanelContext } from './PersonalPanelContext';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    borderRadius: 0,
    marginTop: '10px',
    boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
  },
  switch: {
    textAlign: 'right',
  },
  button: {
    width: '100%',
  },
  right: {
    float: 'right',
  },
}));

const SimulatorParams = ({
  room,
  open,
  onChangeParams,
  params,
  fullSize,
  showIconClose,
  onShowParamsBlock,
}) => {
  const classes = useStyles();

  const handleChange = (event, prop) => {
    const newParams = { ...params };

    onChangeParams({ ...newParams, [prop]: event.target.value });
  };

  const handleDrawerClose = () => {
    onShowParamsBlock();
    console.log('handleDrawerClose');
  };

  return (
    <>
      {params && open && (
        <Paper className={classes.paper}>
          <Grid
            container
            justify="center"
            alignItems="baseline"
            style={{ textAlign: 'center' }}
          >
            {showIconClose && (
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <IconButton
                  onClick={handleDrawerClose}
                  className={classes.right}
                >
                  <FontAwesomeIcon icon={faTimesCircle} />
                </IconButton>
              </Grid>
            )}
            <ModeField
              params={params}
              onChangeParams={onChangeParams}
              fullSize={fullSize}
            />
            {+params.mode === 1 && (
              <SectionField
                params={params}
                onChangeParams={onChangeParams}
                fullSize={fullSize}
              />
            )}
            {params.mode !== 1 && (
              <SignField
                params={params}
                onChangeParams={onChangeParams}
                fullSize={fullSize}
              />
            )}
            {params.mode !== 1 && (
              <NumbersField
                params={params}
                onChangeParams={onChangeParams}
                fullSize={fullSize}
              />
            )}
            {room !== 'roomB' && (
              <WrapTextField
                key={1}
                onValueChange={event => handleChange(event, 'interval')}
                params={params}
                prop="interval"
                label
                row={{
                  xs: fullSize ? 12 : Math.ceil(xs * coefficient.interval.xs),
                  sm: fullSize ? 12 : Math.ceil(sm * coefficient.interval.sm),
                  md: fullSize ? 12 : Math.ceil(md * coefficient.interval.md),
                  lg: fullSize ? 12 : Math.ceil(lg * coefficient.interval.lg),
                }}
              />
            )}
            <WrapTextField
              key={2}
              onValueChange={event => handleChange(event, 'steps')}
              params={params}
              prop="steps"
              label
              row={{
                xs: fullSize ? 12 : Math.ceil(xs * coefficient.steps.xs),
                sm: fullSize ? 12 : Math.ceil(sm * coefficient.steps.sm),
                md: fullSize ? 12 : Math.ceil(md * coefficient.steps.md),
                lg: fullSize ? 12 : Math.ceil(lg * coefficient.steps.lg),
              }}
            />

            <RangeField
              params={params}
              onChangeParams={onChangeParams}
              fullSize={fullSize}
            />
            {(params.mode === 3 || params.mode === 4) && (
              <BoundField
                params={params}
                onChangeParams={onChangeParams}
                fullSize={fullSize}
              />
            )}
          </Grid>
        </Paper>
      )}
    </>
  );
};
SimulatorParams.defaultProps = {
  fullSize: false,
  showIconClose: true,
};

SimulatorParams.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  room: PropTypes.string.isRequired,
  params: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  fullSize: PropTypes.bool,
  showIconClose: PropTypes.bool,
  onShowParamsBlock: PropTypes.func.isRequired,
};

export default SimulatorParams;
