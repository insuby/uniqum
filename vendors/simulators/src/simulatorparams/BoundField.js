import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { xs, sm, md, lg, coefficient } from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function BoundField({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    onChangeParams({ ...params, bound: event.target.value });
  };

  return (
    <Grid
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.bound.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.bound.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.bound.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.bound.lg)}
      key={-10}
    >
      <FormControl className={classes.formControlForSelect}>
        <InputLabel className={classes.inputLabel}>
          <Tooltip
            className={classes.root}
            title={
              <>
                <p style={{ width: '250px' }}>
                  <em>
                    `Без перехода` - в этом случае сумма примера не будет
                    превышать 50
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `За 50` - в этом случае сумма примера не будет превышать 100
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `За 100` - в этом случае сумма примера может превышать 100
                  </em>
                </p>
              </>
            }
          >
            <IconButton aria-label="Активировать">
              <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
            </IconButton>
          </Tooltip>
          Переход
        </InputLabel>
        <Select
          value={params.bound}
          inputProps={{
            name: 'bound',
          }}
          classes={{ root: classes.select }}
          onChange={handleChange}
        >
          <MenuItem value={0}>Без перехода</MenuItem>
          <MenuItem value={1}>за 50</MenuItem>
          <MenuItem value={2}>за 100</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  );
}

BoundField.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
