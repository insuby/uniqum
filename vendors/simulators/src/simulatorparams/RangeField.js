import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { xs, sm, md, lg, coefficient } from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function RangeField({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    onChangeParams({ ...params, [event.target.name]: event.target.value });
  };

  return (
    <Grid
      container
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.under.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.under.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.under.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.under.lg)}
      key={-11}
      justify="center"
      alignItems="center"
    >
      <Grid item xs={12} sm={12} md={12} lg={12} key={-20}>
        <Tooltip
          placement="right"
          className={classes.root}
          classes={{ tooltip: classes.lightTooltip }}
          title={
            <Fragment>
              <p>
                <em>
                  `Если выбрано от 1 до 9` - числа будут в интервале от 1 до 9
                </em>
              </p>
              <p>
                <em>
                  `Если выбрано от 1 до 99` - числа будут в интервале от 1 до 99
                </em>
              </p>
              <p>
                <em>
                  `Если выбрано от 1 до 999` - числа будут в интервале от 1 до
                  999
                </em>
              </p>
              <p>
                <em>
                  `Если выбрано от 10 до 99` - числа будут в интервале от 10 до
                  99
                </em>
              </p>
              <p>
                <em>
                  `Если выбрано от 10 до 999` - числа будут в интервале от 10 до
                  999
                </em>
              </p>
              <p>
                <em>
                  `Если выбрано от 100 до 999 - числа будут в интервале от 100
                  до 999
                </em>
              </p>
            </Fragment>
          }
        >
          <IconButton aria-label="Активировать" style={{ fontSize: '0.8rem' }}>
            <FontAwesomeIcon icon={faQuestionCircle} size="1x" />
          </IconButton>
        </Tooltip>
        <span style={{ fontSize: '13px' }}>Разряд числа</span>
      </Grid>
      <Grid
        item
        xs={4}
        sm={4}
        md={4}
        lg={4}
        key={-21}
        className={classes.right}
      >
        <FormControl className={classes.formControlForSelect}>
          <InputLabel>от</InputLabel>
          <Select
            value={params.under}
            classes={{ root: classes.input }}
            inputProps={{
              name: 'under',
            }}
            onChange={handleChange}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>10</MenuItem>
            <MenuItem value={3}>100</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={4} sm={4} md={4} lg={4} key={-22} className={classes.left}>
        <FormControl className={classes.formControlForSelect}>
          <InputLabel>до</InputLabel>
          <Select
            value={params.over}
            classes={{ root: classes.input }}
            inputProps={{
              name: 'over',
            }}
            onChange={handleChange}
          >
            <MenuItem value={1}>9</MenuItem>
            <MenuItem value={2}>99</MenuItem>
            <MenuItem value={3}>999</MenuItem>
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
}

RangeField.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
