import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { xs, sm, md, lg, coefficient } from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function SignField({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    onChangeParams({ ...params, sign: event.target.value });
  };

  return (
    <Grid
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.sign.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.sign.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.sign.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.sign.lg)}
      key={-8}
    >
      <FormControl className={classes.formControlForSelect}>
        <InputLabel>
          <Tooltip
            className={classes.root}
            placement="right"
            classes={{ tooltip: classes.lightTooltip }}
            title={
              <>
                <p>
                  <em>Примеры формул на знак `+`:</em>
                </p>
                <ul>
                  <li>Правило `МТ`:</li>
                  <li>4 = 5 - 1</li>
                </ul>
                <ul>
                  <li>Правило `СТ`:</li>
                  <li>6 = 10 - 4</li>
                </ul>
                <ul>
                  <li>Правило `MIX`:</li>
                  <li>8 = 10 - 5 + 3</li>
                </ul>
                <p>
                  <em>Примеры формул на знак `-`:</em>
                </p>
                <ul>
                  <li>Правило `МТ`:</li>
                  <li>-4 = -5 + 1</li>
                </ul>
                <ul>
                  <li>Правило `СТ`:</li>
                  <li>-6 = -10 + 4</li>
                </ul>
                <ul>
                  <li>Правило `MIX`:</li>
                  <li>-8 = -10 + 5 - 3</li>
                </ul>
                <p>
                  <em>Примеры формул на знак `-/+`:</em>
                </p>
                <ul>
                  <li>Правило `МТ`:</li>
                  <li>4 = 5 - 1</li>
                  <li>-4 = -5 + 1</li>
                </ul>
                <ul>
                  <li>Правило `СТ`:</li>
                  <li>6 = 10 - 4</li>
                  <li>-6 = -10 + 4</li>
                </ul>
                <ul>
                  <li>Правило `MIX`:</li>
                  <li>8 = 10 - 5 + 3</li>
                  <li>-8 = -10 + 5 - 3</li>
                </ul>
              </>
            }
          >
            <IconButton aria-label="Активировать">
              <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
            </IconButton>
          </Tooltip>
          Знак
        </InputLabel>
        <Select
          value={params.sign}
          classes={{ root: classes.select }}
          inputProps={{
            name: 'sign',
          }}
          onChange={handleChange}
        >
          <MenuItem value={0}>+</MenuItem>
          <MenuItem value={1}>-</MenuItem>
          <MenuItem value={2}>±</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  );
}

SignField.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
