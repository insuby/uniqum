import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {
  xs,
  sm,
  md,
  lg,
  coefficient,
  SECTION_UP_TO_FOUR,
  SECTION_UP_TO_FIVE,
  SECTION_UP_TO_SIX,
  SECTION_UP_TO_SEVEN,
  SECTION_UP_TO_EIGHT,
  SECTION_UP_TO_NINE,
  SECTION_MIRROR,
  SECTION_TEN,
} from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function SectionField({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    onChangeParams({ ...params, [event.target.name]: event.target.value });
  };

  return (
    <Grid
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.section.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.section.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.section.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.section.lg)}
      key={-8}
    >
      <FormControl className={classes.formControlForSelect}>
        <InputLabel>
          <Tooltip
            className={classes.root}
            placement="right"
            title={
              <>
                <p style={{ width: '250px' }}>
                  <em>
                    `Счет 1,2,3,4` - в примере будут встречаться только числа
                    1,2,3,4
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Счет до 5` - в примере будут встречаться только числа
                    1,2,3,4,5
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Счет до 6` - в примере будут встречаться только числа
                    1,2,3,4,5,6
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Счет до 7` - в примере будут встречаться только числа
                    1,2,3,4,5,6,7
                  </em>
                </p>

                <p style={{ width: '250px' }}>
                  <em>
                    `Счет до 8` - в примере будут встречаться только числа
                    1,2,3,4,5,6,7,8
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Счет до 9` - в примере будут встречаться только числа
                    1,2,3,4,5,6,7,8,9
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Зеркальные` - в примере будут встречаться числа вида: 11,
                    22, 333, 444 и т.д.
                  </em>
                </p>
                <p style={{ width: '250px' }}>
                  <em>
                    `Десятки` - в примере будут встречаться числа вида: 10, 20,
                    300, 400 и т.д.
                  </em>
                </p>
              </>
            }
          >
            <IconButton aria-label="Активировать">
              <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
            </IconButton>
          </Tooltip>
          Раздел
        </InputLabel>
        <Select
          value={params.section}
          inputProps={{
            name: 'section',
          }}
          classes={{ root: classes.select }}
          onChange={handleChange}
        >
          <MenuItem value={SECTION_UP_TO_FOUR}>Счет 1,2,3,4</MenuItem>
          <MenuItem value={SECTION_UP_TO_FIVE}>Счет до 5</MenuItem>
          <MenuItem value={SECTION_UP_TO_SIX}>Счет до 6</MenuItem>
          <MenuItem value={SECTION_UP_TO_SEVEN}>Счет до 7</MenuItem>
          <MenuItem value={SECTION_UP_TO_EIGHT}>Счет до 8</MenuItem>
          <MenuItem value={SECTION_UP_TO_NINE}>Счет до 9</MenuItem>
          <MenuItem value={SECTION_MIRROR}>Зеркальные</MenuItem>
          <MenuItem value={SECTION_TEN}>Десятки</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  );
}

SectionField.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
