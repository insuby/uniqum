import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';

import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import { xs, sm, md, lg, formulas, coefficient } from '../utils/const';

const useStyles = makeStyles(() => ({
  select: {
    marginTop: '5px',
  },
  formControlForSelect: {
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
  root: {
    paddingRight: '5px',
    paddingTop: '2px',
    paddingLeft: '0px',
    paddingBottom: '0px',
  },
}));

export default function NumbersFiled({ params, onChangeParams, fullSize }) {
  const classes = useStyles();

  const handleChange = event => {
    onChangeParams({ ...params, numbers: event.target.value });
  };

  const convertToString = number => {
    const numberPattern = /\d+/g;
    const v = String(number).match(numberPattern);
    let value = `${Math.abs(v[0])}`;
    switch (Number(params.sign)) {
      case 1:
        value = `-${value}`;
        break;
      case 2:
        value = `±${value}`;
        break;
      default:
        value = `${value}`;
        break;
    }
    return value;
  };

  return (
    <Grid
      item
      xs={fullSize ? 12 : Math.ceil(xs * coefficient.numbers.xs)}
      sm={fullSize ? 12 : Math.ceil(sm * coefficient.numbers.sm)}
      md={fullSize ? 12 : Math.ceil(md * coefficient.numbers.md)}
      lg={fullSize ? 12 : Math.ceil(lg * coefficient.numbers.lg)}
      key={-9}
    >
      <FormControl className={classes.formControlForSelect}>
        <InputLabel shrink htmlFor="select-multiple-checkbox">
          <Tooltip
            className={classes.root}
            placement="right"
            title={
              <>
                <p style={{ width: '250px' }}>
                  <em>Примеры формул, если выбрано число 1:</em>
                </p>
                <ul>
                  <li>Правило `МТ` и знак `+`:</li>
                  <li>1 = 5 - 4</li>
                </ul>
                <ul>
                  <li>Правило `СТ` и знак `-`:</li>
                  <li>-1 = -10 + 9</li>
                </ul>
                <ul>
                  <li>Правило `MIX` и знак `-/+` :</li>
                  <li>1 = 10 - 5 - 4</li>
                  <li>-1 = -10 + 5 + 4</li>
                </ul>
                <p style={{ width: '250px' }}>
                  <em>Примеры формул, если выбраны числа 1,2:</em>
                </p>
                <ul>
                  <li>Правило `МТ` и знак `+`:</li>
                  <li>1 = 5 - 4</li>
                  <li>2 = 5 - 3</li>
                </ul>
                <ul>
                  <li>Правило `СТ` и знак `-`:</li>
                  <li>-1 = -10 + 9</li>
                  <li>-2 = -10 + 8</li>
                </ul>
                <ul>
                  <li>Правило `MIX` и знак `-/+` :</li>
                  <li>1 = 10 - 5 - 4</li>
                  <li>-1 = -10 + 5 + 4</li>
                  <li>2 = 10 - 5 - 3</li>
                  <li>-2 = -10 + 5 + 3</li>
                </ul>
                <p>и так далее ...</p>
              </>
            }
          >
            <IconButton>
              <FontAwesomeIcon icon={faQuestionCircle} size="xs" />
            </IconButton>
          </Tooltip>
          Формула(ы):
        </InputLabel>
        <Select
          multiple
          value={params.numbers.map(x => convertToString(x))}
          classes={{ root: classes.select }}
          inputProps={{
            name: 'numbers',
          }}
          onChange={handleChange}
          input={<Input id="select-multiple-checkbox" />}
          renderValue={selected => selected.join(', ')}
        >
          {Object.entries(formulas[params.sign][params.mode]).map(item => {
            const key = convertToString(item[0]);
            return (
              <MenuItem
                disabled={
                  !!(
                    params.numbers.length < 2 &&
                    params.numbers.indexOf(key) > -1
                  )
                }
                key={key}
                value={key}
              >
                <Checkbox
                  inputProps={{
                    name: key,
                  }}
                  checked={
                    params.numbers.map(x => convertToString(x)).indexOf(key) >
                    -1
                  }
                  color="default"
                />
                <ListItemText
                  disableTypography
                  primary={
                    <div dangerouslySetInnerHTML={{ __html: item[1] }} />
                  }
                />
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Grid>
  );
}

NumbersFiled.propTypes = {
  onChangeParams: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  fullSize: PropTypes.bool.isRequired,
};
