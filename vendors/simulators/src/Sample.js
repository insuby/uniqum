import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { PersonalPanelContext } from './PersonalPanelContext';

const Sample = ({ samples, afterCheck }) => {
  const [state] = useContext(PersonalPanelContext);
  const {
    controls: { sample, check },
  } = state;

  let i = 0;
  return (
    <Grid item xs={12} lg={12} sm={12} md={12}>
      {sample &&
        (afterCheck ? afterCheck === check : true) &&
        samples.map((item, index) => (
          <div style={{ fontWeight: 'bold' }} key={(i += 1)}>
            #{index + 1} : {item.join(' ')}
          </div>
        ))}
    </Grid>
  );
};

Sample.propTypes = {
  afterCheck: PropTypes.bool,
  samples: PropTypes.array.isRequired,
};

Sample.defaultProps = {
  afterCheck: false,
};

export default Sample;
