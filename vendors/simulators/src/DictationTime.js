import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles(() => ({
  formControl: {
    width: '100%',
    margin: 8 * 2,
  },
  inputLabel: {
    whiteSpace: 'nowrap',
  },
}));

const DictationTime = ({ onChange, period, samples }) => {
  const classes = useStyles();

  const [value, setValue] = useState(period);

  useEffect(() => {
    setValue(period);
  }, [period]);

  const handleChange = event => {
    setValue(+event.target.value);
    onChange(+event.target.value);
  };

  return (
    <Grid
      container
      item
      xs={12}
      lg={12}
      sm={12}
      md={12}
      alignItems="center"
      justify="space-between"
    >
      <Grid item xs={6} lg={6} sm={6} md={6}>
        <FormControl className={classes.formControl}>
          <InputLabel className={classes.inputLabel}>
            Пауза между примерами.
          </InputLabel>
          <Select value={value} onChange={handleChange}>
            <MenuItem value={5}>5 сек.</MenuItem>
            <MenuItem value={10}>10 сек.</MenuItem>
            <MenuItem value={15}>15 сек.</MenuItem>
            <MenuItem value={20}>20 сек.</MenuItem>
            <MenuItem value={25}>25 сек.</MenuItem>
            <MenuItem value={30}>30 сек.</MenuItem>
            <MenuItem value={45}>45 сек.</MenuItem>
            <MenuItem value={60}>1 мин.</MenuItem>
            <MenuItem value={90}>1.5 мин. </MenuItem>
            <MenuItem value={120}>2 мин.</MenuItem>
            <MenuItem value={180}>3 мин.</MenuItem>
            <MenuItem value={300}>5 мин.</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={5} lg={5} sm={5} md={5}>
        <span style={{ fontWeight: 'bold' }}>Пример #{samples.length}</span>
      </Grid>
    </Grid>
  );
};

DictationTime.propTypes = {
  onChange: PropTypes.func.isRequired,
  period: PropTypes.number.isRequired,
  samples: PropTypes.array.isRequired,
};

export default DictationTime;
