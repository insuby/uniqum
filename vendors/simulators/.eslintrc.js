module.exports = {
  parser: 'babel-eslint',
  extends: [
    '@comocapital',
  ],
  env: {
    browser: true,
    node: true,
  },
  settings: {
    'import/resolver': 'webpack',
  },
  rules: {
    'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
    'react/prefer-stateless-function': ['error', {'ignorePureComponents': true}],
    'react/forbid-prop-types': 'off',
    'no-underscore-dangle': 'off',
    'valid-jsdoc': 'off',
    'require-jsdoc': 'off',
    'react/no-multi-comp': 'off',
    'no-return-assign': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'no-nested-ternary': 'off',
    'import/prefer-default-export': 'off'
  },
  overrides: [
    {
      files: [ 'src/**' ],
      rules: {
        'unicorn/filename-case': 'off',
        'react/jsx-wrap-multilines': 'off',
        'no-return-assign': 'off',
      }
    }
  ]
}