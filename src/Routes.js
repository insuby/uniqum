/**
 * App Routes
 */
import React, { Fragment } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

// App default layout
import PrimaryLayout, { PublicLayout } from 'components/PrimaryLayout';

// Routes
import NotFound from 'pages/NotFound';
import Signin from 'pages/Signin';
import Signup from 'pages/Signup';
import Logout from 'pages/Logout';
import ForgotPassword from 'pages/ForgotPassword';
import ChangePassword from 'pages/ChangePassword';

import Homeworks from 'pages/Homeworks';
// import Simulators from 'pages/Simulators';
import Resend from 'pages/Resend';
import Stub from 'pages/Stub';
import Home from 'pages/Home';
import ResetPassword from 'pages/ResetPassword';

import Students from 'pages/directory/Students';
import CreateStudent from 'pages/directory/students/Create';
import EditStudent from 'pages/directory/students/Edit';
import Simulators from 'pages/Simulators';

import Groups from 'pages/directory/Groups';
import CreateGroup from 'pages/directory/groups/Create';
import EditGroup from 'pages/directory/groups/Edit';

import Schools from 'pages/directory/Schools';
import CreateSchool from 'pages/directory/schools/Create';
import EditSchool from 'pages/directory/schools/Edit';

import Teachers from 'pages/directory/Teachers';
import CreateTeacher from 'pages/directory/teachers/Create';
import EditTeacher from 'pages/directory/teachers/Edit';

import CreateHomework from 'pages/homework/Create';
import EditHomework from 'pages/homework/Edit';
import RunHomework from 'pages/homework/Run';

import Reports from 'pages/reports/Report';
import ArifmetikBook from 'pages/ArifmetikBook';
import Cabinet from 'pages/Cabinet';
import Verify from 'pages/Verify';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';
import Init from './Init';
import Notifier from './Notifier';

const Routes = () => (
  <Fragment>
    <Init />
    <Notifier />
    <Router>
      <Switch>
        <PublicRoute path="/signin" layout={PublicLayout} component={Signin} />
        <PublicRoute path="/signup" layout={PublicLayout} component={Signup} />
        <PublicRoute path="/logout" layout={PublicLayout} component={Logout} />

        <PrivateRoute
          path="/books"
          layout={PrimaryLayout}
          component={ArifmetikBook}
        />

        <PrivateRoute
          path="/simulators"
          layout={PrimaryLayout}
          component={Simulators}
        />

        <PublicRoute
          exact
          path="/verify/:code"
          layout={PublicLayout}
          component={Verify}
        />

        <PrivateRoute
          exact
          path="/directory/students"
          layout={PrimaryLayout}
          component={Students}
        />

        <PrivateRoute
          path="/directory/students/create"
          layout={PrimaryLayout}
          component={CreateStudent}
        />
        <PrivateRoute
          path="/directory/students/edit/:id"
          layout={PrimaryLayout}
          component={EditStudent}
        />

        <PrivateRoute
          exact
          path="/directory/groups"
          layout={PrimaryLayout}
          component={Groups}
        />
        <PrivateRoute
          exact
          path="/reports"
          layout={PrimaryLayout}
          component={Reports}
        />

        <PrivateRoute
          path="/directory/groups/create"
          layout={PrimaryLayout}
          component={CreateGroup}
        />
        <PrivateRoute
          path="/directory/groups/edit/:id"
          layout={PrimaryLayout}
          component={EditGroup}
        />

        <PrivateRoute
          path="/directory/schools/create"
          layout={PrimaryLayout}
          component={CreateSchool}
        />
        <PrivateRoute
          path="/directory/schools/edit/:id"
          layout={PrimaryLayout}
          component={EditSchool}
        />

        <PrivateRoute
          exact
          path="/directory/schools"
          layout={PrimaryLayout}
          component={Schools}
        />

        <PrivateRoute
          path="/directory/teachers/create"
          layout={PrimaryLayout}
          component={CreateTeacher}
        />
        <PrivateRoute
          path="/directory/teachers/edit/:id"
          layout={PrimaryLayout}
          component={EditTeacher}
        />

        <PrivateRoute
          exact
          path="/directory/teachers"
          layout={PrimaryLayout}
          component={Teachers}
        />

        <PrivateRoute
          path="/cabinet/:id"
          layout={PrimaryLayout}
          component={Cabinet}
        />

        <PrivateRoute
          exact
          path="/homeworks"
          layout={PrimaryLayout}
          component={Homeworks}
        />

        <PrivateRoute
          path="/homeworks/create"
          layout={PrimaryLayout}
          component={CreateHomework}
        />
        <PrivateRoute
          path="/homeworks/edit/:id"
          layout={PrimaryLayout}
          component={EditHomework}
        />

        <PrivateRoute
          path="/homeworks/run/:id"
          layout={PrimaryLayout}
          component={RunHomework}
        />

        <PublicRoute
          path="/forgot-password"
          layout={PublicLayout}
          component={ForgotPassword}
        />
        <PublicRoute
          exact
          path="/resend/:email"
          layout={PublicLayout}
          component={Resend}
        />
        <PublicRoute
          exact
          path="/reset-password/:token"
          layout={PublicLayout}
          component={ResetPassword}
        />

        <PrivateRoute path="/" exact layout={PrimaryLayout} component={Home} />
        <PrivateRoute path="/stub1" layout={PrimaryLayout} component={Stub} />
        <PrivateRoute path="/stub2" layout={PrimaryLayout} component={Stub} />

        <PrivateRoute
          path="/change-password"
          layout={PrimaryLayout}
          component={ChangePassword}
        />

        <PublicRoute path="*" layout={PrimaryLayout} component={NotFound} />
      </Switch>
    </Router>
  </Fragment>
);

export default Routes;
