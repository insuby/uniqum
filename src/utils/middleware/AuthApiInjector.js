import { RSAA } from 'redux-api-middleware';

// Добавляем Authorization для каждого запроса
export default store => next => action => {
  const callApi = action[RSAA];
  const { token } = store.getState().user;

  if (callApi && token) {
    callApi.headers = Object.assign({}, callApi.headers, {
      Authorization: token,
    });
  }

  return next(action);
};
