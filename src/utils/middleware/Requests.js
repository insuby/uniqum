import cookie from 'react-cookies';
import { SUCCESS } from 'modules/Requests';
import { types as guestTypes } from 'modules/Guest';
import { setLocale, setLocaleMessages } from 'modules/Locale';
import {
  SET_INIT,
  SET_TASKS,
  SET_TOKEN,
  SET_PROFILE,
  SET_AVTIVITY,
  SET_STUDENTS,
  REQUEST_STUDENTS,
  REQUEST_STUDENT_DELETE,
  SET_GROUPS,
  REQUEST_GROUPS,
  REQUEST_GROUP_DELETE,
  SET_SCHOOLS,
  REQUEST_SCHOOLS,
  REQUEST_SCHOOL_DELETE,
  SET_TEACHERS,
  REQUEST_TEACHERS,
  REQUEST_TEACHER_DELETE,
  REQUEST_LOAD_DATA,
  REQUEST_LOAD_TASKS,
  REQUEST_CONFIRM_CODE,
  GET_REPORTS,
  SET_REPORTS,
  fetchGroups,
  fetchSchools,
  fetchStudents,
  fetchTeachers,
  fetchLoadData,
} from 'modules/User';

import { actions as socketActions } from 'modules/Socket';
import {
  types as homeworkTypes,
  actions as homeworkActions,
} from 'modules/Homework';

export default store => next => action => {
  const token = cookie.load('Authorization');
  const { user } = store.getState();

  if (action.type === SUCCESS) {
    /* User */
    if (action.meta.request === guestTypes.REQUEST_LOGIN) {
      // Устанавливаем токен в Store после успешной авторизации
      store.dispatch({ type: SET_TOKEN, payload: token, meta: 'token' });
      // Загружаем данные пользователя после успешной авторизации
      store.dispatch(fetchLoadData());
    }

    // При успешной инициализации данных пользователя
    if (action.meta.request === REQUEST_LOAD_DATA) {
      const response = action.payload.data;
      // Устанавливаем данные в Store : ...user
      store.dispatch({
        type: SET_PROFILE,
        payload: response,
        meta: 'profile',
      });
      // Устанавливаем флаг в Store : user.init
      store.dispatch({
        type: SET_INIT,
        payload: true,
        meta: 'init',
      });
      // Регистрируем ws
      if (response._id) {
        store.dispatch(socketActions.userRegister(response._id));
      }
      // Устанавливаем локаль если с сервера приходит значение отличное от клиента.
      // Store : user.lang и user.localeMessages
      if (user.lang !== response.lang) {
        store.dispatch(setLocale(response.lang));
        store.dispatch(setLocaleMessages(response.lang));
      }
    }

    // Устанавливаем данные профиля пользователя в Store при успешной загрузке данных пользователя
    if (action.meta.request === REQUEST_LOAD_TASKS) {
      store.dispatch({
        type: SET_TASKS,
        payload: action.payload.data,
        meta: 'tasks',
      });
    }

    // Устанавливаем данные профиля пользователя в Store при успешной загрузке данных пользователя
    if (action.meta.request === REQUEST_CONFIRM_CODE) {
      store.dispatch({
        type: SET_AVTIVITY,
        payload: 2,
        meta: 'activity',
      });
    }
    if (action.meta.request === REQUEST_STUDENTS) {
      // TODO если возможность смотреть пароль нужно будет сделать для всех справочников, то присвоение password = originalPassword,
      // нужно будет вынести на бэк в модель User
      store.dispatch({
        type: SET_STUDENTS,
        payload: action.payload.data.map(student => ({
          ...student,
          password: student.originalPassword,
        })),
        meta: 'students',
      });
    }
    if (action.meta.request === REQUEST_STUDENT_DELETE) {
      store.dispatch(fetchStudents());
    }
    if (action.meta.request === REQUEST_GROUPS) {
      store.dispatch({
        type: SET_GROUPS,
        payload: action.payload.data,
        meta: 'groups',
      });
    }
    if (action.meta.request === REQUEST_GROUP_DELETE) {
      store.dispatch(fetchGroups());
    }
    if (action.meta.request === REQUEST_SCHOOLS) {
      store.dispatch({
        type: SET_SCHOOLS,
        payload: action.payload.data,
        meta: 'schools',
      });
    }
    if (action.meta.request === REQUEST_SCHOOL_DELETE) {
      store.dispatch(fetchSchools());
    }
    if (action.meta.request === REQUEST_TEACHERS) {
      store.dispatch({
        type: SET_TEACHERS,
        payload: action.payload.data,
        meta: 'teachers',
      });
    }
    if (action.meta.request === REQUEST_TEACHER_DELETE) {
      store.dispatch(fetchTeachers());
    }
    if (action.meta.request === homeworkTypes.REQUEST_HOMEWORK) {
      store.dispatch({
        type: homeworkTypes.SET_HOMEWORK,
        payload: action.payload.data,
        meta: 'homework',
      });
    }
    if (action.meta.request === homeworkTypes.REQUEST_HOMEWORKS) {
      store.dispatch({
        type: homeworkTypes.SET_HOMEWORKS,
        payload: action.payload.data,
        meta: 'homeworks',
      });
      store.dispatch({
        type: homeworkTypes.SET_PAGE,
        payload: +action.payload.page,
        meta: 'page',
      });
      store.dispatch({
        type: homeworkTypes.SET_COUNT,
        payload: +action.payload.count,
        meta: 'count',
      });
      store.dispatch({
        type: homeworkTypes.SET_FILTER_DATA,
        payload: action.payload.filterData,
        meta: 'filterData',
      });
    }
    if (action.meta.request === GET_REPORTS) {
      store.dispatch({
        type: SET_REPORTS,
        payload: action.payload.data,
        meta: 'reports',
      });
    }
    if (action.meta.request === homeworkTypes.REQUEST_HOMEWORK_DELETE) {
      store.dispatch(homeworkActions.fetchHomeworks(0, 10));
    }
    // if (action.meta.request === arifmetikTypes.REQUEST_GET_ARGS) {
    //   store.dispatch({
    //     type: arifmetikTypes.SET_DATA,
    //     payload: action.payload.data,
    //     meta: 'data',
    //   });
    // }
  }
  return next(action);
};
