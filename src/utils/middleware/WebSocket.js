import {
  actions,
  SET_USERS_AVAILABLE,
  SET_ROOMS,
  CHANGE_POSITION,
  CHANGE_REQUESTS,
  SET_EVENT,
  SET_MEMBERS,
  CHANGE_CONTROL,
  CHANGE_DICTATION,
  types,
} from 'modules/Socket';
import {
  actions as simulatorsActions,
  types as simulatorsTypes,
} from 'modules/Simulators';
import { SET_USER } from 'modules/User';

import io from 'socket.io-client';

const statuses = {
  error: false,
  success: false,
  loading: false,
  data: [],
};

const host = process.env.WEBSOCKET_URL;

const SocketMiddleware = (function() {
  let socket = null;

  const onOpen = (ws, store, token) => evt => {
    //  Send a handshake, or authenticate with remote end
    console.log('----onOpen', ws, store, token, evt);
    //  Tell the store we're connected
    store.dispatch(actions.connected());
  };

  const onClose = (ws, store) => evt => {
    //  Tell the store we've disconnected
    console.log('----onClose', ws, store, evt);
    store.dispatch(actions.disconnected());
  };

  const onError = error => {
    console.log('----onError', error);
  };

  /*
   *  Пост обработка запроса, по сутм разделение на подтипы (START, STARTED ...) главного типа  CHANGE_CONTROL
   */
  const postRequestProcessing = (store, data) => {
    const { to: user } = data;

    switch (data.type) {
      case 'START': {
        store.dispatch({
          type: CHANGE_REQUESTS,
          payload: { user, statuses: { ...statuses, loading: true } },
        });
        break;
      }
      case 'STARTED': {
        store.dispatch({
          type: CHANGE_REQUESTS,
          payload: {
            user,
            statuses: { ...statuses, success: true, data: data.args },
          },
        });
        break;
      }
      case 'START_ALL':
      case 'STOP_ALL':
      case 'CHECK_ALL':
      case simulatorsTypes.CHANGE_CONTROLS_ALL: {
        store.dispatch({
          type: simulatorsTypes.SET_CONTROLS,
          meta: 'controls',
          payload: data.data.controls,
        });
        break;
      }
      case simulatorsTypes.CHANGE_PARAMS_ALL: {
        store.dispatch({
          type: simulatorsTypes.SET_PARAMS,
          meta: 'params',
          payload: data.data.params,
        });
        break;
      }
      default:
        break;
    }
  };

  /*
   *  Пост обработка запроса для dictations state
   */
  const postRequestProcessingByDictation = (store, data) => {
    switch (data.type) {
      case simulatorsTypes.CHANGE_DICTATION_ALL: {
        store.dispatch({
          type: simulatorsTypes.SET_DICTATION,
          meta: 'dictationState',
          payload: data.data.dictationState,
        });
        break;
      }
      default:
        break;
    }
  };

  const onMessage = (ws, store) => evt => {
    console.log('----onMessage', ws, store, evt);

    const msg = evt.message;
    switch (msg.type) {
      case types.SET_GLOBAL_FILTER_PROP: {
        const [prop, value] = Object.entries(msg.payload.data)[0];
        store.dispatch(simulatorsActions.updatePropFilter(prop, value));
        break;
      }
      case CHANGE_POSITION:
        store.dispatch({
          type: CHANGE_POSITION,
          payload: { user: evt.user, message: msg.payload },
          meta: 'position',
        });
        break;
      case CHANGE_DICTATION:
        store.dispatch({
          type: CHANGE_DICTATION,
          payload: { user: evt.user, message: msg.payload },
          meta: 'dictation',
        });
        postRequestProcessingByDictation(store, msg.payload);
        break;
      case CHANGE_CONTROL:
        store.dispatch({
          type: CHANGE_CONTROL,
          payload: { user: evt.user, message: msg.payload },
          meta: 'control',
        });
        postRequestProcessing(store, msg.payload);
        break;
      default:
        break;
    }
  };

  const onEvent = (ws, store) => evt => {
    //  Parse the JSON message received on the websocket
    const msg = evt.data;
    console.log('----onEvent', ws, store, evt, msg);
    store.dispatch({
      type: SET_EVENT,
      payload: evt,
      meta: 'event',
    });
  };

  return store => next => action => {
    const { user } = store.getState();
    switch (action.type) {
      //  The user wants us to connect
      case 'SOCKET_CONNECT':
        //  Start a new connection to the server
        if (socket != null) {
          socket.close();
        }
        //  Send an action that shows a "connecting..." status for now
        store.dispatch(actions.connecting());

        //  Attempt to connect (we could send a 'failed' action on error)
        socket = io(host, {
          reconnection: true,
          reconnectionDelay: 1000,
          reconnectionDelayMax: 5000,
          reconnectionAttempts: Infinity,
        });
        socket.on('connect', onOpen(socket, store, action.token));
        socket.on('disconnect', onClose(socket, store));
        socket.on('message', onMessage(socket, store));
        socket.on('event', onEvent(socket, store));
        socket.on('error', error => onError(error));

        break;

      //  The user wants us to disconnect
      case 'SOCKET_DISCONNECT':
        if (socket != null) {
          socket.close();
        }
        socket = null;

        //  Set our state to disconnected
        store.dispatch(actions.disconnected());
        break;

      case 'SEND_MESSAGE':
        socket.send(action.payload, action.cb);
        break;
      case 'GET_USERS_AVAILABLE':
        socket.emit('availableUsers', null, (err, users) => {
          if (err) {
            throw new Error(['GET_USERS_AVAILABLE -> Error ', err].join(' : '));
          }
          store.dispatch({
            type: SET_USERS_AVAILABLE,
            payload: users,
            meta: 'usersAvailable',
          });
        });
        break;
      case 'GET_ROOMS':
        socket.emit('chatrooms', null, (err, rooms) => {
          if (err) {
            throw new Error(['GET_ROOMS -> Error ', err].join(' : '));
          }
          let creatorId = user._id;
          if (user.type === 'learner') {
            creatorId = user.creator_id;
          }
          store.dispatch({
            type: SET_ROOMS,
            payload: rooms.filter(item => item.creator_id === creatorId),
            meta: 'rooms',
          });
        });
        break;
      case 'USER_REGISTER': {
        socket.emit('register', action.payload, (err, userData) => {
          if (err) {
            throw new Error(['USER_REGISTER -> Error ', err].join(' : '));
          }

          store.dispatch({
            type: SET_USER,
            payload: userData,
            meta: 'user',
          });
          store.dispatch(actions.getRooms());
        });
        break;
      }
      case 'JOIN_ROOM': {
        socket.emit('join', action.payload, (err, members) => {
          if (err) {
            throw new Error(['JOIN_ROOM -> Error ', err].join(' : '));
          }
          store.dispatch({
            type: SET_MEMBERS,
            payload: members,
            meta: 'membersRoom',
          });
        });
        break;
      }
      case 'LEAVE_ROOM':
        socket.emit('leave', action.payload, err => {
          if (err) {
            throw new Error(['LEAVE_ROOM -> Error ', err].join(' : '));
          }
        });
        break;

      //  This action is irrelevant to us, pass it on to the next middleware
      default:
        return next(action);
    }
    return false;
  };
})();

export default SocketMiddleware;
