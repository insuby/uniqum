export const required = value =>
  value || typeof value === 'number' ? undefined : 'REQUIRED';

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,9}$/i.test(value)
    ? 'INVALID_EMAIL_ADDRESS'
    : undefined;

export const phoneNumber = value =>
  value && !/^(0|[1-9][\d]{10})$/i.test(value)
    ? 'Invalid phone number, must be 11 digits'
    : undefined;

export const minLength = min => value =>
  value && value.length < min ? `MUST_BE_${min}_CHARACTERS_OR_MORE` : undefined;

export const containsLetters = value =>
  value && !/[a-z]+/i.test(value) ? 'MUST_BE_CONTAINS_LETTERS' : undefined;

export const containsNumbers = value =>
  value && !/[\d]+/i.test(value) ? 'MUST_BE_CONTAINS_NUMBER' : undefined;

export const passwordMatch = (value, values) =>
  values.password &&
  values.new_password &&
  values.password === values.new_password
    ? undefined
    : 'PASSWORD_DO_NOT_MATCH';
export const minLength4 = minLength(4);
export const minLength7 = minLength(7);
export const minLength6 = minLength(6);
