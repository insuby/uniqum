export const UPDATE_PROPERTY = 'UPDATE_PROPERTY';
export const UPDATE_PROPERTY_FILTER = 'UPDATE_PROPERTY_FILTER';
export const UPDATE_PROPERTY_CONTROLS = 'UPDATE_PROPERTY_CONTROLS';
export const UPDATE_ALL_PROPERTY_FILTER = 'UPDATE_ALL_PROPERTY_FILTER';
export const SHOW_ABACUS = 'SHOW_ABACUS';
export const HIDE_ABACUS = 'HIDE_ABACUS';

export const NO_MODE = 0;
export const RULE_SIMPLE_SUM = 1;
export const RULE_FIVE_SUM = 2;
export const RULE_TEN_SUM = 3;
export const RULE_FIVE_AND_TEN_SUM = 4;

export const SECTION_UP_TO_FOUR = 4;
export const SECTION_UP_TO_FIVE = 5;
export const SECTION_UP_TO_SIX = 6;
export const SECTION_UP_TO_SEVEN = 7;
export const SECTION_UP_TO_EIGHT = 8;
export const SECTION_UP_TO_NINE = 9;
export const SECTION_MIRROR = 2;
export const SECTION_TEN = 1;

export const SIGN_ADD = 0;
export const SIGN_SUB = 1;
export const SIGN_MIX = 2;

export const NOT_EXCEED_50 = 0;
export const NOT_EXCEED_100 = 1;
export const EXCEED_100 = 2;

const columns = ['a', 'b', 'c', 'd', 'e', 'f'];
export const leftNumbers = [
  { value: 1, label: 1 },
  { value: 2, label: 2 },
  { value: 3, label: 3 },
  { value: 4, label: 4 },
];
export const rightNumbers = [
  { value: 6, label: 6 },
  { value: 7, label: 7 },
  { value: 8, label: 8 },
  { value: 9, label: 9 },
];
export const numbers = [
  ...leftNumbers,
  { value: 5, label: 5 },
  ...rightNumbers,
];

export const abacusInitState = {
  a: [0, 0, 0, 0, 0],
  b: [0, 0, 0, 0, 0],
  c: [0, 0, 0, 0, 0],
  d: [0, 0, 0, 0, 0],
  e: [0, 0, 0, 0, 0],
};

export const labels = {
  roomA: 'Анзан',
  roomB: 'Столбики',
  roomC: 'Флеш карты',
};

export const sumOnRod = rod =>
  rod.reduce((accumulator, value, index) => {
    const result = accumulator;
    let summand = 0;
    if (value === 1) {
      summand = index === 0 ? 5 : 1;
    }
    return result + summand;
  }, 0);

export const sumAbacus = abacusState =>
  Object.values(abacusState)
    .reverse()
    .reduce((accumulator, value, index) => {
      const result = accumulator;
      const summand = sumOnRod(value);
      const order = Number('1'.padEnd(index + 1, '0'));
      return result + summand * order;
    }, 0);

const entity = 'SIMULATORS';

export const types = {
  SET_PARAMS: `${entity}/SET_PARAMS`,
  SET_CONTROLS: `${entity}/SET_CONTROLS`,
  SET_DICTATION: `${entity}/SET_DICTATION`,
  SET_SHOW_TRAINER: `${entity}/SET_SHOW_TRAINER`,
  CHANGE_PARAMS: `CHANGE_FILTER`,
  CHANGE_PARAMS_ALL: `CHANGE_FILTER_ALL`,
  CHANGE_CONTROLS_ALL: `${entity}/CHANGE_CONTROLS_ALL`,
  // SET_COMMON_START: `${entity}/SET_COMMON_START`,
  // SET_DICTATION_START: `${entity}/SET_DICTATION_START`,
  // SET_DICTATION_STOP: `${entity}/SET_DICTATION_STOP`,
  // SET_DICTATION_PROGRESS: `${entity}/SET_DICTATION_PROGRESS`,
  // SET_COMMON_CONTROLS: `${entity}/SET_COMMON_CONTROLS`,
  // SET_COMMON_AMOUNT: `${entity}/SET_COMMON_AMOUNT`,
  // SET_COMMON_PARAMS: `${entity}/SET_COMMON_PARAMS`,
  // SET_COMMON_STOP: `${entity}/SET_COMMON_STOP`,
  // SET_COMMON_CHECK: `${entity}/SET_COMMON_CHECK`,
  // SET_COMMON_SHOW_ANSWER: `${entity}/SET_COMMON_SHOW_ANSWER`,
  // SET_COMMON_DICTATION: `${entity}/SET_COMMON_DICTATION`,
};

/*
 * Определяет является ли пользователь получателем сообщения
 * @param {object} workareaUser - пользователь рабочей области
 * @param {object} sender - отправитель сообщения
 * @param {object} receiver - то кому направляется сообщение,
 * тот кто указан в поле to сообщения
 */
// export const isReceiver = (workareaUser, sender, receiver) =>
//   receiver &&
//   (receiver.type === 'teacher' ||
//     (receiver.type === 'learner' &&
//       ((sender.id === workareaUser.id && workareaUser.type === 'learner') ||
//         (sender.type === 'teacher' && receiver.id === workareaUser.id))));

/*
 * Ищет сравнение отправить = получатель
 * @param {object} workareaUser - пользователь рабочей области
 * @param {object} sender - отправитель сообщения
 * @param {object} receiver - то кому направляется сообщение,
 * тот кто указан в поле to сообщения
 */
export const isReceiver = (workareaUser, sender, receiver) =>
  receiver &&
  (receiver.type === 'teacher' ||
    (receiver.type === 'learner' &&
      ((sender.id === workareaUser.id && workareaUser.type === 'learner') ||
        (sender.type === 'teacher' && receiver.id === workareaUser.id))));

/**
 * Заполняет состояние абака единицами согласно
 * переданному числу
 * @param {number} number - число которое нужно набрать на абаке
 * @param {number} range - это порядок абака
 * @return {object} состояние абакуса
 */
export const fillingValues = (number, range) => {
  let nextAbacus = {};
  for (let i = 0; i < range; i += 1) {
    nextAbacus = { ...nextAbacus, [columns[i]]: [0, 0, 0, 0, 0] };
  }
  if (number) {
    let k = range;
    let pod = [0, 0, 0, 0, 0];
    const numberComposition = number.toString();

    for (let i = numberComposition.length - 1; i >= 0; i -= 1) {
      k -= 1;
      const label = columns[k];
      switch (numberComposition[i]) {
        case '1':
          pod = [0, 1, 0, 0, 0];
          break;
        case '2':
          pod = [0, 1, 1, 0, 0];
          break;
        case '3':
          pod = [0, 1, 1, 1, 0];
          break;
        case '4':
          pod = [0, 1, 1, 1, 1];
          break;
        case '5':
          pod = [1, 0, 0, 0, 0];
          break;
        case '6':
          pod = [1, 1, 0, 0, 0];
          break;
        case '7':
          pod = [1, 1, 1, 0, 0];
          break;
        case '8':
          pod = [1, 1, 1, 1, 0];
          break;
        case '9':
          pod = [1, 1, 1, 1, 1];
          break;
        default:
          pod = [0, 0, 0, 0, 0];
          break;
      }
      nextAbacus = { ...nextAbacus, [label]: pod };
    }
  }
  return nextAbacus;
};

// Reducer
const initialState = {
  params: {
    section: 5,
    steps: 3,
    interval: 1,
    mode: 1,
    sign: 0,
    bound: 0,
    under: 1,
    over: 1,
    numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
  },
  controls: {
    count: 5,
    start: false,
    stop: false,
    check: false,
    clear: false,
    abacus: false,
    settings: true,
    repeat: false,
    correct: false,
    open: false,
    sound: false,
    sample: false,
    showAnswers: false,
  },
  dictationState: {
    dictation: false,
    amount: 3,
    start: false,
    progress: false,
    stop: false,
    period: 5,
    samples: [],
  },
  showTrainer: true,
  displayAbacus: false,
};

const simulatorsrReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_PARAMS:
    case types.SET_DICTATION:
    case types.SET_SHOW_TRAINER:
    case types.SET_CONTROLS: {
      return {
        ...state,
        [action.meta]: action.payload,
      };
    }

    case UPDATE_PROPERTY_FILTER: {
      return {
        ...state,
        params: { ...state.params, [action.payload.prop]: action.payload.data },
      };
    }
    case UPDATE_PROPERTY_CONTROLS: {
      return {
        ...state,
        controls: action.payload.controls,
      };
    }
    case UPDATE_PROPERTY: {
      return { ...state, [action.payload.prop]: action.payload.data };
    }
    case UPDATE_ALL_PROPERTY_FILTER: {
      return { ...state, params: action.payload.data };
    }
    case SHOW_ABACUS: {
      return { ...state, displayAbacus: true };
    }
    case HIDE_ABACUS: {
      return { ...state, displayAbacus: false };
    }

    default:
      return state;
  }
};

// Actions

/**
 * Обновляем свойства первого уровня
 * @param {string} prop - наименование свойства.
 * @param {object} data - значение.
 * @returns {object}.
 */
export function updateProp(prop, data) {
  return dispatch => {
    dispatch({
      type: UPDATE_PROPERTY,
      payload: {
        prop,
        data,
      },
    });
  };
}

/**
 * Обновляем свойства первого уровня
 * @param {string} prop - наименование свойства.
 * @param {object} data - значение.
 * @returns {object}.
 */
function updatePropFilter(prop, data) {
  return dispatch => {
    dispatch({
      type: UPDATE_PROPERTY_FILTER,
      payload: {
        prop,
        data,
      },
    });
  };
}

const updatePropControl = controls => dispatch =>
  dispatch({
    type: UPDATE_PROPERTY_CONTROLS,
    payload: { controls },
  });

/**
 * Обновляем свойства фильтра  оюъект переменной params
 * @param {object} data - значение.
 * @returns {object}.
 */
export function updateAllPropFilter(data) {
  return dispatch => {
    dispatch({
      type: UPDATE_ALL_PROPERTY_FILTER,
      payload: {
        data,
      },
    });
  };
}

/**
 * Показываем абак
 * @returns {object}.
 */
export function showAbacus() {
  return dispatch => {
    dispatch({
      type: SHOW_ABACUS,
    });
  };
}

/**
 * Скрываем абак
 * @returns {object}.
 */
export function hideAbacus() {
  return dispatch => {
    dispatch({
      type: HIDE_ABACUS,
    });
  };
}

function setShowTrainer(payload) {
  return dispatch => {
    dispatch({
      type: types.SET_SHOW_TRAINER,
      meta: 'showTrainer',
      payload,
    });
  };
}
// function onDictationStart() {
//   return dispatch => {
//     dispatch({
//       type: types.SET_DICTATION_START,
//       meta: 'dictationStatuses',
//       payload: {
//         start: true,
//         stop: false,
//         progress: false,
//       },
//     });
//   };
// }

// function onDictationStop() {
//   return dispatch => {
//     dispatch({
//       type: types.SET_DICTATION_STOP,
//       meta: 'dictationStatuses',
//       payload: {
//         start: false,
//         stop: true,
//         progress: false,
//       },
//     });
//   };
// }
// function onDictationProgress() {
//   return dispatch => {
//     dispatch({
//       type: types.SET_DICTATION_PROGRESS,
//       meta: 'dictationStatuses',
//       payload: {
//         start: false,
//         stop: false,
//         progress: true,
//       },
//     });
//   };
// }

export const actions = {
  setShowTrainer,
  updatePropFilter,
  updatePropControl,
  // onDictationStart,
  // onDictationStop,
  // onDictationProgress,
};

export default simulatorsrReducer;
