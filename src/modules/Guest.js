import { RSAA } from 'redux-api-middleware';
import cookie from 'react-cookies';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';

const host = process.env.BACKEND_URL;
export const FORM_SIGNIN = 'signin';

export const types = {
  REQUEST_LOGIN: 'GUEST/REQUEST_LOGIN',
  REQUEST_RESEND: 'GUEST/REQUEST_RESEND',
  REQUEST_VERIFICATION: 'GUEST/REQUEST_VERIFICATION',
  REQUEST_REGISTRATION: 'GUEST/REQUEST_REGISTRATION',
  REQUEST_RESET_PASSWORD: 'GUEST/REQUEST_RESET_PASSWORD',
  REQUEST_FORGOT_PASSWORD: 'GUEST/REQUEST_FORGOT_PASSWORD',
};

// Actions
const fetchLogin = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/login/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [
      { type: REQUEST },
      {
        type: SUCCESS,
        payload: (action, state, res) => {
          const token = res.headers.get('Authorization');
          const expires = new Date();
          expires.setDate(expires.getDate() + 364);
          cookie.save('Authorization', token, {
            path: '/',
            expires,
          });
          return res.json();
        },
      },
      { type: FAILURE },
    ].map(type => ({
      ...type,
      meta: {
        request: types.REQUEST_LOGIN,
        params: formData,
      },
    })),
  },
});

const fetchForgotPassword = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/forgot/${formData.email}/`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_FORGOT_PASSWORD,
      },
    })),
  },
});

const fetchResetPassword = (token, formData) => ({
  [RSAA]: {
    endpoint: `${host}/users/restore/${token}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_RESET_PASSWORD,
      },
    })),
  },
});

const fetchRegistration = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_REGISTRATION,
      },
    })),
  },
});

const fetchVerification = code => ({
  [RSAA]: {
    endpoint: `${host}/users/verify/${code}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_VERIFICATION,
      },
    })),
  },
});

const fetchResend = email => ({
  [RSAA]: {
    endpoint: `${host}/users/resend/${email}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_RESEND,
      },
    })),
  },
});

export const actions = {
  fetchLogin,
  fetchResend,
  fetchVerification,
  fetchRegistration,
  fetchResetPassword,
  fetchForgotPassword,
};
