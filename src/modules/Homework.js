import { RSAA } from 'redux-api-middleware';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';
import { labels } from './Simulators';

export const ADD_HOMEWORK_FORM = 'ADD_HOMEWORK_FORM';
export const HOMEWORK_FORM = 'HOMEWORK_FORM';

const host = process.env.BACKEND_URL;

// Init state
const initialState = {
  homeworks: [],
  page: 0,
  count: 10,
  rowsPerPage: 10,
  filterList: [],
  filterData: [],
  homework: {},
};

export const types = {
  SET_PAGE: 'SET_PAGE',
  SET_COUNT: 'SET_COUNT',
  SET_HOMEWORK: 'SET_HOMEWORK',
  SET_HOMEWORKS: 'SET_HOMEWORKS',
  SET_FILTER_LIST: 'SET_FILTER_LIST',
  SET_FILTER_DATA: 'SET_FILTER_DATA',
  SET_ROWS_PER_PAGE: 'SET_ROWS_PER_PAGE',
  REQUEST_HOMEWORK: 'REQUEST_HOMEWORK',
  REQUEST_HOMEWORKS: 'REQUEST_HOMEWORKS',
  REQUEST_HOMEWORK_CREATE: 'REQUEST_HOMEWORK_CREATE',
  REQUEST_GET_ARGS: 'HOMEWORK/REQUEST_GET_ARGS',
  REQUEST_CREATE_TASK: 'HOMEWORK/REQUEST_CREATE_TASK',
};

export const simulators = [
  {
    value: 'roomA',
    label: labels.roomA,
  },
  {
    value: 'roomB',
    label: labels.roomB,
  },
  {
    value: 'roomC',
    label: labels.roomC,
  },
];

// Reducer
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_PAGE:
    case types.SET_COUNT:
    case types.SET_HOMEWORK:
    case types.SET_HOMEWORKS:
    case types.SET_FILTER_LIST:
    case types.SET_FILTER_DATA:
    case types.SET_ROWS_PER_PAGE:
      return {
        ...state,
        [action.meta]: action.payload,
      };
    default:
      return state;
  }
};

// Actions

const setRowsPerPage = payload => ({
  type: types.SET_ROWS_PER_PAGE,
  payload,
  meta: 'rowsPerPage',
});

const setFilterList = payload => ({
  type: types.SET_FILTER_LIST,
  payload,
  meta: 'filterList',
});

const fetchHomework = id => ({
  [RSAA]: {
    endpoint: `${host}/homeworks/${id}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_HOMEWORK,
      },
    })),
  },
});

const fetchHomeworks = (page, rowsPerPage, filterData = {}) => ({
  [RSAA]: {
    endpoint: `${host}/homeworks?page=${page}&rowsPerPage=${rowsPerPage}&filter=${JSON.stringify(
      filterData
    )}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_HOMEWORKS,
      },
    })),
  },
});

const fetchHomeworkCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/homeworks/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_HOMEWORK_CREATE,
      },
    })),
  },
});

const fetchTaskCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/tasks/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_CREATE_TASK,
      },
    })),
  },
});

const fetchHomeworkEdit = (id, formData) => ({
  [RSAA]: {
    endpoint: `${host}/homeworks/${id}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_HOMEWORK_EDIT,
      },
    })),
  },
});

const fetchArgs = formData => ({
  [RSAA]: {
    endpoint: `${host}/arifmetik_book/args`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_GET_ARGS,
      },
    })),
  },
});
const fetchHomeworkDelete = formData => ({
  [RSAA]: {
    endpoint: `${host}/homeworks/`,
    method: 'DELETE',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_HOMEWORK_DELETE,
      },
    })),
  },
});

export const actions = {
  setFilterList,
  setRowsPerPage,
  fetchArgs,
  fetchHomework,
  fetchHomeworks,
  fetchTaskCreate,
  fetchHomeworkCreate,
  fetchHomeworkEdit,
  fetchHomeworkDelete,
};

export default reducer;
