import { RSAA } from 'redux-api-middleware';
import cookie from 'react-cookies';
import languages from 'utils/languages';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';
import { lang, SET_LOCALE, SET_LOCALE_MESSAGES } from 'modules/Locale';

export const CLEAN_USER = 'user/CleanUser';
export const SET_INIT = 'user/SET_INIT';
export const SET_TOKEN = 'user/SET_TOKEN';
export const SET_TASKS = 'USER/SET_TASKS';
export const SET_TOKENS = 'user/SET_TOKENS';
export const SET_PAY_ACC = 'user/SET_PAY_ACC';
export const SET_PROFILE = 'user/SET_PROFILE';
export const SET_AVTIVITY = 'user/SET_AVTIVITY';
export const SET_KYC_TOKEN = 'user/SET_KYC_TOKEN';
export const SET_APPLICANT_ID = 'user/SET_APPLICANT_ID';
export const SET_TWOFA_CHECKED = 'user/SET_TWOFA_CHECKED';
export const SET_TWOFA_SECRET_TOKEN = 'user/SET_TWOFA_SECRET_TOKEN';

export const REQUEST_TOKENS = 'user/requestTokens';
export const REQUEST_LOAD_DATA = 'user/requestLoadData';
export const REQUEST_LOAD_TASKS = 'USER/REQUEST_LOAD_TASKS';
export const REQUEST_CONFIRM_CODE = 'user/requestConfirmCode';
export const REQUEST_CONFIRM_PHONE = 'user/requestConfirmPhone';
export const REQUEST_CHANGE_PASSWORD = 'user/requestChangePassword';
export const REQUEST_KYC_ACCESS_DATA = 'user/requestKycAccessData';
export const REQUEST_CREATE_APPLICANT = 'user/REQUEST_CREATE_APPLICANT';
export const REQUEST_TWOFA_CHECK = 'user/requestTwofaCheck';
export const REQUEST_TWOFA_ACTIVATE = 'user/requestTwofaActivate';
export const REQUEST_TWOFA_DEACTIVATE = 'user/requestTwofaDeactivate';
export const REQUEST_TWOFA_SECRET_TOKEN = 'user/requestTwofaSecretToken';
export const REQUEST_LOAD_REFERRALS = 'USER/REQUEST_LOAD_REFERRALS';
export const REQUEST_PAYMENT = 'USER/REQUEST_PAYMENT';
export const SET_USER = 'SET_USER';
export const STUDENT_FORM = 'STUDENT_FORM';
export const REQUEST_STUDENT_CREATE = 'REQUEST_STUDENT_CREATE';
export const REQUEST_STUDENT_EDIT = 'REQUEST_STUDENT_EDIT';
export const REQUEST_STUDENT_DELETE = 'REQUEST_STUDENT_DELETE';
export const REQUEST_STUDENTS = 'REQUEST_STUDENTS';
export const SET_STUDENTS = 'SET_STUDENTS';
export const GROUP_FORM = 'GROUP_FORM';
export const REQUEST_GROUP_CREATE = 'REQUEST_GROUP_CREATE';
export const REQUEST_GROUP_EDIT = 'REQUEST_GROUP_EDIT';
export const REQUEST_GROUP_DELETE = 'REQUEST_GROUP_DELETE';
export const REQUEST_GROUPS = 'REQUEST_GROUPS';
export const SET_GROUPS = 'SET_GROUPS';
export const SCHOOL_FORM = 'SCHOOL_FORM';
export const REQUEST_SCHOOL_CREATE = 'REQUEST_SCHOOL_CREATE';
export const REQUEST_SCHOOL_EDIT = 'REQUEST_SCHOOL_EDIT';
export const REQUEST_SCHOOL_DELETE = 'REQUEST_SCHOOL_DELETE';
export const REQUEST_SCHOOLS = 'REQUEST_SCHOOLS';
export const SET_SCHOOLS = 'SET_SCHOOLS';
export const TEACHER_FORM = 'TEACHER_FORM';
export const REQUEST_TEACHER_CREATE = 'REQUEST_TEACHER_CREATE';
export const REQUEST_TEACHER_EDIT = 'REQUEST_TEACHER_EDIT';
export const REQUEST_TEACHER_DELETE = 'REQUEST_TEACHER_DELETE';
export const REQUEST_TEACHERS = 'REQUEST_TEACHERS';
export const SET_TEACHERS = 'SET_TEACHERS';
export const GET_REPORTS = 'GET_REPORTS';
export const SET_REPORTS = 'SET_REPORTS';
export const REQUEST_SEND_DATA_TO_EMAIL = 'REQUEST_SEND_DATA_TO_EMAIL';

const host = process.env.BACKEND_URL;

// Init state
const initialState = {
  token: cookie.load('Authorization') || '',
  balance: '0',
  init: false,
  lang,
  localeMessages: languages[lang],
  tasks: [],
  user: {},
  role: 0,
  students: [],
  groups: [],
  schools: [],
  teachers: [],
  reports: {},
};

export const ROLE = {
  GUEST: 0,
  AUTH: 1,
  MANAGER: 2,
  ADMIN: 3,
  SYSTEM: 4,
};

// Reducer
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        ...action.payload,
      };
    case CLEAN_USER:
      return { ...initialState, token: '' };
    case SET_INIT:
    case SET_TASKS:
    case SET_TOKEN:
    case SET_TOKENS:
    case SET_PAY_ACC:
    case SET_AVTIVITY:
    case SET_KYC_TOKEN:
    case SET_APPLICANT_ID:
    case SET_TWOFA_CHECKED:
    case SET_TWOFA_SECRET_TOKEN:
    case SET_LOCALE:
    case SET_LOCALE_MESSAGES:
    case SET_USER:
    case SET_STUDENTS:
    case SET_GROUPS:
    case SET_SCHOOLS:
    case SET_TEACHERS:
    case SET_REPORTS:
      return {
        ...state,
        [action.meta]: action.payload,
      };
    default:
      return state;
  }
};

// Actions
export const fetchReports = studentId => ({
  [RSAA]: {
    endpoint: `${host}/tasks/?student=${studentId}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: GET_REPORTS,
      },
    })),
  },
});

export const fetchLoadData = () => ({
  [RSAA]: {
    endpoint: `${host}/users/me`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_LOAD_DATA,
      },
    })),
  },
});

export const fetchChangePassword = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/password/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_CHANGE_PASSWORD,
      },
    })),
  },
});

export const fetchStudentCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/students/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_STUDENT_CREATE,
      },
    })),
  },
});

export const fetchStudentEdit = (id, formData) => ({
  [RSAA]: {
    endpoint: `${host}/students/${id}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_STUDENT_EDIT,
      },
    })),
  },
});

export const fetchStudentDelete = id => ({
  [RSAA]: {
    endpoint: `${host}/students/${id}`,
    method: 'DELETE',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_STUDENT_DELETE,
      },
    })),
  },
});

export const fetchStudents = () => ({
  [RSAA]: {
    endpoint: `${host}/students/`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_STUDENTS,
      },
    })),
  },
});

export const fetchGroupCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/groups/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_GROUP_CREATE,
      },
    })),
  },
});

export const fetchGroupEdit = (id, formData) => ({
  [RSAA]: {
    endpoint: `${host}/groups/${id}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_GROUP_EDIT,
      },
    })),
  },
});

export const fetchGroupDelete = id => ({
  [RSAA]: {
    endpoint: `${host}/groups/${id}`,
    method: 'DELETE',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_GROUP_DELETE,
      },
    })),
  },
});

export const fetchGroups = () => ({
  [RSAA]: {
    endpoint: `${host}/groups/`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_GROUPS,
      },
    })),
  },
});

export const fetchSchoolCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/schools/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_SCHOOL_CREATE,
      },
    })),
  },
});

export const fetchSchoolEdit = (id, formData) => ({
  [RSAA]: {
    endpoint: `${host}/schools/${id}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_SCHOOL_EDIT,
      },
    })),
  },
});

export const fetchSchoolDelete = id => ({
  [RSAA]: {
    endpoint: `${host}/schools/${id}`,
    method: 'DELETE',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_SCHOOL_DELETE,
      },
    })),
  },
});

export const fetchSchools = () => ({
  [RSAA]: {
    endpoint: `${host}/schools/`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_SCHOOLS,
      },
    })),
  },
});
export const fetchTeacherCreate = formData => ({
  [RSAA]: {
    endpoint: `${host}/teachers/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TEACHER_CREATE,
      },
    })),
  },
});

export const fetchTeacherEdit = (id, formData) => ({
  [RSAA]: {
    endpoint: `${host}/teachers/${id}`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TEACHER_EDIT,
      },
    })),
  },
});

export const fetchTeacherDelete = id => ({
  [RSAA]: {
    endpoint: `${host}/teachers/${id}`,
    method: 'DELETE',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TEACHER_DELETE,
      },
    })),
  },
});

export const fetchTeachers = () => ({
  [RSAA]: {
    endpoint: `${host}/teachers/`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TEACHERS,
      },
    })),
  },
});

export const fetchConfirmCode = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/sms/${formData.token}/?token=${formData.token}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_CONFIRM_CODE,
      },
    })),
  },
});

export const fetchConfirmPhone = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/phone/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_CONFIRM_PHONE,
      },
    })),
  },
});

export const fetchTokens = () => ({
  [RSAA]: {
    endpoint: `${host}/users/tokens`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TOKENS,
      },
    })),
  },
});

export const fetchKycAccessData = userId => ({
  [RSAA]: {
    endpoint: `${host}/users/kyc/accessData/?userId=${userId}`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_KYC_ACCESS_DATA,
      },
    })),
  },
});

export const fetchTwofaSecretToken = () => ({
  [RSAA]: {
    endpoint: `${host}/users/2fa/activate/`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TWOFA_SECRET_TOKEN,
      },
    })),
  },
});

export const fetchTwofaActivate = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/2fa/activate/confirm/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TWOFA_ACTIVATE,
      },
    })),
  },
});

export const fetchTwofaDeactivate = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/2fa/deactivate/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TWOFA_DEACTIVATE,
      },
    })),
  },
});

export const fetchCreateApplicant = data => ({
  [RSAA]: {
    endpoint: `${host}/users/kyc/applicant`,
    method: 'POST',
    body: JSON.stringify(data),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_CREATE_APPLICANT,
      },
    })),
  },
});

export const fetchTwofaCheck = formData => ({
  [RSAA]: {
    endpoint: `${host}/login/2fa/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_TWOFA_CHECK,
      },
    })),
  },
});

export const fetchRequestPayment = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/payment/`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_PAYMENT,
      },
    })),
  },
});

export const cleanUser = () => ({
  type: CLEAN_USER,
});

export const fetchLoadReferrals = () => ({
  [RSAA]: {
    endpoint: `${host}/users/referrals`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_LOAD_REFERRALS,
      },
    })),
  },
});

export const fetchLoadTasks = () => ({
  [RSAA]: {
    endpoint: `${host}/tasks`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_LOAD_TASKS,
      },
    })),
  },
});

export const fetchRequestSendData = formData => ({
  [RSAA]: {
    endpoint: `${host}/users/send`,
    method: 'POST',
    body: JSON.stringify(formData),
    headers: { 'Content-Type': 'application/json' },
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: REQUEST_SEND_DATA_TO_EMAIL,
      },
    })),
  },
});

export default userReducer;
