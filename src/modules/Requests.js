import isObject from 'lodash.isobject';

export const REQUEST = 'REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';

const states = {
  error: false,
  success: false,
  loading: false,
  errorMessage: '',
};

export const combineRequests = crudList =>
  Object.keys({ ...states })
    .map(v => crudList.find(r => r && r[v]) && crudList.find(r => r && r[v])[v])
    .reduce(
      (accumulator, item, index) => {
        const result = accumulator;
        result[Object.keys(accumulator)[index]] = item;
        return result;
      },
      { ...states }
    );

const entity = (state = { ...states }, action) => {
  switch (action.type) {
    case REQUEST:
      return {
        ...state,
        error: false,
        success: false,
        loading: true,
      };
    case SUCCESS:
      return {
        ...state,
        error: false,
        success: true,
        loading: false,
        data: isObject(action.payload) ? action.payload.data : action.payload,
      };
    case FAILURE:
      return {
        ...state,
        error: true,
        success: false,
        loading: false,
        errorMessage: action.payload.response.data,
      };
    default:
      return state;
  }
};

// Reducer
const requestReducer = (state = {}, action) => {
  switch (action.type) {
    case REQUEST:
    case SUCCESS:
    case FAILURE:
      return {
        ...state,
        [action.meta.request]: entity(state[action.meta], action),
      };
    default:
      return state;
  }
};

export default requestReducer;
