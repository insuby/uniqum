import { RSAA } from 'redux-api-middleware';
import { REQUEST, SUCCESS, FAILURE } from 'modules/Requests';

const host = process.env.BACKEND_URL;

export const FORM_MATHS = 'maths';

export const types = {
  REQUEST_LOAD_DEVICES: 'TASKS/REQUEST_LOAD_DEVICES',
  REQUEST_SAVE_SETTINGS: 'TASKS/REQUEST_SAVE_SETTINGS',
};

const fetchLoadDevices = () => ({
  [RSAA]: {
    endpoint: `${host}/devices`,
    method: 'GET',
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: {
        request: types.REQUEST_LOAD_DEVICES,
      },
    })),
  },
});

export const fetchSaveSettings = formData => ({
  [RSAA]: {
    endpoint: `${host}/settings`,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(formData),
    types: [REQUEST, SUCCESS, FAILURE].map(type => ({
      type,
      meta: types.REQUEST_SAVE_SETTINGS,
    })),
  },
});

export const actions = {
  fetchLoadDevices,
  fetchSaveSettings,
};
