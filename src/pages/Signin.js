import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import { types, actions, FORM_SIGNIN } from 'modules/Guest';

// Components
import { H1Panel, H2Panel, FormError } from 'UIKit/Fonts';
import { Box } from 'UIKit/Grid';
import Loading from 'components/Form/Loading';
import SigninForm from './signin/SigninForm';

/**
 * Signin page.
 * @param {object} values values.
 */
class Signin extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Life cycle.
   */
  componentDidMount() {
    const { user, history } = this.props;
    if (user && user.token) {
      history.push('/');
    }
  }

  /**
   * @param {object} formData данные формы.
   */
  handleSubmit(formData) {
    const { login } = this.props;
    login(formData);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, history, forms, user } = this.props;
    const request = requests[types.REQUEST_LOGIN];
    const form = forms[FORM_SIGNIN];
    const email = form && form.values && form.values.email;

    return (
      <Fragment>
        <Box mb={8}>
          <Box mb={1}>
            <H1Panel>
              <FormattedMessage id="signin.title" defaultMessage="" />
            </H1Panel>
          </Box>
          <H2Panel>
            <FormattedMessage id="signin.subtitle" defaultMessage="" />
          </H2Panel>
        </Box>
        {request && (
          <Fragment>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                {request.errorMessage === 'EMAIL_NOT_VERIFIED' ? (
                  <FormattedMessage
                    id="signin.repeat"
                    defaultMessage=""
                    values={{
                      repeat: (
                        <span style={{ textDecoration: 'underline' }}>
                          <Link to={`/resend/${email}`}>
                            <FormattedMessage
                              id="common.repeat"
                              defaultMessage="Repeat"
                            />
                          </Link>
                        </span>
                      ),
                    }}
                  />
                ) : (
                  <FormattedMessage
                    id={request.errorMessage}
                    defaultMessage=""
                  />
                )}
              </FormError>
            )}
            {user.token && history.push('/')}
          </Fragment>
        )}
        <SigninForm onSubmit={this.handleSubmit} />
      </Fragment>
    );
  }
}

Signin.propTypes = {
  login: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  forms: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  forms: state.form,
  user: state.user,
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  login(formData) {
    dispatch(actions.fetchLogin(formData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Signin);
