import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage } from 'react-intl';

import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { required, minLength4, passwordMatch } from 'utils/Validate';

// styled
import { FormGroup } from 'UIKit/Form';
import colors from 'UIKit/Colors';
import styled from 'styled-components';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

const ChangePasswordForm = ({
  invalid,
  pristine,
  submitting,
  handleSubmit,
}) => (
  <Fragment>
    <form onSubmit={handleSubmit}>
      <FormGroup>
        <Field
          name="old_password"
          type="password"
          label={
            <FormattedMessage id="signin.password_old" defaultMessage="" />
          }
          component={RenderCustomField}
          validate={[required, minLength4]}
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="password"
          component={RenderCustomField}
          type="password"
          label={
            <FormattedMessage id="signin.password_new" defaultMessage="" />
          }
          validate={[required, minLength4]}
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="new_password"
          component={RenderCustomField}
          type="password"
          label={
            <FormattedMessage id="signin.password_confirm" defaultMessage="" />
          }
          validate={[required, passwordMatch]}
        />
      </FormGroup>
      <ButtonStyled
        fullWidth
        type="submit"
        disabled={pristine || submitting || invalid}
      >
        <FormattedMessage id="changePassword.btn_title" defaultMessage="" />
      </ButtonStyled>
    </form>
  </Fragment>
);

ChangePasswordForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'changepwd',
})(ChangePasswordForm);
