import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { actions, HOMEWORK_FORM, types } from 'modules/Homework';
import { FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import Panel from 'components/Panel';
import { H3, FormError, FormSuccess } from 'UIKit/Fonts';

import Loading from 'components/Form/Loading';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

import HomeworkForm from './HomeworkForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

/**
 * EditHomework page.
 * @param {object} values values.
 */
class EditHomework extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
    this.state = {
      query: false,
    };
  }

  /**
   * @param {object} formData formData.
   */
  onSubmit = formData => {
    const {
      onEdit,
      match: {
        params: { id },
      },
    } = this.props;
    this.setState({ query: true }, () => onEdit(id, formData));
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      requests,
      handleSubmit,
      match: {
        params: { id },
      },
      homeworks,
    } = this.props;
    const { query } = this.state;
    const request = requests[types.REQUEST_HOMEWORK_EDIT];
    return (
      <Box w={1} px={4}>
        <Panel>
          {homeworks && (
            <Fragment>
              <Box px={[1, 8]} py={[0, 4]}>
                <H3>
                  {request && query && (
                    <Fragment>
                      {request.loading && <Loading />}
                      {request.error && (
                        <FormError>{request.errorMessage}</FormError>
                      )}
                      {request.data && (
                        <FormSuccess>
                          <FormattedMessage
                            id="HOMEWORK_SUCCESS_SAVE_MESSAGE"
                            defaultMessage="HOMEWORK_SUCCESS_SAVE_MESSAGE"
                          />
                        </FormSuccess>
                      )}
                    </Fragment>
                  )}
                  <FormattedMessage
                    id="HOMEWORK_TABLE_SAVE_TITLE"
                    defaultMessage="HOMEWORK_TABLE_SAVE_TITLE"
                  />
                </H3>
              </Box>
              <Box px={[4, 8]} py={[3, 6]}>
                <form onSubmit={handleSubmit(this.onSubmit)}>
                  <HomeworkForm
                    initialValues={homeworks.filter(item => item.id === id)[0]}
                  />
                  <ButtonStyled fullWidth type="submit">
                    <FormattedMessage
                      id="HOMEWORK_TABLE_SAVE"
                      defaultMessage="HOMEWORK_TABLE_SAVE"
                    />
                  </ButtonStyled>
                </form>
              </Box>
            </Fragment>
          )}
        </Panel>
      </Box>
    );
  }
}

EditHomework.propTypes = {
  requests: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  homeworks: PropTypes.array.isRequired,
  onEdit: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(actions.fetchHomeworks());
  },
  onEdit(id, formData) {
    dispatch(actions.fetchHomeworkEdit(id, formData));
  },
});

const mapStateToProps = state => ({
  requests: state.requests,
  homeworks: state.homeworks.homeworks,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: HOMEWORK_FORM,
  })(EditHomework)
);
