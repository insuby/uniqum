import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { required } from 'utils/Validate';

import { HOMEWORK_FORM } from 'modules/Homework';
import RenderCustomDayPicker from 'components/Form/Input/RenderCustomDayPicker';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';

// Components
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';
import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import { fetchGroups, fetchStudents } from 'modules/User';

/**
 * RigthPathForm.
 */
class RigthPathForm extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
    this.state = {};
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      user: { lang: locale },
      groups,
      students,
      selectedGroup,
    } = this.props;
    let studentsList = [];
    let groupsList = [];

    if (students.length > 0) {
      if (selectedGroup.value) {
        studentsList = students
          .filter(
            item => item.groups.map(j => j.id).indexOf(selectedGroup.value) > -1
          )
          .map(item => ({
            value: item.id,
            label: [item.name, item.surname].join(' '),
          }));
      } else {
        studentsList = students.map(item => ({
          value: item.id,
          label: [item.name, item.surname].join(' '),
        }));
      }
    }

    if (groups.length > 0) {
      groupsList = groups.map(item => ({
        value: item.id,
        label: item.name,
      }));
    }

    return (
      <Fragment>
        {groups.length > 0 && (
          <Fragment>
            <Box py={[0, 4]}>
              <H6>
                <FormattedMessage
                  id="HOMEWORK_FORM_RIGTH_PATH_TITLE"
                  defaultMessage="HOMEWORK_FORM_RIGTH_PATH_TITLE"
                />
              </H6>
            </Box>

            <Box mb={[4]}>
              <StyledLabel>
                <Label>Группа</Label>
                <Field
                  name="group"
                  validate={[required]}
                  component={RenderCustomSelect}
                  {...{
                    options: groupsList,
                    isSearchable: true,
                  }}
                />
              </StyledLabel>
            </Box>

            <Box mb={[4]}>
              <StyledLabel>
                <Label>Ученик(и)</Label>
                <Field
                  name="students"
                  validate={[required]}
                  component={RenderCustomSelect}
                  {...{
                    options: studentsList,
                    isMulti: true,
                    isSearchable: true,
                    closeMenuOnSelect: false,
                  }}
                />
              </StyledLabel>
            </Box>
            <Box mb={[4]}>
              <StyledLabel>
                <Label>Дата(ы)</Label>
                <Field
                  validate={[required]}
                  name="dates"
                  locale={locale}
                  component={RenderCustomDayPicker}
                />
              </StyledLabel>
            </Box>
            <Box mb={[4]}>
              <StyledLabel>
                <Label>Комментарий</Label>
                <InputStyled name="comment" type="text" component="input" />
              </StyledLabel>
            </Box>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

const selector = formValueSelector(HOMEWORK_FORM);

RigthPathForm.propTypes = {
  user: PropTypes.object.isRequired,
  loadData: PropTypes.func.isRequired,
  groups: PropTypes.array.isRequired,
  students: PropTypes.array.isRequired,
  selectedGroup: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchStudents());
    dispatch(fetchGroups());
  },
});

const mapStateToProps = state => ({
  user: state.user,
  groups: state.user.groups,
  students: state.user.students,
  selectedGroup: selector(state, 'group') ? selector(state, 'group') : {},
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: HOMEWORK_FORM,
  })(RigthPathForm)
);
