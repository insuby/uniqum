import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6, FormError } from 'UIKit/Fonts';
import { Field, formValueSelector } from 'redux-form';

import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import { email, required } from 'utils/Validate';
import RenderCustomField from 'components/Form/Input/RenderCustomField';
import RenderCustomSelect from 'components/Form/Input/RenderCustomSelect';
import Loading from 'components/Form/Loading';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAsterisk,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import styled from 'styled-components';

import {
  ROLE,
  REQUEST_GROUPS,
  fetchGroups,
  fetchTeachers,
  STUDENT_FORM,
} from 'modules/User';

const Icon = styled(FontAwesomeIcon)`
  float: right;
  margin-right: 10px;
  margin-top: -25px;
  position: relative;
  cursor: pointer;
  z-index: 2;
`;

/**
 * LeftPathform.
 */
class LeftPathform extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
    this.state = {
      showPassword: false,
    };
  }

  handleToggle = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      requests,
      groups,
      user,
      teachers,
      isNew,
      selectedTeacher,
    } = this.props;
    const { showPassword } = this.state;
    let optionsTeachers = [];

    const request = requests[REQUEST_GROUPS];

    if (teachers.length > 0) {
      optionsTeachers = teachers.map(item => ({
        value: item.id,
        label: [
          item.surname,
          `${String(item.name).charAt(0)}.`,
          `${String(item.patronymic).charAt(0)}.`,
        ].join(' '),
      }));
    }

    let optionsGroup = [];
    if (groups.length > 0) {
      if (selectedTeacher.value) {
        optionsGroup = groups
          .filter(item => item.creator_id._id === selectedTeacher.value)
          .map(item => ({
            value: item.id,
            label: item.name,
          }));
      } else {
        optionsGroup = groups.map(item => ({
          value: item.id,
          label: item.name,
        }));
      }
    }

    return (
      <Fragment>
        {request && (
          <div>
            {request.loading && <Loading />}
            {request.error && <FormError>request.errorMessage</FormError>}
            <Fragment>
              <Box py={[0, 4]}>
                <H6>
                  <FormattedMessage
                    id="STUDENT_FORM_LEFT_PATH_TITLE"
                    defaultMessage="STUDENT_FORM_LEFT_PATH_TITLE"
                  />
                </H6>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>
                    Email{' '}
                    <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
                  </Label>
                  <InputStyled
                    validate={[email, required]}
                    name="email"
                    type="text"
                    component={RenderCustomField}
                  />
                </StyledLabel>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>
                    Пароль{' '}
                    {isNew && (
                      <FontAwesomeIcon
                        icon={faAsterisk}
                        size="sm"
                        color="red"
                      />
                    )}
                  </Label>
                  <InputStyled
                    name="password"
                    type={showPassword ? 'text' : 'password'}
                    validate={isNew ? [required] : []}
                    component={RenderCustomField}
                  />
                  <Icon
                    icon={showPassword ? faEyeSlash : faEye}
                    onClick={this.handleToggle}
                  />
                </StyledLabel>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>Имя</Label>
                  <InputStyled name="name" type="text" component="input" />
                </StyledLabel>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>Фамилия</Label>
                  <InputStyled name="surname" type="text" component="input" />
                </StyledLabel>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>Отчество</Label>
                  <InputStyled
                    name="patronymic"
                    type="text"
                    component="input"
                  />
                </StyledLabel>
              </Box>
              {[ROLE.SYSTEM, ROLE.ADMIN].indexOf(user.role) > -1 && (
                <Box mb={[4]}>
                  <StyledLabel>
                    <Label>
                      Педагог{' '}
                      <FontAwesomeIcon
                        icon={faAsterisk}
                        size="sm"
                        color="red"
                      />
                    </Label>
                    <Field
                      name="creator_id"
                      validate={[required]}
                      component={RenderCustomSelect}
                      {...{
                        options: optionsTeachers,
                        isSearchable: true,
                      }}
                    />
                  </StyledLabel>
                </Box>
              )}
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>
                    Группа{' '}
                    <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
                  </Label>
                  <Field
                    name="groups"
                    validate={[required]}
                    component={RenderCustomSelect}
                    {...{
                      options: optionsGroup,
                      isMulti: true,
                      isSearchable: true,
                      closeMenuOnSelect: false,
                    }}
                  />
                </StyledLabel>
              </Box>
              <Box mb={[4]}>
                <StyledLabel>
                  <Label>Дата рождения</Label>
                  <InputStyled name="birthday" type="date" component="input" />
                </StyledLabel>
              </Box>
            </Fragment>
          </div>
        )}
      </Fragment>
    );
  }
}

const selector = formValueSelector(STUDENT_FORM);

LeftPathform.propTypes = {
  user: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
  loadData: PropTypes.func.isRequired,
  groups: PropTypes.array.isRequired,
  teachers: PropTypes.array.isRequired,
  isNew: PropTypes.bool.isRequired,
  selectedTeacher: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchGroups());
    dispatch(fetchTeachers());
  },
});

const mapStateToProps = state => ({
  user: state.user,
  requests: state.requests,
  groups: state.user.groups,
  teachers: state.user.teachers,
  selectedTeacher: selector(state, 'creator_id')
    ? selector(state, 'creator_id')
    : {},
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftPathform);
