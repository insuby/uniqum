import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

// Components
import { Flex, Box } from 'UIKit/Grid';
import Panel from 'components/Panel';

import { reduxForm } from 'redux-form';
import { STUDENT_FORM } from 'modules/User';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import LeftPathform from './LeftPathform';
import RigthPathForm from './RigthPathForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

const StudentForm = ({ isNew }) => (
  <Fragment>
    <Flex flexWrap="wrap" mx={-4}>
      <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
        <Panel style={{ overflow: 'inherit' }}>
          <Box px={[4, 8]} py={[3, 6]}>
            <LeftPathform isNew={isNew} />
          </Box>
        </Panel>
      </Box>
      <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
        <Panel>
          <Box px={[4, 8]} py={[3, 6]}>
            <RigthPathForm />
          </Box>
        </Panel>
      </Box>
    </Flex>
  </Fragment>
);

StudentForm.propTypes = {
  isNew: PropTypes.bool,
};

StudentForm.defaultProps = {
  isNew: false,
};

export default reduxForm({
  form: STUDENT_FORM,
})(StudentForm);
