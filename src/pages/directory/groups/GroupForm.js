import React, { Fragment } from 'react';

// Components
import { Flex, Box } from 'UIKit/Grid';
import Panel from 'components/Panel';

import { reduxForm } from 'redux-form';
import { GROUP_FORM } from 'modules/User';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

import LeftPathform from './LeftPathform';
import RigthPathForm from './RigthPathForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

const GroupForm = () => (
  <Fragment>
    <Flex flexWrap="wrap" mx={-4}>
      <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
        <Panel>
          <Box px={[4, 8]} py={[3, 6]}>
            <LeftPathform />
          </Box>
        </Panel>
      </Box>
      <Box w={[1, 1, 1 / 2, 1 / 2]} px={4} pb={[4, 8]}>
        <Panel>
          <Box px={[4, 8]} py={[3, 6]}>
            <RigthPathForm />
          </Box>
        </Panel>
      </Box>
    </Flex>
  </Fragment>
);

export default reduxForm({
  form: GROUP_FORM,
})(GroupForm);
