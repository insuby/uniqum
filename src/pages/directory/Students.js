import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import moment from 'moment';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

// Components
import Table from '@material-ui/core/Table';
import Tooltip from '@material-ui/core/Tooltip';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

import { Flex, Box } from 'UIKit/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import Panel from 'components/Panel';
import { H3, FormError } from 'UIKit/Fonts';

import Button from '@material-ui/core/Button';
import { StyledButtonIcon } from 'UIKit/Form';
import Loading from 'components/Form/Loading';
import { combineRequests } from 'modules/Requests';
import {
  ROLE,
  fetchStudents,
  fetchStudentDelete,
  fetchRequestSendData,
  REQUEST_STUDENTS,
  REQUEST_STUDENT_DELETE,
  REQUEST_SEND_DATA_TO_EMAIL,
} from 'modules/User';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';
import LinkIconStyled from 'components/Layout/LinkIconStyled';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import SearchIcon from '@material-ui/icons/Search';

// Styles
import colors from 'UIKit/Colors';
import styled from 'styled-components';

const Icon = styled(FontAwesomeIcon)`
  cursor: pointer;
`;

const StyledTable = styled(Table)`
  font-size: 16px;
`;
const StyledTableCell = styled(TableCell)`
  background-color: ${colors.brand['400']};
  font-size: 10px;
  font-weight: bold;
  letter-spacing: 1.33px;
  color: ${colors.brand['800']} !important
  text-transform: uppercase;
`;

const WhiteSpaceNowrap = styled.div`
  white-space: nowrap;
`;

export const ButtonStyled = styled(Button)`
  width: 72px;
  height: 72px;
  border-radius: 100% !important;
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.16);
  font-size: 28px !important;
  text-decoration: none !important;
`;

const calculateAge = birthday => {
  const ageDifMs = Date.now() - new Date(birthday).getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
};

const DATE_FORMAT = 'DD.MM.YYYY';

/**
 * Clients page.
 * @param {object} values values.
 */
class Students extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();

    const columns = [
      { name: 'group', title: 'Группа(ы)' },
      { name: 'email', title: 'Email ученика' },
      { name: 'fio', title: 'Имя Фамилия ученика' },
      { name: 'created', title: 'Возраст' },
      { name: 'parent', title: 'Email родителя' },
      { name: 'fio_parent', title: 'Имя Фамилия родителя' },
      { name: 'lang', title: 'Дата регистрации' },
      { name: 'edit', title: 'Операции' },
    ];

    this.state = {
      page: 0,
      columns,
      search: '',
      init: false,
      rowsPerPage: 10,
    };
  }

  /**
   * Life cycle
   * @param {object} nextProps nextProps.
   */
  componentWillReceiveProps(nextProps) {
    const { user } = nextProps;
    const { columns, init } = this.state;

    if (!init) {
      if (user.role === ROLE.SYSTEM) {
        columns.splice(0, 0, { name: 'school', title: 'Школа' });
        columns.splice(1, 0, { name: 'teacher', title: 'Педагог' });
        this.setState({ columns, init: true });
      }
      if (user.role === ROLE.ADMIN) {
        columns.splice(0, 0, { name: 'teacher', title: 'Педагог' });
        this.setState({ columns, init: true });
      }
    }
  }

  /**
   * @param {string} id studentId.
   */
  handleDelete = id => {
    const { onDelete } = this.props;
    onDelete(id);
  };

  /**
   * @param {object} event event.
   * @param {string} page page.
   */
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  /**
   * @param {object} event event.
   */
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  /**
   * @param {string} id id.
   * @param {string} email email.
   */
  handleSendDataToEmail = (id, email) => {
    const { onSendData } = this.props;
    onSendData({ id, email });
  };

  /**
   * @param {object} event event.
   */
  handleSearch = event => {
    this.setState({ search: event.target.value });
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests, students, user } = this.props;
    const { columns, rowsPerPage, page, search } = this.state;

    const request = combineRequests([
      requests[REQUEST_STUDENTS],
      requests[REQUEST_STUDENT_DELETE],
      requests[REQUEST_SEND_DATA_TO_EMAIL],
    ]);

    const studentsList = students.filter(
      i =>
        String(i.name)
          .toUpperCase()
          .indexOf(search.toUpperCase()) > -1 ||
        String(i.surname)
          .toUpperCase()
          .indexOf(search.toUpperCase()) > -1 ||
        String(i.patronymic)
          .toUpperCase()
          .indexOf(search.toUpperCase()) > -1 ||
        i.email.toUpperCase().indexOf(search.toUpperCase()) > -1 ||
        (
          i.groups.length > 0 &&
          i.groups.filter(
            group =>
              String(group.name)
                .toUpperCase()
                .indexOf(search.toUpperCase()) > -1
          )
        ).length > 0 ||
        (i.creator_id &&
          (String(i.creator_id.name)
            .toUpperCase()
            .indexOf(search.toUpperCase()) > -1 ||
            String(i.creator_id.surname)
              .toUpperCase()
              .indexOf(search.toUpperCase()) > -1 ||
            String(i.creator_id.patronymic)
              .toUpperCase()
              .indexOf(search.toUpperCase()) > -1)) ||
        (i.creator_id &&
          i.creator_id.creator_id &&
          i.creator_id.creator_id.school &&
          String(i.creator_id.creator_id.school.name)
            .toUpperCase()
            .indexOf(search.toUpperCase()) > -1)
    );

    return (
      <Panel style={{ overflow: 'auto' }}>
        <Box px={4} py={(15, 5)}>
          {request && (
            <div>
              <Box px={[0]} py={[0, 4]}>
                <Grid
                  container
                  spacing={8}
                  justify="space-between"
                  alignItems="flex-end"
                >
                  <Grid item>
                    <H3>
                      <FormattedMessage
                        id="STUDENT_TITLE"
                        defaultMessage="STUDENT_TITLE"
                      />
                    </H3>
                  </Grid>
                  <Grid item>
                    <Grid container spacing={8} alignItems="flex-end">
                      <Grid item>
                        <SearchIcon />
                      </Grid>
                      <Grid item>
                        <TextField
                          id="input-with-icon-grid"
                          label="Поиск"
                          onChange={this.handleSearch}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Box>
              {request.loading && <Loading />}
              {request.error && (
                <FormError>
                  <FormattedMessage
                    id={request.errorMessage}
                    defaultMessage={request.errorMessage}
                  />
                </FormError>
              )}
              {studentsList && (
                <Flex flexWrap="wrap" mx={-4} mb={4}>
                  <Box px={4} w={1}>
                    <Panel table>
                      <Box>
                        <StyledTable>
                          <TableHead>
                            <TableRow>
                              {columns.map(column => (
                                <StyledTableCell key={column.name}>
                                  {column.title}
                                </StyledTableCell>
                              ))}
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {studentsList
                              .slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                              )
                              .map(row => (
                                <TableRow key={row.id}>
                                  {/* <TableCell component="th" scope="row">
                                    {row.id}
                                  </TableCell> */}
                                  {user.role === ROLE.ADMIN && (
                                    <TableCell>
                                      {row.creator_id &&
                                        [
                                          row.creator_id.name,
                                          row.creator_id.surname,
                                          row.creator_id.patronymic,
                                        ].join(' ')}
                                    </TableCell>
                                  )}
                                  {user.role === ROLE.SYSTEM && (
                                    <Fragment>
                                      <TableCell>
                                        {row.creator_id &&
                                          row.creator_id.creator_id &&
                                          row.creator_id.creator_id.school &&
                                          row.creator_id.creator_id.school.name}
                                      </TableCell>
                                      <TableCell>
                                        {row.creator_id &&
                                          [
                                            row.creator_id.name,
                                            row.creator_id.surname,
                                            row.creator_id.patronymic,
                                          ].join(' ')}
                                      </TableCell>
                                    </Fragment>
                                  )}
                                  <TableCell>
                                    {row.groups.map(i => i.name).join('; ')}
                                  </TableCell>
                                  <TableCell>
                                    <WhiteSpaceNowrap>
                                      {row.email}{' '}
                                      <Tooltip
                                        title="Отправить логин и пароль пользователя на этот email"
                                        placement="right"
                                      >
                                        <Icon
                                          icon={faPaperPlane}
                                          size="sm"
                                          onClick={() =>
                                            this.handleSendDataToEmail(
                                              row.id,
                                              row.email
                                            )
                                          }
                                        />
                                      </Tooltip>
                                    </WhiteSpaceNowrap>
                                  </TableCell>
                                  <TableCell>
                                    {[row.name, row.surname].join(' ')}
                                  </TableCell>
                                  <TableCell>
                                    {calculateAge(row.birthday)} лет
                                  </TableCell>
                                  <TableCell>
                                    <WhiteSpaceNowrap>
                                      {row.parent && row.parent.email && (
                                        <Fragment>
                                          {row.parent.email}{' '}
                                          <Tooltip
                                            title="Отправить логин и пароль пользователя на этот email"
                                            placement="right"
                                          >
                                            <Icon
                                              icon={faPaperPlane}
                                              size="sm"
                                              onClick={() =>
                                                this.handleSendDataToEmail(
                                                  row.id,
                                                  row.parent.email
                                                )
                                              }
                                            />
                                          </Tooltip>
                                        </Fragment>
                                      )}
                                    </WhiteSpaceNowrap>
                                  </TableCell>
                                  <TableCell>
                                    {row.parent &&
                                      [
                                        row.parent.name,
                                        row.parent.surname,
                                      ].join(' ')}
                                  </TableCell>
                                  <TableCell>
                                    {moment(row.created_at).format(DATE_FORMAT)}
                                  </TableCell>
                                  <TableCell>
                                    <Tooltip
                                      title="Редактировать данные ученика"
                                      placement="right"
                                    >
                                      <LinkIconStyled
                                        to={`/directory/students/edit/${row._id}`}
                                      >
                                        <Edit />
                                      </LinkIconStyled>
                                    </Tooltip>{' '}
                                    <Tooltip
                                      title="Удалить ученика"
                                      placement="right"
                                    >
                                      <StyledButtonIcon
                                        type="button"
                                        onClick={() =>
                                          this.handleDelete(row.id)
                                        }
                                        del="true"
                                      >
                                        <Delete
                                          style={{
                                            color: 'rgba(0, 0, 0, 0.54)',
                                          }}
                                        />
                                      </StyledButtonIcon>
                                    </Tooltip>
                                  </TableCell>
                                </TableRow>
                              ))}
                          </TableBody>
                        </StyledTable>
                      </Box>
                      <TablePagination
                        rowsPerPageOptions={[10, 25, 50, 100]}
                        component="div"
                        count={studentsList.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        backIconButtonProps={{
                          'aria-label': 'Предыдущая страница',
                        }}
                        nextIconButtonProps={{
                          'aria-label': 'Следующая страница',
                        }}
                        onChangePage={this.handleChangePage}
                        labelRowsPerPage="Строк на странице"
                        labelDisplayedRows={({ from, to, count }) =>
                          `${from}-${to} из ${count}`
                        }
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                      />
                    </Panel>
                  </Box>
                </Flex>
              )}
            </div>
          )}
          <Flex justifyContent="center" pb={10}>
            <Tooltip title="Добавить ученика" placement="right">
              <ButtonStyled
                variant="raised"
                className="text-white"
                aria-label="Добавить ученика"
                component={Link}
                onClick={() => console.log()}
                style={{ textDecoration: 'underline' }}
                to="/directory/students/create"
              >
                <FontAwesomeIcon icon="plus" />
              </ButtonStyled>
            </Tooltip>
          </Flex>
        </Box>
      </Panel>
    );
  }
}

Students.propTypes = {
  loadData: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onSendData: PropTypes.func.isRequired,
  students: PropTypes.array.isRequired,
  requests: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
  students: state.user.students,
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchStudents());
  },
  onDelete(id) {
    dispatch(fetchStudentDelete(id));
  },
  onSendData(formData) {
    dispatch(fetchRequestSendData(formData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Students);
