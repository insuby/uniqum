import React, { Fragment } from 'react';

import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';

const RigthPathForm = () => (
  <Fragment>
    <Box py={[0, 4]}>
      <H6>
        <FormattedMessage
          id="GROUP_FORM_RIGTH_PATH_TITLE"
          defaultMessage="GROUP_FORM_RIGTH_PATH_TITLE"
        />
      </H6>
    </Box>
  </Fragment>
);

export default RigthPathForm;
