import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import {
  fetchTeachers,
  fetchTeacherEdit,
  TEACHER_FORM,
  REQUEST_TEACHER_EDIT,
} from 'modules/User';
import { FormattedMessage } from 'react-intl';

// Components
import { Box } from 'UIKit/Grid';
import Panel from 'components/Panel';
import { H3, FormError, FormSuccess } from 'UIKit/Fonts';

import Loading from 'components/Form/Loading';

import colors from 'UIKit/Colors';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import TeacherForm from './TeacherForm';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

/**
 * EditTeacher page.
 * @param {object} values values.
 */
class EditTeacher extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
    this.state = {
      query: false,
    };
  }

  /**
   * @param {object} formData formData.
   */
  onSubmit = formData => {
    const {
      onEdit,
      match: {
        params: { id },
      },
    } = this.props;
    this.setState({ query: true }, () => onEdit(id, formData));
  };

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      requests,
      handleSubmit,
      match: {
        params: { id },
      },
      teachers,
    } = this.props;
    const { query } = this.state;
    const request = requests[REQUEST_TEACHER_EDIT];
    return (
      <Box w={1} px={4}>
        <Panel>
          {teachers && (
            <Fragment>
              <Box px={[1, 8]} py={[0, 4]}>
                <H3>
                  {request && query && (
                    <Fragment>
                      {request.loading && <Loading />}
                      {request.error && (
                        <FormError>
                          {' '}
                          <FormattedMessage
                            id={request.errorMessage}
                            defaultMessage={request.errorMessage}
                          />
                        </FormError>
                      )}
                      {request.data && (
                        <FormSuccess>
                          <FormattedMessage
                            id="TEACHER_SUCCESS_SAVE_MESSAGE"
                            defaultMessage="TEACHER_SUCCESS_SAVE_MESSAGE"
                          />
                        </FormSuccess>
                      )}
                    </Fragment>
                  )}
                  <FormattedMessage
                    id="TEACHER_TABLE_SAVE_TITLE"
                    defaultMessage="TEACHER_TABLE_SAVE_TITLE"
                  />
                </H3>
              </Box>
              <Box px={[4, 8]} py={[3, 6]}>
                <form onSubmit={handleSubmit(this.onSubmit)}>
                  <TeacherForm
                    initialValues={
                      teachers
                        .filter(i => i.id === id)
                        .map(j => ({
                          ...j,
                          ...{
                            creator_id: {
                              value: j.creator_id.id,
                              label:
                                j.creator_id.school && j.creator_id.school.name,
                            },
                          },
                        }))[0]
                    }
                  />
                  <ButtonStyled fullWidth type="submit">
                    <FormattedMessage
                      id="TEACHER_TABLE_SAVE"
                      defaultMessage="TEACHER_TABLE_SAVE"
                    />
                  </ButtonStyled>
                </form>
              </Box>
            </Fragment>
          )}
        </Panel>
      </Box>
    );
  }
}

EditTeacher.propTypes = {
  requests: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  teachers: PropTypes.array.isRequired,
  onEdit: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(fetchTeachers());
  },
  onEdit(id, formData) {
    dispatch(fetchTeacherEdit(id, formData));
  },
});

const mapStateToProps = state => ({
  requests: state.requests,
  teachers: state.user.teachers,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: TEACHER_FORM,
  })(EditTeacher)
);
