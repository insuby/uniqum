import React, { Fragment } from 'react';

import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';

import { StyledLabel, Label, InputStyled } from 'UIKit/Form';

const RigthPathForm = () => (
  <Fragment>
    <Box py={[0, 4]}>
      <H6>
        <FormattedMessage
          id="SCHOOL_FORM_RIGTH_PATH_TITLE"
          defaultMessage="SCHOOL_FORM_RIGTH_PATH_TITLE"
        />
      </H6>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Название</Label>
        <InputStyled name="school[name]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Город</Label>
        <InputStyled name="school[city]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Адрес</Label>
        <InputStyled name="school[address]" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Станция метро</Label>
        <InputStyled
          name="school[metro_station]"
          type="text"
          component="input"
        />
      </StyledLabel>
    </Box>
  </Fragment>
);

export default RigthPathForm;
