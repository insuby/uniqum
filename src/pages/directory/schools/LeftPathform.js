import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Box } from 'UIKit/Grid';
import { H6 } from 'UIKit/Fonts';

import { email, required } from 'utils/Validate';
import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { StyledLabel, Label, InputStyled } from 'UIKit/Form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAsterisk } from '@fortawesome/free-solid-svg-icons';

const LeftPathform = ({ isNew }) => (
  <Fragment>
    <Box py={[0, 4]}>
      <H6>
        <FormattedMessage
          id="SCHOOL_FORM_LEFT_PATH_TITLE"
          defaultMessage="SCHOOL_FORM_LEFT_PATH_TITLE"
        />
      </H6>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>
          Email <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />
        </Label>
        <InputStyled
          validate={[email, required]}
          name="email"
          type="text"
          component={RenderCustomField}
        />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>
          Пароль{' '}
          {isNew && <FontAwesomeIcon icon={faAsterisk} size="sm" color="red" />}
        </Label>
        <InputStyled
          name="password"
          type="text"
          validate={isNew && [required]}
          component={RenderCustomField}
        />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Имя</Label>
        <InputStyled name="name" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Фамилия</Label>
        <InputStyled name="surname" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Отчество</Label>
        <InputStyled name="patronymic" type="text" component="input" />
      </StyledLabel>
    </Box>
    <Box mb={[4]}>
      <StyledLabel>
        <Label>Телефон</Label>
        <InputStyled name="phone" type="text" component="input" />
      </StyledLabel>
    </Box>
  </Fragment>
);

LeftPathform.propTypes = {
  isNew: PropTypes.bool.isRequired,
};

export default LeftPathform;
