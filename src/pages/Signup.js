import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import cookie from 'react-cookies';

import { types, actions } from 'modules/Guest';

// Components
import { H1Panel, H2Panel, FormError } from 'UIKit/Fonts';
import { Box } from 'UIKit/Grid';
import Loading from 'components/Form/Loading';
import SuccessMessage from 'components/SuccessMessage';
import RegistrationForm from 'pages/signup/RegistrationForm';

/**
 * Signup page.
 * @param {object} values values.
 */
class Signup extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * @param {object} formData данные формы.
   */
  handleSubmit(formData) {
    const { registration } = this.props;
    registration(formData);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[types.REQUEST_REGISTRATION];
    const referer = cookie.load('referer');

    return (
      <Fragment>
        <Box mb={8}>
          <Box mb={1}>
            <H1Panel>
              <FormattedMessage id="signup.title" defaultMessage="" />
            </H1Panel>
          </Box>
          {!(request && request.success) && (
            <H2Panel>
              <FormattedMessage id="signup.subtitle" defaultMessage="" />
            </H2Panel>
          )}
        </Box>
        {request && (
          <div>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                <FormattedMessage id={request.errorMessage} defaultMessage="" />
              </FormError>
            )}
          </div>
        )}
        {request && request.success ? (
          <SuccessMessage
            message={
              <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                id="signup.success_message"
                defaultMessage=""
              />
            }
          />
        ) : (
          <RegistrationForm
            onSubmit={this.handleSubmit}
            initialValues={{ referer }}
          />
        )}
      </Fragment>
    );
  }
}

Signup.propTypes = {
  requests: PropTypes.object.isRequired,
  registration: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  registration(formData) {
    dispatch(actions.fetchRegistration(formData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
