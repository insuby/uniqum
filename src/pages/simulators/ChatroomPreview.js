import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

const Wrapper = styled.div`
  cursor: pointer;
`;

const getCardTitleStyle = () => ({
  textAlign: 'center',
  color: 'white',
  fontSize: '40px',
  fontWeight: 900,
});

const styles = {
  card: {
    width: '100%',
    marginBottom: 40,
    overflow: 'initial',
  },
  media: {
    height: 200,
  },
  content: {
    alignItems: 'center',
    opacity: 0.6,
    background: '#212121',
  },
};

const ChatroomPreview = props => {
  const { user, history, chatroom, classes } = props;

  const handleClick = () => {
    if (Object.keys(user.user).length === 0) {
      return history.push('/simulators');
    }
    return history.push(`/cabinet/${chatroom._id}`);
  };

  return (
    <Paper className={classes.card} elevation={5}>
      <Wrapper onClick={handleClick}>
        <Card className={classes.card}>
          <CardMedia className={classes.media} image={chatroom.image} title="">
            <CardContent className={classes.content}>
              <Typography variant="h1" style={getCardTitleStyle()}>
                {chatroom.icon ? (
                  <table style={{ width: '100%' }}>
                    <tbody>
                      <tr>
                        <td style={{ textAlign: 'right', width: '50%' }}>
                          <img alt="" src={chatroom.icon} width="50px" />
                        </td>
                        <td style={{ textAlign: 'left', width: '50%' }}>
                          {chatroom.desc}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                ) : (
                  chatroom.desc
                )}
              </Typography>
            </CardContent>
          </CardMedia>
        </Card>
      </Wrapper>
    </Paper>
  );
};

ChatroomPreview.propTypes = {
  chatroom: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  socket: state.socket,
  user: state.user,
});

const mapDispatchToProps = () => ({});

export default withRouter(
  withStyles(styles)(
    connect(mapStateToProps, mapDispatchToProps)(ChatroomPreview)
  )
);
