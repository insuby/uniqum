import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Field, reduxForm, formValueSelector } from 'redux-form';

import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { required } from 'utils/Validate';
import { FORM_MATHS, types, actions } from 'modules/Tasks';

// styled
import { FormGroup, Label, FieldGroup } from 'UIKit/Form';
import { FormError } from 'UIKit/Fonts';
import Loading from 'components/Form/Loading';
import colors from 'UIKit/Colors';
import styled from 'styled-components';

export const ButtonStyled = styled(Button)`
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
`;

export const SelectStyled = styled(Field)`
  width: 100%;
  height: 36px;
  padding: 0.625rem;
  border: 0;
  text-transform: uppercase;
  text-align: right;
  background-color: #fff;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &:not(:last-child) {
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  &:not(:first-child) {
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
  }
`;

const onSubmit = (formData, dispatch) => {
  dispatch(actions.fetchSaveSettings(formData));
};

/**
 * Отрисовка формы для настройки тренажера по математике
 * @param {object} values values.
 */
class MathsForm extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    props.loadData();
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      invalid,
      pristine,
      submitting,
      handleSubmit,
      requests,
    } = this.props;
    const request = requests[types.REQUEST_LOAD_DEVICES];

    return (
      <Fragment>
        {request && (
          <Fragment>
            {request.loading && <Loading />}
            {request.error && <FormError>{request.errorMessage}</FormError>}
            {request.data && (
              <form onSubmit={handleSubmit}>
                <FormGroup>
                  <Label>Планшет или устройство</Label>
                  <FieldGroup>
                    <SelectStyled name="imei" component="select">
                      {request.data.map(d => (
                        <option key={d.imei} value={d.imei}>
                          {`${d.platform.brand} ${d.platform.model}`}
                        </option>
                      ))}
                    </SelectStyled>
                  </FieldGroup>
                </FormGroup>
                <FormGroup>
                  <Field
                    name="min"
                    type="number"
                    label="Минимальное значение каждого слагаемого"
                    component={RenderCustomField}
                    validate={[required]}
                  />
                </FormGroup>
                <FormGroup>
                  <Field
                    name="max"
                    component={RenderCustomField}
                    type="number"
                    label="Максимальное значение каждого слагаемого"
                    validate={[required]}
                  />
                </FormGroup>
                <FormGroup>
                  <Field
                    name="summands"
                    component={RenderCustomField}
                    type="number"
                    label="Количество слагаемые в примере"
                    validate={[required]}
                  />
                </FormGroup>
                <FormGroup>
                  <Field
                    name="coefficient"
                    component={RenderCustomField}
                    type="number"
                    label="Сколько примеров необходимо решить ребенку за 1 минуту пользования планшетом."
                    validate={[required]}
                  />
                </FormGroup>
                <ButtonStyled
                  fullWidth
                  type="submit"
                  disabled={pristine || submitting || invalid}
                >
                  Изменить настройки
                </ButtonStyled>
              </form>
            )}
          </Fragment>
        )}
      </Fragment>
    );
  }
}

MathsForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  requests: PropTypes.object.isRequired,
  loadData: PropTypes.func.isRequired,
};

const selector = formValueSelector('maths');

const mapStateToProps = state => {
  const imei = selector(state, 'imei');
  const data =
    state.requests[types.REQUEST_LOAD_DEVICES] &&
    state.requests[types.REQUEST_LOAD_DEVICES].data;

  let initialValues = {};
  if (data && data.length > 0) {
    if (!imei) {
      initialValues = { ...data[0].settings, imei: data[0].imei };
    } else {
      initialValues = {
        ...data.filter(s => s.imei === imei)[0].settings,
        imei,
      };
    }
  }

  return {
    initialValues,
    requests: state.requests,
  };
};

const mapDispatchToProps = dispatch => ({
  loadData() {
    dispatch(actions.fetchLoadDevices());
  },
  saveSettings(formData) {
    dispatch(actions.fetchSaveSettings(formData));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: FORM_MATHS,
    onSubmit,
    enableReinitialize: true,
  })(MathsForm)
);

// export default reduxForm({
//   form: 'maths',
//   onSubmit,
// })(MathsForm);
// const selector = formValueSelector('selectingFormValues')
// SelectingFormValuesForm = connect(state => {
//   const hasEmailValue = selector(state, 'hasEmail')
//   const favoriteColorValue = selector(state, 'favoriteColor')
//   const { firstName, lastName } = selector(state, 'firstName', 'lastName')
//   return {
//     hasEmailValue,
//     favoriteColorValue,
//     fullName: `${firstName || ''} ${lastName || ''}`
//   }
// })(SelectingFormValuesForm)

// const mapStateToProps = state => ({
//   requests: state.requests,
//   user: state.user,
//   initialValues: { to: state.user.pay_acc },
// });
