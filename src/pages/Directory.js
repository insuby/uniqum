import React from 'react';
import Panel from 'components/Panel';
import { Box } from 'UIKit/Grid';
import { FormattedMessage } from 'react-intl';

const Directory = () => (
  <Panel>
    <Box px={4} py={(15, 5)}>
      Главная страница справочников
    </Box>
  </Panel>
);

export default Directory;
