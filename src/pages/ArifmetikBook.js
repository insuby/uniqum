import React, { useReducer, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Panel from 'components/Panel';
import Grid from '@material-ui/core/Grid';
// Components
import { Box } from 'UIKit/Grid';

import ControlPanel from 'components/Dashboard/teacher/ControlPanel';
import { actions, types } from 'modules/ArifmetikBook';
import WrapTerminal from './arifmetikbook/WrapTerminal';
import SimulatorParams from '../../vendors/simulators/src/SimulatorParams';

const initialState = {
  number: 3,
  numbers: [0, 1, 2],
  open: true,

  room: 'roomB',
  controls: {
    count: 5,
    amount: 3,
    dictation: false,
    start: false,
    stop: false,
    check: false,
    clear: false,
    abacus: false,
    settings: true,
    repeat: false,
    correct: false,
    open: false,
    sound: false,
    sample: false,
    showAnswers: false,
  },
  params: {
    section: 5,
    steps: 3,
    interval: 1,
    mode: 1,
    sign: 0,
    bound: 0,
    under: 1,
    over: 1,
    numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
  },
  dictationState: {
    dictation: false,
    amount: 3,
    start: false,
    progress: false,
    stop: false,
    period: 5,
    samples: [],
  },
  visible: {
    answerCounter: {
      sound: true,
      settings: true,
      sample: true,
      play: true,
      params: true,
      abacus: true,
      showAnswers: true,
    },
  },
};

function reducer(state, action) {
  switch (action.type) {
    case types.SET_ROOM:
    case types.SET_PARAMS:
    case types.SET_CONTROLS:
    case types.SET_DICTATION_STATE: {
      return {
        ...state,
        [action.meta]: action.payload,
      };
    }
    case 'open': {
      const { open } = state;
      return {
        ...state,
        open: !open,
      };
    }

    case types.SET_NUMBER: {
      const number = action.payload;
      const numbers = new Array(number).fill().map((a, i) => i);

      return {
        ...state,
        number,
        numbers,
      };
    }
    default:
      return state;
  }
}

const ArifmetikBook = ({ requests, fetchArgs }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const {
    params,
    number,
    numbers,
    controls,
    room,
    open,
    dictationState,
  } = state;
  const { dictation, amount } = dictationState;

  /*
   * Ref
   */
  const controlsRef = useRef(controls);
  const dictationStateRef = useRef(dictationState);

  /*
   * End Ref
   */

  useEffect(() => {
    controlsRef.current = controls;
  }, [controls]);

  useEffect(() => {
    dictationStateRef.current = dictationState;
  }, [dictationState]);

  const handleStart = () => {
    let count = number;
    if (dictation) {
      count = number * amount;
    }
    fetchArgs(count, params);
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...controlsRef.current,
        start: true,
        stop: false,
        check: false,
      },
    });
  };

  const handleStop = () => {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...controlsRef.current,
        start: false,
        stop: true,
        check: false,
      },
    });
  };

  const handleCheck = () => {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...controlsRef.current,
        start: false,
        stop: false,
        check: true,
      },
    });
  };

  const handleChange = (event, name) => {
    dispatch({
      type: types.SET_DICTATION_STATE,
      meta: 'dictationState',
      payload: {
        ...dictationStateRef.current,
        [name]: event.target.value,
      },
    });
  };

  const handleChangeNumber = event => {
    dispatch({
      type: types.SET_NUMBER,
      payload: +event.target.value,
    });
  };

  const handleChangeRoom = event => {
    dispatch({
      type: types.SET_ROOM,
      meta: 'room',
      payload: event.target.value,
    });
  };

  const handleChecked = (event, name) => {
    dispatch({
      type: types.SET_DICTATION_STATE,
      meta: 'dictationState',
      payload: { ...dictationStateRef.current, [name]: event.target.checked },
    });
  };

  const handleCheckedControls = (event, name) => {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: {
        ...controlsRef.current,
        [name]: event.target.checked,
      },
    });
  };

  const handleChangeParams = nextParams => {
    dispatch({
      type: types.SET_PARAMS,
      meta: 'params',
      payload: nextParams,
    });
  };

  const handleShowParamsBlock = () => {
    dispatch({ type: 'open' });
  };

  const request = requests[types.REQUEST_GET_ARGS];
  const size = Math.round(numbers.length < 6 ? 12 / numbers.length : 2);
  let i = 0;
  return (
    <Panel>
      <Box px={[4, 8]} py={[3, 6]}>
        <SimulatorParams
          onChangeParams={handleChangeParams}
          params={params}
          room={room}
          onShowParamsBlock={handleShowParamsBlock}
          open={open}
        />
      </Box>
      <Box px={[4, 8]}>
        <ControlPanel
          request={request || {}}
          open={open}
          state={state}
          onStop={handleStop}
          onCheck={handleCheck}
          onStart={handleStart}
          onChange={handleChange}
          onChecked={handleChecked}
          onChangeNumber={handleChangeNumber}
          onChangeRoom={handleChangeRoom}
          onShowSettings={handleShowParamsBlock}
          onCheckedControls={handleCheckedControls}
          visible={{
            number: true,
            room: true,
            trainer: false,
          }}
        />
      </Box>
      <Box textAlign="center" px={[4, 8]} py={[3, 6]}>
        <Grid justify="center" spacing={1} container>
          {numbers.map(num => (
            <Grid item xs={size} lg={size} sm={size} md={size} key={(i += 1)}>
              <WrapTerminal state={state} num={num} parentRequest={request} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Panel>
  );
};

ArifmetikBook.propTypes = {
  requests: PropTypes.object.isRequired,
  fetchArgs: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  fetchArgs(terminalCount, params) {
    dispatch(actions.fetchArgs(terminalCount, { params }));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ArifmetikBook);
