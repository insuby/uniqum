import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import Reaptcha from 'reaptcha';
import RenderCustomField from 'components/Form/Input/RenderCustomField';
import { email, required, minLength4, passwordMatch } from 'utils/Validate';

// styled
import { Box } from 'UIKit/Grid';
import { FormGroup, StyledButton } from 'UIKit/Form';

const { RECAPTCHA_PUBLIC } = process.env;

/**
 * RegistrationForm.
 */
class RegistrationForm extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.captcha = React.createRef();
    this.onVerify = this.onVerify.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Функция выполняется если пользователь успешно прошел проверку recaptcha.
   * @param {object} gretoken токен recaptcha.
   * @param {object} params парметры компонента Field.
   */
  onVerify(gretoken, params) {
    const { handleSubmit } = this.props;
    params.input.onChange(gretoken);
    handleSubmit();
    if (this.captcha.current.rendered) {
      this.captcha.current.reset();
    }
  }

  /**
   * Выполняется при submit-е формы, запускает проверку невидимой recaptcha.
   * @param {object} event event.
   */
  handleSubmit(event) {
    event.preventDefault();
    this.captcha.current.execute();
  }

  /**
   * @returns {*}
   */
  render() {
    const { invalid, pristine, submitting } = this.props;

    return (
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          <Field name="referer" type="hidden" component="input" />
          <FormGroup>
            <Field
              label="Email"
              name="email"
              component={RenderCustomField}
              type="text"
              placeholder="Email"
              validate={[required, email]}
            />
          </FormGroup>
          <FormGroup>
            <Field
              label="Password"
              name="password"
              component={RenderCustomField}
              type="password"
              placeholder="Password"
              validate={[required, minLength4]}
            />
          </FormGroup>
          <FormGroup>
            <Field
              label="Confirm password"
              name="new_password"
              component={RenderCustomField}
              type="password"
              placeholder="Confirm password"
              validate={[required, passwordMatch]}
            />
          </FormGroup>
          <FormGroup>
            <Field
              label="Я согласен с Политикой конфиденциальности и Правилами использования"
              name="private_policy"
              component={RenderCustomField}
              type="checkbox"
              validate={required}
            />
          </FormGroup>
          <Field
            name="captcha"
            component={params => (
              <Reaptcha
                ref={this.captcha}
                sitekey={RECAPTCHA_PUBLIC}
                onVerify={gretoken => this.onVerify(gretoken, params)}
                size="invisible"
              />
            )}
          />
          <Box mt={10} pb={2}>
            <StyledButton
              className="btn-block text-white w-100"
              variant="raised"
              size="large"
              fullWidth
              type="submit"
              disabled={invalid || pristine || submitting}
            >
              <FormattedMessage id="signup.btn_title" defaultMessage="" />
            </StyledButton>
          </Box>
        </form>
      </Fragment>
    );
  }
}

RegistrationForm.propTypes = {
  invalid: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'registration',
})(RegistrationForm);
