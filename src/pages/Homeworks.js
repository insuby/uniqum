import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';

// Styles
import colors from 'UIKit/Colors';
import styled from 'styled-components';

// Components
import Panel from 'components/Panel';
import Typography from '@material-ui/core/Typography';

import Loading from 'components/Form/Loading';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Tooltip from '@material-ui/core/Tooltip';
import { FormError } from 'UIKit/Fonts';
import { Flex, Box } from 'UIKit/Grid';
import { StyledButtonIcon } from 'UIKit/Form';
import { types, actions } from 'modules/Homework';
import { ROLE } from 'modules/User';
import { combineRequests } from 'modules/Requests';
import LinkIconStyled from 'components/Layout/LinkIconStyled';
import PlayCircleFilled from '@material-ui/icons/PlayCircleFilled';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import {
  SIGN_ADD,
  SIGN_SUB,
  SIGN_MIX,
  NO_MODE,
  RULE_SIMPLE_SUM,
  RULE_FIVE_SUM,
  RULE_TEN_SUM,
  RULE_FIVE_AND_TEN_SUM,
} from 'modules/Simulators';
// import { Line } from 'rc-progress';
import Delete from '@material-ui/icons/Delete';
import MUIDataTable from 'mui-datatables';

const ButtonStyled = styled(Button)`
  width: 72px;
  height: 72px;
  border-radius: 100% !important;
  background-color: ${colors.brand['500']} !important;
  color: ${colors.white['0']} !important;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.16);
  font-size: 28px !important;
  text-decoration: none !important;
`;

const DATE_FORMAT = 'DD.MM.YYYY';

/**
 * Homework page.
 * @param {object} values values.
 */
class Homeworks extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);

    props.loadData(props.page, props.rowsPerPage, {});
  }

  getFilterData = filterList => {
    const columnsList = [
      'params',
      'count',
      'simulator',
      'group',
      'student',
      'date',
      'comment',
      'progress',
      'operations',
    ];
    const filterData = {};

    for (let i = 0; i < columnsList.length; i += 1) {
      if (filterList && filterList[i] && filterList[i].length > 0) {
        filterData[columnsList[i]] = filterList[i];
      }
    }
    return filterData;
  };

  /**
   * @param {number} page page.
   * @param {number} rowsPerPage rowsPerPage.
   */
  changePage = (page, rowsPerPage) => {
    const { loadData, filterList } = this.props;
    const filterData = this.getFilterData(filterList);
    loadData(page, rowsPerPage, filterData);
  };

  /**
   * @param {string} id studentId.
   */
  handleDelete = id => {
    const { onDelete } = this.props;
    onDelete([id]);
  };

  /**
   * @param {string} id room id.
   * @param {string} homeworkId homework id.
   * @param {object} params params to homework.
   * @param {number} count count.
   * @param {string} comment comment.
   * @return {object}.
   */
  handleStart = (id, homeworkId, params, count, comment) => {
    const { user, history } = this.props;
    if (id) {
      if (Object.keys(user.user).length === 0) {
        return history.push('/simulators');
      }
      return history.push(
        `/cabinet/${id}?homework=${homeworkId}&params=${JSON.stringify(
          params
        )}&count=${count}&comment=${comment || ''}`
      );
    }
    return null;
  };

  onChangeRowsPerPage = count => {
    const { setRowsPerPage } = this.props;
    setRowsPerPage(count);
  };

  onFilterDialogClose = () => {};

  onFilterChange = (changedColumn, filterList) => {
    const { setFilterList } = this.props;
    setFilterList(filterList);
  };

  handleRowsDelete = rows => {
    const { data } = rows;
    const { homeworks, onDelete } = this.props;

    const ids = data.map(item => {
      const { dataIndex } = item;
      if (homeworks[dataIndex] && homeworks[dataIndex]._id) {
        return homeworks[dataIndex]._id;
      }
      return null;
    });

    // const { onDelete } = this.props;
    onDelete(ids);
  };

  handleFilterSubmit = filterList => {
    const { loadData, rowsPerPage } = this.props;
    const filterData = this.getFilterData(filterList);
    loadData(0, rowsPerPage, filterData);
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTable: {
          paper: {
            height: 'inherit',
          },
          responsiveScroll: {
            maxHeight: 'none',
            height: 'calc(100% - 128px)',
          },
        },
      },
    });

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const {
      requests,
      homeworks,
      count,
      page,
      filterList,
      filterData,
      rowsPerPage,
      user,
      intl,
    } = this.props;
    const request = combineRequests([
      requests[types.REQUEST_HOMEWORKS],
      requests[types.REQUEST_HOMEWORK_DELETE],
    ]);

    const signList = [
      {
        value: SIGN_ADD,
        label: '+',
      },
      {
        value: SIGN_SUB,
        label: '-',
      },
      {
        value: SIGN_MIX,
        label: '-/+',
      },
    ];
    const rules = [
      {
        value: RULE_SIMPLE_SUM,
        label: intl.formatMessage({ id: 'RULE_SIMPLE_SUM' }),
      },
      {
        value: RULE_FIVE_SUM,
        label: intl.formatMessage({ id: 'RULE_FIVE_SUM' }),
      },
      {
        value: RULE_TEN_SUM,
        label: intl.formatMessage({ id: 'RULE_TEN_SUM' }),
      },
      {
        value: RULE_FIVE_AND_TEN_SUM,
        label: intl.formatMessage({ id: 'RULE_FIVE_AND_TEN_SUM' }),
      },
      {
        value: NO_MODE,
        label: intl.formatMessage({ id: 'NO_MODE' }),
      },
    ];

    const paramsToString = params => (
      <Fragment>
        <p>
          <span>П:</span>
          <span>
            {params.mode && rules.filter(i => i.value === params.mode)[0].label}
            ;{' '}
          </span>
          {(params.sign || params.sign === 0) && (
            <Fragment>
              <span>З:</span>
              <span>
                {signList.filter(i => i.value === params.sign)[0].label};{' '}
              </span>
            </Fragment>
          )}
          <span>Ф:</span>
          <span>{params.numbers.join(', ')}; </span>
          <span>ВИ:</span>
          <span>{params.interval} сек.; </span>
          <span>КС:</span>
          <span>{params.steps}; </span>
          <span>ПЧ:</span>
          <span>{params.range}; </span>
        </p>
      </Fragment>
    );

    const returnAnswer = (resolved, success) => (
      <Fragment>
        <span style={{ color: 'green' }}>{success}</span>/
        <span>{resolved}</span>
      </Fragment>
    );

    const columns = [
      {
        name: 'Настройки',
        options: {
          filter: false,
          sort: false,
          empty: false,
        },
      },
      {
        name: 'Всего заданий',
        options: {
          filter: false,
          sort: false,
          empty: false,
        },
      },
      {
        name: 'Тренажер',
        options: {
          filter: true,
          filterList: filterList[2],
          filterOptions: filterData[2],
        },
      },
      {
        name: 'Группа',
        options: {
          filter: true,
          filterList: filterList[3],
          filterOptions: filterData[3],
        },
      },
      {
        name: 'ФИО ученика',
        options: {
          filter: true,
          filterList: filterList[4],
          filterOptions: filterData[4],
        },
      },
      {
        name: 'Дата',
        options: {
          filter: false,
        },
      },
      {
        name: 'Комментарий',
        options: {
          filter: false,
          sort: false,
        },
      },
      {
        name: 'Верно/Всего',
        options: {
          filter: false,
          sort: false,
          empty: false,
        },
      },
      {
        name: 'Операции',
        options: {
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta) => {
            const { rowIndex } = tableMeta;
            const { _id } = homeworks[rowIndex];

            return (
              <table>
                <tbody>
                  <tr>
                    <td>
                      {user.role === ROLE.MANAGER && (
                        <Tooltip
                          title="Удалить домашнее задание"
                          placement="right"
                        >
                          <StyledButtonIcon
                            type="button"
                            onClick={() => this.handleDelete(`${value}`)}
                            del="true"
                          >
                            <Delete
                              style={{
                                color: 'rgba(0, 0, 0, 0.54)',
                              }}
                            />
                          </StyledButtonIcon>
                        </Tooltip>
                      )}
                    </td>
                    <td>
                      {[ROLE.MANAGER, ROLE.AUTH].indexOf(user.role) > -1 && (
                        <Tooltip title="Перейти к заданию" placement="right">
                          <LinkIconStyled to={`/homeworks/run/${_id}`}>
                            <PlayCircleFilled />
                          </LinkIconStyled>
                        </Tooltip>
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
            );
          },
        },
      },
    ];

    let filter = false;
    if (user.role === 2) {
      filter = true;
    }
    const options = {
      search: false,
      responsive: 'scrollFullHeight',
      download: false,
      print: false,
      filter,
      filterType: 'multiselect',
      selectableRows: true,
      serverSide: true,
      count,
      page,
      rowsPerPage,
      onFilterChange: this.onFilterChange,
      onFilterDialogClose: this.onFilterDialogClose,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
          case 'changeRowsPerPage':
            this.changePage(tableState.page, tableState.rowsPerPage);
            break;
          default:
            break;
        }
      },
      customFilterDialogFooter: curfilterList => (
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="contained"
            onClick={() => this.handleFilterSubmit(curfilterList)}
          >
            Отфильтровать
          </Button>
        </div>
      ),
      onChangeRowsPerPage: this.onChangeRowsPerPage,
      onRowsDelete: this.handleRowsDelete,
      textLabels: {
        filter: {
          all: 'Все записи',
          title: 'Фильтра',
          reset: 'Сбросить фильтр',
        },
        pagination: {
          next: 'Следующая',
          previous: 'Предыдущая',
          rowsPerPage: 'Записей на странице:',
          displayRows: 'из',
        },
        viewColumns: {
          title: 'Показать/скрыть столбцы',
          titleAria: 'Показать/скрыть столбцы',
        },
        body: {
          noMatch: 'Не найдено ни одной записи, попробуйте создать новое',
          toolTip: 'Сортировать',
          columnHeaderTooltip: column => `Сортировка для ${column.label}`,
        },
        selectedRows: {
          text: 'записей отмечено',
          delete: 'Удалить',
          deleteAria: 'Удалить выделенные записи',
        },
      },
    };
    const availableField = [
      'group',
      'student',
      'simulator',
      'settings',
      'date',
      'params',
      'count',
      'comment',
      'homeworkId',
      'resolved',
      'success',
    ];
    let data = [];
    if (homeworks.length > 0) {
      data = homeworks.reduce(
        (list, homework) => {
          const result = Object.keys(homework).reduce((acc, key) => {
            if (availableField.includes(key)) {
              if (key === 'group' && homework[key]) {
                acc[key] = homework[key].name;
              } else if (key === 'student' && homework[key]) {
                acc[key] = [homework[key].surname, homework[key].name].join(
                  ' '
                );
              } else if (key === 'date') {
                acc[key] = moment(homework[key]).format(DATE_FORMAT);
              } else if (key === 'simulator' && homework[key]) {
                acc[key] = homework[key].desc;
              } else if (key === 'params' && homework[key]) {
                acc[key] = paramsToString(homework[key]);
              } else {
                acc[key] = homework[key];
              }

              return acc;
            }
            return acc;
          }, {});

          const comment = result.comment ? result.comment : '';
          const answers = returnAnswer(result.resolved, result.success);

          list.push([
            result.params,
            result.count,
            result.simulator,
            result.group,
            result.student,
            result.date,
            comment,
            answers,
            result.homeworkId,
          ]);
          return list;
        },

        []
      );
    }

    return (
      <Panel style={{ overflow: 'auto' }}>
        {request && (
          <Box px={4} py={(15, 5)}>
            {request.loading && <Loading />}
            {request.error && <FormError>{request.errorMessage}</FormError>}
            {request.success && homeworks && (
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable
                  title={
                    <Typography variant="title">
                      <FormattedMessage
                        id="HOMEWORK_TITLE"
                        defaultMessage="HOMEWORK_TITLE"
                      />
                    </Typography>
                  }
                  data={data}
                  columns={columns}
                  options={options}
                />
              </MuiThemeProvider>
            )}
            {user.role === ROLE.MANAGER && (
              <Flex justifyContent="center" pt={5}>
                <Tooltip title="Добавить домашнее задание" placement="right">
                  <ButtonStyled
                    variant="raised"
                    className="text-white"
                    aria-label="Добавить домашнее задание"
                    component={Link}
                    onClick={() => console.log()}
                    style={{ textDecoration: 'underline' }}
                    to="/homeworks/create"
                  >
                    <FontAwesomeIcon icon="plus" />
                  </ButtonStyled>
                </Tooltip>
              </Flex>
            )}
          </Box>
        )}
      </Panel>
    );
  }
}

Homeworks.propTypes = {
  loadData: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  setFilterList: PropTypes.func.isRequired,
  setRowsPerPage: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  intl: PropTypes.object.isRequired,
  filterList: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  homeworks: PropTypes.array.isRequired,
  filterData: PropTypes.array.isRequired,
  requests: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
  page: state.homeworks.page,
  filterList: state.homeworks.filterList,
  filterData: state.homeworks.filterData,
  rowsPerPage: state.homeworks.rowsPerPage,
  count: state.homeworks.count,
  homeworks: state.homeworks.homeworks,
  requests: state.requests,
});

const mapDispatchToProps = dispatch => ({
  loadData(page, rowsPerPage, filterData) {
    dispatch(actions.fetchHomeworks(page, rowsPerPage, filterData));
  },
  setRowsPerPage(value) {
    dispatch(actions.setRowsPerPage(value));
  },
  setFilterList(arr) {
    dispatch(actions.setFilterList(arr));
  },
  onDelete(id) {
    dispatch(actions.fetchHomeworkDelete(id));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(Homeworks));
