import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { types, actions } from 'modules/Guest';

// Components
import { H1Panel, FormError } from 'UIKit/Fonts';
import { Box } from 'UIKit/Grid';
import Loading from 'components/Form/Loading';
import ResetPasswordForm from 'pages/resetpassword/ResetPasswordForm';
import SuccessMessage from 'components/SuccessMessage';

/**
 * ResetPassword page.
 * @param {object} values values.
 */
class ResetPassword extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * @param {object} formData formData.
   */
  handleSubmit(formData) {
    const {
      match: {
        params: { token },
      },
      resetPassword,
    } = this.props;
    resetPassword(token, formData);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const { requests } = this.props;
    const request = requests[types.REQUEST_RESET_PASSWORD];

    return (
      <Fragment>
        <Box mb={8}>
          <Box mb={1}>
            <H1Panel>
              <FormattedMessage id="resetPassword.title" defaultMessage="" />
            </H1Panel>
          </Box>
        </Box>
        {request && (
          <div>
            {request.loading && <Loading />}
            {request.error && (
              <FormError>
                <FormattedMessage id={request.errorMessage} defaultMessage="" />
              </FormError>
            )}
          </div>
        )}
        {request && request.success ? (
          <SuccessMessage
            message={
              <FormattedMessage // eslint-disable-line react/jsx-wrap-multilines
                id="resetPassword.success_message"
                defaultMessage=""
              />
            }
          />
        ) : (
          <ResetPasswordForm onSubmit={this.handleSubmit} />
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  requests: state.requests,
  resetPassword: PropTypes.func.isRequired,
});

const mapDispatchToProps = dispatch => ({
  resetPassword(token, formData) {
    dispatch(actions.fetchResetPassword(token, formData));
  },
});

ResetPassword.propTypes = {
  match: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired,
  resetPassword: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
