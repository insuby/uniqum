import '@babel/polyfill';
/**
 * Platform
 * Copyright 2018 All Rights Reserved
 * Created By Gorazd company
 */
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faUsers,
  faFolder,
  faDatabase,
  faPlus,
  faEdit,
  faQuestion,
  faCalendarAlt,
  faMinusCircle,
  faSave,
  faCopy,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';

// Styles
import theme from 'UIKit/Theme';
import { ThemeProvider } from 'styled-components';
import 'components/Layout';
import { SnackbarProvider } from 'notistack';
import store from './Store';
import Routes from './Routes';
import ConnectedIntlProvider from './ConnectedIntlProvider';

library.add(
  faUser,
  faUsers,
  faFolder,
  faDatabase,
  faPlus,
  faEdit,
  faQuestion,
  faCalendarAlt,
  faMinusCircle,
  faSave,
  faCopy,
  faExclamationTriangle
);

const App = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <SnackbarProvider
        maxSnack={10}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <ConnectedIntlProvider textComponent={Fragment}>
          <Routes />
        </ConnectedIntlProvider>
      </SnackbarProvider>
    </Provider>
  </ThemeProvider>
);

ReactDOM.render(<App />, document.getElementById('root'));
