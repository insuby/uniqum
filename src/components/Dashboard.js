import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { actions } from 'modules/Socket';
import { withRouter } from 'react-router';

import ErrorBoundary from '../ErrorBoundary';
import TeacherControlPanel from './Dashboard/teacher/TeacherControlPanel';
import UserTerminal from './Dashboard/UserTerminal';

const Dashboard = ({
  socket,
  updateMembersRoom,
  chatroom,
  user,
  joinRoom,
  match: {
    params: { id: roomId },
  },
  leaveRoom,
  simulators: { showTrainer: showTeacherTrainer },
}) => {
  const { event, membersRoom } = socket;

  useEffect(() => {
    let leave = () => ({});
    if (roomId && chatroom && chatroom._id) {
      joinRoom(roomId);
      leave = () => leaveRoom(roomId);
    }
    return () => leave();
  }, [roomId, chatroom]);

  useEffect(() => {
    if (/joined|left/.test(event.event)) {
      const nextMembers = [...membersRoom.filter(u => u.id !== event.user.id)];
      if (/joined/.test(event.event)) {
        nextMembers.push(event.user);
      }
      updateMembersRoom(nextMembers);
    }
  }, [event]);

  return (
    <>
      {chatroom && chatroom._id && (
        <ErrorBoundary>
          {!(Object.keys(user).length === 0 && user.constructor === Object) && (
            <Grid container justify="center">
              {user.type === 'teacher' ? (
                <>
                  <Grid xs={12} lg={12} sm={12} md={12} key={0} item>
                    <TeacherControlPanel chatroom={chatroom} user={user} />
                  </Grid>
                  <Grid xs={12} lg={6} sm={10} md={8} key={1} item>
                    {showTeacherTrainer && (
                      <UserTerminal chatroom={chatroom} user={user} />
                    )}
                  </Grid>
                  <Grid xs={12} lg={12} sm={12} md={12} key={2} item>
                    {user.type === 'teacher' &&
                      membersRoom.length > 0 &&
                      membersRoom.filter(l => l.type === 'learner').length ===
                        0 && (
                        <p style={{ textAlign: 'center' }}>
                          Нет учеников онлайн!
                        </p>
                      )}
                  </Grid>
                  <Grid xs={12} lg={12} sm={12} md={12} key={3} item>
                    <Grid spacing={10} container justify="center">
                      {user.type === 'teacher' &&
                        membersRoom.length > 0 &&
                        membersRoom.filter(l => l.type === 'learner').length >
                          0 &&
                        membersRoom
                          .filter(l => l.type === 'learner')
                          .map(learner => (
                            <Grid
                              xs={12}
                              sm={6}
                              md={6}
                              lg={5}
                              item
                              key={learner.id}
                            >
                              <UserTerminal
                                chatroom={chatroom}
                                user={learner}
                              />
                            </Grid>
                          ))}
                    </Grid>
                  </Grid>
                </>
              ) : (
                <Grid xs={12} lg={12} sm={12} md={12} key={4} item>
                  <UserTerminal chatroom={chatroom} user={user} />
                </Grid>
              )}
            </Grid>
          )}
        </ErrorBoundary>
      )}
    </>
  );
};

Dashboard.propTypes = {
  chatroom: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  simulators: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
  updateMembersRoom: PropTypes.func.isRequired,
  joinRoom: PropTypes.func.isRequired,
  leaveRoom: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = (state, props) => {
  const {
    match: {
      params: { id },
    },
  } = props;

  const chatroom = state.socket.rooms.filter(item => item._id === id)[0];
  return {
    socket: state.socket,
    user: state.user.user,
    simulators: state.simulators,
    chatroom: chatroom || {},
  };
};

const mapDispatchToProps = dispatch => ({
  updateMembersRoom(members) {
    dispatch(actions.setMembersRoom(members));
  },
  leaveRoom(roomId) {
    dispatch(actions.leaveRoom(roomId));
  },
  joinRoom(roomId) {
    dispatch(actions.joinRoom(roomId));
  },
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Dashboard)
);
