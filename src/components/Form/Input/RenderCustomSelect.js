import PropTypes from 'prop-types';
import React, { Fragment } from 'react';

import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import Select from 'react-select';

export const Error = styled.div`
  font-size: 0.875rem;
  color: ${({ warning }) => (warning ? 'orange' : '#f55145')};
  &:not(:first-child) {
    margin-top: 4px;
  }
`;

const RenderCustomSelect = props => {
  const {
    input,
    meta: { touched, error, warning },
    options,
    isMulti,
    handleChange,
    isSearchable,
    closeMenuOnSelect,
  } = props;

  const { onChange, value } = input;

  const handleSelect = v => {
    onChange(v);
    handleChange(v);
  };

  return (
    <Fragment>
      <Select
        isMulti={isMulti}
        isSearchable={isSearchable}
        closeMenuOnSelect={closeMenuOnSelect}
        value={value}
        onChange={v => handleSelect(v)}
        options={options}
        placeholder="Выбор..."
        noOptionsMessage={() => 'Нет записей'}
      />
      {touched &&
        (((error || !value) && (
          <Error>
            <FormattedMessage id="REQUIRED" defaultMessage="" />
          </Error>
        )) ||
          (warning && <Error warning>{warning}</Error>))}
    </Fragment>
  );
};

RenderCustomSelect.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired,
  handleChange: PropTypes.func,
  isMulti: PropTypes.bool,
  isSearchable: PropTypes.bool,
  closeMenuOnSelect: PropTypes.bool,
};

RenderCustomSelect.defaultProps = {
  handleChange: () => ({}),
  isMulti: false,
  isSearchable: false,
  closeMenuOnSelect: true,
};

export default RenderCustomSelect;
