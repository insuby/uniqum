import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';
import colors from 'UIKit/Colors';
import { FormattedMessage } from 'react-intl';

export const InputStyled = styled.input`
  position: relative;
  width: 100%;
  height: 36px;
  padding: 0.625rem;
  border: 1px solid #ddd;
  border-radius: 2px;
  color: #464d69;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  &[type='checkbox'] {
    position: absolute;
    left: -999px;
    opacity: 0;
    box-shadow: none;
  }
  &:focus {
    border-color: #d4e2fc;
    outline: 0;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 0 0.2rem rgba(93, 146, 244, 0.25);
  }
  &:checked {
    ~ .label {
      &:before {
        background-color: ${colors.brand['500']};
      }
      &:after {
        opacity: 1;
      }
    }
  }
  ~ .label {
    position: relative;
    padding-left: 28px;
    cursor: pointer;
    font-size: 14px;
    &:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      width: 18px;
      height: 18px;
      border-radius: 2px;
      border: 1px solid ${colors.brand['500']};;
      //background-color: ${colors.brand['500']};
    }
    &:after {
      content: '';
      opacity: 0;
      position: absolute;
      top: 3px;
      left: 4px;
      display: block;
      width: 10px;
      height: 6px;
      border-left: 2px solid ${colors.white['0']};
      border-bottom: 2px solid ${colors.white['0']};
      transform: rotate(-45deg);
      transition: opacity 0.15s ease-in-out;
    }
  }
`;

export const StyledLabel = styled.label``;
export const Label = styled.div`
  font-size: 13px;
  color: #a3a3a3;
  &:not(:last-child) {
    margin-bottom: 4px;
  }
`;

export const Error = styled.div`
  font-size: 0.875rem;
  color: ${({ warning }) => (warning ? 'orange' : '#f55145')};
  &:not(:first-child) {
    margin-top: 4px;
  }
`;

const RenderCustomField = ({
  input,
  label,
  type,
  meta: { touched, error, warning },
  placeholder,
  disabled,
  autocomplete,
}) => (
  <StyledLabel>
    {type !== 'checkbox' && label && <Label>{label}</Label>}
    <InputStyled
      {...input}
      autoComplete={autocomplete}
      placeholder={placeholder}
      type={type}
      disabled={disabled}
    />
    {type === 'checkbox' && <Label className="label">{label}</Label>}
    {touched &&
      ((error && (
        <Error>
          <FormattedMessage id={error} defaultMessage="" />
        </Error>
      )) ||
        (warning && <Error warning>{warning}</Error>))}
  </StyledLabel>
);

RenderCustomField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  autocomplete: PropTypes.string,
};

RenderCustomField.defaultProps = {
  disabled: false,
  placeholder: '',
  label: '',
  type: 'text',
  autocomplete: null,
};

export default RenderCustomField;
