import { Link } from 'react-router-dom';
import styled from 'styled-components';

const LinkIconStyled = styled(Link)`
  display: inline-flex;
  vertical-align: middle;
  justify-content: center;
  color: rgba(0, 0, 0, 0.54);
`;

export default LinkIconStyled;
