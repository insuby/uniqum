import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

import Rod from './Abacus/Rod';

/**
 * Отобраежение отображения абака
 * @param {object} props props.
 */
class Abacus extends React.Component {
  /**
   * Class constructor.
   * @param {object} props props.
   */
  constructor(props) {
    super(props);

    this.handleChangeRod = this.handleChangeRod.bind(this);
  }

  /**
   * Обрабатывает изменение состояния стержня
   * @param {array} rodData props.
   * @param {string} rodLable props.
   */
  handleChangeRod(rodData, rodLable) {
    const { handleChange } = this.props;
    handleChange(rodData, rodLable);
  }

  /**
   * Render
   * @returns {object}.
   */
  render() {
    const columns = [];
    const { abacus } = this.props;

    Object.keys(abacus).forEach(index => {
      const data = abacus[index];
      columns.push(
        <Grid item xs={2} lg={2} sm={2} md={2} key={index}>
          <Rod
            data={data}
            onChangeRod={rodData => this.handleChangeRod(rodData, index)}
          />
        </Grid>
      );
    });

    return (
      <div>
        <Grid container justify="center" spacing={24}>
          {columns}
        </Grid>
      </div>
    );
  }
}

Abacus.propTypes = {
  abacus: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  handleChange: PropTypes.func.isRequired,
};

Abacus.defaultProps = {
  abacus: {},
};

export default Abacus;
