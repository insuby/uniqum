import React, { Component } from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';
import { NavLink } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withStyles } from '@material-ui/core/styles';

// Styles
import styled, { css } from 'styled-components';
import colors from 'UIKit/Colors';
import { connect } from 'react-redux';
import mq from 'UIKit/Mq';

const styles = {
  root: {
    padding: '12px 24px',
  },
};

// StyledComponents
const NavMenuItemStyled = styled.div`
  position: relative;
`;

const Icon = styled(FontAwesomeIcon)`
  display: inline-block;
  width: 22px !important;
  opacity: 0.5;
  color: ${colors.brand['700']} !important;
  text-align: center;
  font-size: 1.1em;
`;

const CollapseStyled = styled(Collapse)`
  &._nav-collapsed {
    overflow: hidden;
    position: absolute;
    top: 0;
    left: 100%;
    height: 0;
    min-width: 175px;
    box-shadow: 10px 0 20px rgba(0, 0, 0, 0.06);
    ${mq.tabletHorizontal`
      position: relative;
      top: auto;
      left: auto;
      min-width: 0;
      box-shadow: none;
    `};
  }
`;

const ListStyled = styled(List)`
  padding: 0 !important;
  background-color: ${colors.brand['50']};
  &._nav-collapsed {
    background-color: ${colors.white['0']};
    ${mq.tabletHorizontal`
      background-color: ${colors.brand['50']};
    `};
    .submenu__item {
      padding-left: 24px;
    }
  }
`;

const ListItemStyled = styled(ListItem)`
  padding: 0 !important;
  ${({ inner }) =>
    inner &&
    css`
      ${mq.tabletHorizontal`display:none`};
    `};
`;

const NavLinkStyled = styled(NavLink)`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 12px 24px;
  padding-left: ${({ inner }) => (inner ? '80px' : '24px')};
  line-height: 1.1;
  &:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    width: 4px;
  }
  &._active {
    background-color: ${colors.brand['0']};
    &:after {
      background-image: linear-gradient(
        225deg,
        ${colors.brand['700']} 0%,
        ${colors.brand['700']} 100%
      );
    }
  }
  &._nav-collapsed {
    padding-left: 24px;
    i ~ div,
    .menu {
      display: none;
      ${mq.tablet`display: block;`};
    }
  }
`;
const NavInnerTitleStyled = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 12px 24px;
  padding-left: ${({ inner }) => (inner ? '80px' : '24px')};
  line-height: 1.1;
  &:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    width: 4px;
  }
  &._active {
    background-color: ${colors.brand['0']};
    &:after {
      background-image: linear-gradient(225deg, #1de9b6 0%, #1dc4e9 100%);
    }
  }
  &._nav-collapsed {
    padding-left: 24px;
    i ~ div,
    .menu {
      display: none;
    }
  }
`;

const ListItemWithArrow = styled(ListItem)`
  position: relative;
  &:after {
    content: '';
    position: absolute;
    top: 50%;
    right: 20px;
    width: 5px;
    height: 5px;
    margin-top: -4px;
    border-bottom: 2px solid ${colors.gray['500']};
    border-left: 2px solid ${colors.gray['500']};
    transform: rotate(-45deg);
  }
  &._nav-collapsed {
    &:after {
      display: none;
      ${mq.tablet`display: block;`};
    }
  }
  ${({ open }) =>
    open &&
    css`
      &._nav-collapsed {
        background-color: ${colors.brand['0']};
        &:before {
          content: '';
          position: absolute;
          height: 0;
          width: 0;
          top: 50%;
          right: 0;
          border-top: 7px solid transparent;
          border-right: 8px solid #fff;
          border-bottom: 7px solid transparent;
          transform: translateY(-50%);
          ${mq.tablet`display: none;`};
        }
      }
      &:after {
        margin-top: 1px;
        transform: rotate(135deg);
      }
    `};
`;

const renderCollapsedTitle = menu => {
  const itemTitle = (
    <div>
      <FormattedMessage id={menu.menu_title} defaultMessage="" />
    </div>
  );
  return (
    <ListItemStyled inner="true" button component="li">
      <NavInnerTitleStyled className="submenu__item" inner="true">
        <span className="menu">{itemTitle}</span>
      </NavInnerTitleStyled>
    </ListItemStyled>
  );
};

/**
 * Nav Menu Item
 */
class NavMenuItem extends Component {
  state = {
    subMenuOpen: false,
  };

  /**
   * On Toggle Collapse Menu
   */
  onToggleCollapseMenu() {
    const { subMenuOpen } = this.state;
    this.setState({ subMenuOpen: !subMenuOpen });
  }

  /**
   * Render opener
   * @returns {*}
   * @param {Object} menu The list.
   */
  renderOpener(menu) {
    const { navCollapsed, classes } = this.props;
    const { subMenuOpen } = this.state;

    const itemTitle = (
      <div>
        <FormattedMessage id={menu.menu_title} defaultMessage="" />
      </div>
    );
    return (
      <ListItemWithArrow
        button
        component="li"
        onClick={() => this.onToggleCollapseMenu()}
        open={subMenuOpen}
        disableGutters
        classes={{ root: classes.root }}
        className={navCollapsed && '_nav-collapsed'}
      >
        <ListItemIcon>
          <Icon icon={menu.menu_icon} />
        </ListItemIcon>
        {!navCollapsed && (
          <>
            {itemTitle}
            <i className="ti-angle-right side-arrow" />
          </>
        )}
      </ListItemWithArrow>
    );
  }

  /**
   * @returns {*}
   */
  render() {
    const { menu, user, navCollapsed } = this.props;
    const { subMenuOpen } = this.state;

    if (menu.child_routes != null) {
      return (
        <NavMenuItemStyled>
          {this.renderOpener(menu)}
          <CollapseStyled
            in={subMenuOpen}
            timeout="auto"
            className={navCollapsed && '_nav-collapsed'}
          >
            <ListStyled className={navCollapsed && '_nav-collapsed'}>
              {navCollapsed && renderCollapsedTitle(menu)}
              {menu.child_routes
                .filter(item => item.role.indexOf(user.role) > -1)
                .map(subMenu => (
                  <ListItemStyled button component="li" key={subMenu.id}>
                    <NavLinkStyled
                      exact
                      activeClassName="_active"
                      className="submenu__item"
                      to={subMenu.path}
                      inner="true"
                    >
                      <span className="menu">
                        <FormattedMessage
                          id={subMenu.menu_title}
                          defaultMessage=""
                        />
                      </span>
                    </NavLinkStyled>
                  </ListItemStyled>
                ))}
            </ListStyled>
          </CollapseStyled>
        </NavMenuItemStyled>
      );
    }
    return (
      <ListItemStyled button component="li">
        <NavLinkStyled
          exact
          activeClassName={
            menu.path === window.location.pathname ? '_active' : ' '
          }
          className={navCollapsed && '_nav-collapsed'}
          to={menu.path}
        >
          <ListItemIcon>
            <Icon icon={menu.menu_icon} />
          </ListItemIcon>
          <span className="menu">
            <FormattedMessage id={menu.menu_title} defaultMessage="" />
          </span>
        </NavLinkStyled>
      </ListItemStyled>
    );
  }
}

NavMenuItem.propTypes = {
  user: PropTypes.object.isRequired,
  menu: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  navCollapsed: PropTypes.any.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
});

export default withStyles(styles)(
  withRouter(connect(mapStateToProps)(NavMenuItem))
);
