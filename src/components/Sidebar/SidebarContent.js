import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import { withRouter } from 'react-router';

import styled, { css } from 'styled-components';
import mq from 'UIKit/Mq';
// import { ROLE } from 'modules/User';
import navLinks from './NavLinks';
import NavMenuItem from './NavMenuItem';

// const { AUTH, MANAGER } = ROLE;

// Styles

// StyledComponents
const NavStyled = styled.nav`
  overflow: auto;
  height: 100%;
  padding-top: 20px;
  ${mq.tabletHorizontal`
    position: fixed;
    z-index: 2;
    top: 0;
    bottom: 0;
    min-width: 270px;
    max-width: 300px;
    margin-top: 35px;
    transform:translateX(0)
    `};
  ${mq.tablet`
    width: 100%;
    margin-top: 35px;
    `};
  ${({ navCollapsed }) =>
    navCollapsed &&
    css`
      overflow: initial;
    `};
`;

/**
 * Sidebar Content
 */
class SidebarContent extends Component {
  /**
   * @returns {*}
   */
  render() {
    const { navCollapsed, user } = this.props;

    return (
      <NavStyled navCollapsed={navCollapsed}>
        <List>
          {navLinks.menu
            .filter(item => item.role.indexOf(user.role) > -1)
            .map(menu => (
              <NavMenuItem
                menu={menu}
                type="menu"
                key={menu.id}
                onToggleMenu={() => this.toggleMenu(menu)}
                navCollapsed={navCollapsed}
              />
            ))}
        </List>
        <List>
          {navLinks.user.map(menu => (
            <NavMenuItem
              menu={menu}
              type="user"
              key={menu.id}
              onToggleMenu={() => this.toggleMenu(menu)}
              navCollapsed={navCollapsed}
            />
          ))}
        </List>
      </NavStyled>
    );
  }
}

SidebarContent.propTypes = {
  navCollapsed: PropTypes.any.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.user,
});

export default withRouter(connect(mapStateToProps)(SidebarContent));
