// sidebar nav links
import { ROLE } from 'modules/User';

const { GUEST, AUTH, MANAGER, ADMIN, SYSTEM } = ROLE;

export default {
  user: [
    {
      id: 1,
      menu_title: 'sidebar.settings',
      menu_icon: 'user',
      role: [GUEST, AUTH, MANAGER, ADMIN, SYSTEM],
      child_routes: [
        {
          id: 11,
          menu_title: 'sidebar.change_password',
          role: [AUTH, MANAGER, ADMIN, SYSTEM],
          path: '/change-password',
        },
        {
          id: 14,
          path: '/logout',
          role: [GUEST, AUTH, MANAGER, ADMIN, SYSTEM],
          menu_title: 'sidebar.logout',
        },
      ],
    },
  ],
  menu: [
    {
      id: 2,
      menu_title: 'simulators',
      menu_icon: 'users',
      role: [AUTH, MANAGER],
      path: '/simulators',
      child_routes: null,
    },
    {
      id: 4,
      menu_title: 'reports',
      role: [AUTH, MANAGER, ADMIN, SYSTEM],
      menu_icon: 'database',
      path: '/reports',
      child_routes: null,
    },
    {
      id: 5,
      menu_title: 'HOMEWORK_TITLE',
      menu_icon: 'calendar-alt',
      role: [AUTH, MANAGER, ADMIN, SYSTEM],
      path: '/homeworks',
      child_routes: null,
    },
    {
      id: 6,
      menu_title: 'BOOK_TITLE',
      menu_icon: 'database',
      role: [MANAGER, ADMIN, SYSTEM],
      path: '/books',
      child_routes: null,
    },
    {
      id: 3,
      menu_title: 'DIRECTORY',
      menu_icon: 'folder',
      role: [MANAGER, ADMIN, SYSTEM],
      child_routes: [
        {
          id: 31,
          path: '/directory/schools',
          role: [SYSTEM],
          menu_title: 'SCHOOL_TITLE',
        },
        {
          id: 32,
          path: '/directory/teachers',
          role: [ADMIN, SYSTEM],
          menu_title: 'TEACHER_TITLE',
        },
        {
          id: 33,
          menu_title: 'GROUP_TITLE',
          role: [MANAGER, ADMIN, SYSTEM],
          path: '/directory/groups',
        },
        {
          id: 34,
          path: '/directory/students',
          role: [MANAGER, ADMIN, SYSTEM],
          menu_title: 'STUDENT_TITLE',
        },
      ],
    },
  ],
};
