import React from 'react';
import PropTypes from 'prop-types';
import {
  faCheck,
  faSlidersH,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import { Box } from 'UIKit/Grid';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';

import FormControlLabel from '@material-ui/core/FormControlLabel';

import { labels } from 'modules/Simulators';
import RenderPlayButton from './RenderPlayButton';

const ControlPanel = ({
  open,
  request,
  state,
  onStart,
  onStop,
  onCheck,
  onChange,
  onChecked,
  onShowSettings,
  onChangeNumber,
  onChangeRoom,
  onCheckedControls,
  visible,
  showTrainer,
  onShowTrainer,
}) => {
  const {
    controls: { showAnswers, start },
    dictationState: { dictation, amount },
    number,
    room,
  } = state;

  return (
    <Box my={[4, 4]}>
      <table>
        <tbody>
          <tr>
            <td>
              <>
                {request && request.loading ? (
                  <Button
                    color="primary"
                    variant="contained"
                    disabled
                    endIcon={<FontAwesomeIcon spin icon={faSpinner} />}
                  >
                    Загрузка
                  </Button>
                ) : (
                  <RenderPlayButton
                    onStart={onStart}
                    onStop={() => onStop()}
                    play={start}
                  />
                )}
              </>
            </td>
            <td>
              <Button
                variant="contained"
                onClick={onCheck}
                startIcon={<FontAwesomeIcon icon={faCheck} />}
              >
                Проверить
              </Button>
            </td>
            {visible.number && (
              <td>
                <FormControl>
                  <InputLabel>Кол-во экраном</InputLabel>
                  <Select value={number} onChange={onChangeNumber}>
                    <MenuItem value="1">1</MenuItem>
                    <MenuItem value="2">2</MenuItem>
                    <MenuItem value="3">3</MenuItem>
                    <MenuItem value="4">4</MenuItem>
                    <MenuItem value="5">5</MenuItem>
                    <MenuItem value="6">6</MenuItem>
                    <MenuItem value="7">7</MenuItem>
                    <MenuItem value="8">8</MenuItem>
                    <MenuItem value="9">9</MenuItem>
                    <MenuItem value="10">10</MenuItem>
                    <MenuItem value="11">11</MenuItem>
                    <MenuItem value="12">12</MenuItem>
                  </Select>
                </FormControl>
              </td>
            )}
            {visible.room && (
              <td>
                <FormControl>
                  <InputLabel>Тренажер</InputLabel>
                  <Select value={room} onChange={onChangeRoom}>
                    <MenuItem value="roomA">{labels.roomA}</MenuItem>
                    <MenuItem value="roomB">{labels.roomB}</MenuItem>
                    <MenuItem value="roomC">{labels.roomC}</MenuItem>
                  </Select>
                </FormControl>
              </td>
            )}
            <td>
              <Button
                variant="contained"
                onClick={onShowSettings}
                startIcon={<FontAwesomeIcon icon={faSlidersH} />}
              >
                {open ? 'Скрыть' : 'Показать'} параметры
              </Button>
            </td>
            {visible.trainer && (
              <td>
                <Button
                  variant="contained"
                  onClick={() => onShowTrainer()}
                  startIcon={<FontAwesomeIcon icon={faSlidersH} />}
                >
                  {showTrainer ? 'Скрыть' : 'Показать'} тренажер педагога
                </Button>
              </td>
            )}
            <td>
              <FormControl>
                <FormControlLabel
                  value="top"
                  control={
                    <Switch
                      checked={dictation}
                      onChange={event => onChecked(event, 'dictation')}
                      color="primary"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  }
                  label="Режим диктанта"
                />
              </FormControl>
              {dictation && (
                <>
                  <FormGroup aria-label="position" row>
                    <TextField
                      id="outlined-basic"
                      value={amount}
                      type="number"
                      onChange={event => onChange(event, 'amount')}
                      autoComplete="off"
                      label="Кол-во примеров диктанта"
                      variant="outlined"
                    />
                  </FormGroup>
                  <FormControl>
                    <FormControlLabel
                      value="top"
                      control={
                        <Switch
                          checked={showAnswers}
                          onChange={event =>
                            onCheckedControls(event, 'showAnswers')
                          }
                          color="primary"
                          inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                      }
                      label="Показать ответы по окончанию диктанта"
                    />
                  </FormControl>
                </>
              )}
            </td>
          </tr>
        </tbody>
      </table>
    </Box>
  );
};

ControlPanel.defaultProps = {
  request: {},
  showTrainer: false,
  onShowTrainer: () => ({}),
};

ControlPanel.propTypes = {
  request: PropTypes.object,
  state: PropTypes.object.isRequired,
  visible: PropTypes.object.isRequired,
  onShowTrainer: PropTypes.func,
  showTrainer: PropTypes.bool,
  open: PropTypes.bool.isRequired,
  onChangeNumber: PropTypes.func.isRequired,
  onChangeRoom: PropTypes.func.isRequired,

  onStart: PropTypes.func.isRequired,
  onCheck: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onCheckedControls: PropTypes.func.isRequired,
  onChecked: PropTypes.func.isRequired,
  onShowSettings: PropTypes.func.isRequired,
  onStop: PropTypes.func.isRequired,
};

export default ControlPanel;
