import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ControlPanel from 'components/Dashboard/teacher/ControlPanel';
import { connect } from 'react-redux';
import { actions, CHANGE_CONTROL, CHANGE_DICTATION } from 'modules/Socket';
import { types, actions as simulatorsActions } from 'modules/Simulators';
import { actions as notifierActions } from 'modules/Notifier';

const PreTeacherControlPanel = props => {
  const {
    socket: { request },
    user,
    send,
    chatroom,
    open,
    simulators,
    handleShowParamsBlock,
    displayNotifier,
    setShowTrainer,
  } = props;
  const { controls, showTrainer, dictationState } = simulators;
  const [hasError, setHasError] = useState(null);

  const onShowTrainer = () => {
    setShowTrainer(!showTrainer);
  };

  const handleStart = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'START_ALL',
            data: {
              controls: { ...controls, start: true, check: false, stop: false },
              dictationState,
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['START_ALL', err].join(': '));
        }
      }
    );
  };

  const handleStop = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'STOP_ALL',
            data: {
              controls: { ...controls, start: false, check: false, stop: true },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['STOP_ALL', err].join(': '));
        }
      }
    );
  };

  const handleCheck = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHECK_ALL',
            data: {
              controls: { ...controls, check: true, start: false, stop: false },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHECK_ALL', err].join(': '));
        }
      }
    );
  };

  const handleChange = (event, name) => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_DICTATION,
          payload: {
            to: user,
            type: types.CHANGE_DICTATION_ALL,
            data: {
              dictationState: {
                ...dictationState,
                [name]: event.target.value,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE DICTATION STATE', err].join(': '));
        }
      }
    );
  };

  const handleCheckedControls = (event, name) => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: types.CHANGE_CONTROLS_ALL,
            data: { controls: { ...controls, [name]: event.target.checked } },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE_CONTROLS_ALL', err].join(': '));
        }
      }
    );
  };

  const handleChecked = (event, name) => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_DICTATION,
          payload: {
            to: user,
            type: types.CHANGE_DICTATION_ALL,
            data: {
              dictationState: {
                ...dictationState,
                [name]: event.target.checked,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE DICTATION STATE', err].join(': '));
        }
      }
    );
  };

  useEffect(() => {
    if (hasError) {
      console.log('hasError=', hasError);
      displayNotifier({
        message: 'Ваша онлайн сессия истекла, перезагрузите страницу',
        options: {
          variant: 'warning',
        },
      });
    }
  }, [hasError]);

  const handleChangeNumber = () => {
    console.log('handleChangeNumber');
  };
  const handleChangeRoom = () => {
    console.log('handleChangeRoom');
  };

  return (
    <ControlPanel
      open={open}
      request={request}
      state={{ ...simulators }}
      onStop={handleStop}
      onCheck={handleCheck}
      onStart={handleStart}
      onChange={handleChange}
      onChecked={handleChecked}
      onChangeNumber={handleChangeNumber}
      onChangeRoom={handleChangeRoom}
      onShowSettings={handleShowParamsBlock}
      showTrainer={showTrainer}
      onShowTrainer={onShowTrainer}
      onCheckedControls={handleCheckedControls}
      visible={{
        number: false,
        room: false,
        trainer: true,
      }}
    />
  );
};

PreTeacherControlPanel.propTypes = {
  chatroom: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  displayNotifier: PropTypes.func.isRequired,
  send: PropTypes.func.isRequired,
  handleShowParamsBlock: PropTypes.func.isRequired,
  setShowTrainer: PropTypes.func.isRequired,
  socket: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  simulators: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  socket: state.socket,
  simulators: state.simulators,
});

const mapDispatchToProps = dispatch => ({
  send(msg, cb) {
    dispatch(actions.send(msg, cb));
  },
  setShowTrainer(toggle) {
    dispatch(simulatorsActions.setShowTrainer(toggle));
  },
  displayNotifier(message) {
    dispatch(notifierActions.enqueueSnackbar(message));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreTeacherControlPanel);
