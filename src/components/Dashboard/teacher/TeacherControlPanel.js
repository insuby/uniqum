import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { actions as notifierActions } from 'modules/Notifier';

import { makeStyles } from '@material-ui/core/styles';

import PreTeacherControlPanel from 'components/Dashboard/teacher/PreTeacherControlPanel';
import Paper from '@material-ui/core/Paper';

import { actions, CHANGE_CONTROL } from 'modules/Socket';
import { types } from 'modules/Simulators';
import SimulatorParams from '../../../../vendors/simulators/src/SimulatorParams';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    margin: '0 0 24px 0',
    borderRadius: 0,
    boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
  },
}));

const TeacherControlPanel = ({
  simulators: { params },
  chatroom,
  user,
  send,
  displayNotifier,
}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [hasError, setHasError] = useState(null);

  const handleChangeParams = nextParams => {
    let type = 'CHANGE_FILTER';
    if (user.type === 'teacher') {
      type = types.CHANGE_PARAMS_ALL;
    }

    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type,
            data: { params: nextParams },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE_PARAMS_ALL', err].join(': '));
        }
      }
    );
  };

  useEffect(() => {
    if (hasError) {
      console.log('hasError=', hasError);
      displayNotifier({
        message: 'Ваша онлайн сессия истекла, перезагрузите страницу',
        options: {
          variant: 'warning',
        },
      });
    }
  }, [hasError]);

  const handleShowParamsBlock = () => {
    setOpen(!open);
  };

  return (
    <Paper className={classes.paper}>
      <SimulatorParams
        onShowParamsBlock={handleShowParamsBlock}
        onChangeParams={handleChangeParams}
        params={params}
        room={chatroom.name}
        open={open}
      />
      <PreTeacherControlPanel
        chatroom={chatroom}
        user={user}
        open={open}
        handleShowParamsBlock={handleShowParamsBlock}
      />
    </Paper>
  );
};

TeacherControlPanel.propTypes = {
  send: PropTypes.func.isRequired,
  displayNotifier: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  chatroom: PropTypes.object.isRequired,
  simulators: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  simulators: state.simulators,
});

const mapDispatchToProps = dispatch => ({
  send(msg, cb) {
    dispatch(actions.send(msg, cb));
  },
  displayNotifier(message) {
    dispatch(notifierActions.enqueueSnackbar(message));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TeacherControlPanel);
