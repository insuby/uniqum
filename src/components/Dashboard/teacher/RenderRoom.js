import React from 'react';
import Column from '../../../../vendors/simulators/src/Column';
import FlashCard from '../../../../vendors/simulators/src/FlashCard';
import Forsage from '../../../../vendors/simulators/src/Forsage';

const RenderRoom = ({ room }) => {
  let component = <Column />;
  switch (room) {
    case 'roomA':
      component = <Forsage />;
      break;
    case 'roomC':
      component = <FlashCard />;
      break;
    default:
      break;
  }

  return component;
};

export default RenderRoom;
