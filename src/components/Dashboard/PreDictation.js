import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  actions,
  CHANGE_CONTROL,
  CHANGE_POSITION,
  CHANGE_DICTATION,
} from 'modules/Socket';
import Dictation from 'components/Dashboard/Dictation';
import { types, sumAbacus } from 'modules/Simulators';
import { actions as notifierActions } from 'modules/Notifier';
// import SimulatorParams from '../../../vendors/simulators/src/SimulatorParams';
import RenderSimulatorParams from 'styledComponents/RenderSimulatorParams';
import RenderDictation from 'styledComponents/RenderDictation';

const PreDictation = ({
  state,
  request,
  send,
  user,
  parentArgs,
  chatroom,
  displayNotifier,
}) => {
  const { controls, params: parentParams, dictationState } = state;

  const { open } = controls;

  const [hasError, setHasError] = useState(null);

  /*
   * Ref
   */
  const controlsRef = useRef(controls);
  const dictationStateRef = useRef(dictationState);
  const argsRef = useRef([]);
  /*
   * End Ref
   */

  const [params, setParams] = useState(parentParams);
  useEffect(() => {
    setParams(parentParams);
  }, [parentParams]);

  const [args, setArgs] = useState([]);
  useEffect(() => {
    setArgs(parentArgs);
  }, [parentArgs]);

  useEffect(() => {
    if (request && request.success) {
      argsRef.current = request.data;
      setArgs(request.data[0]);
    }
  }, [request]);

  useEffect(() => {
    controlsRef.current = controls;
  }, [controls]);

  useEffect(() => {
    dictationStateRef.current = dictationState;
  }, [dictationState]);

  const handleGetArgs = nextParams => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'START',
            data: {
              params: nextParams,
              dictationState: dictationStateRef.current,
              controls: {
                ...controlsRef.current,
                start: true,
                stop: false,
                check: false,
                value: '',
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['START', err].join(': '));
        }
      }
    );
  };

  const handleStop = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'STOP',
            data: {
              controls: {
                ...controlsRef.current,
                start: false,
                stop: true,
                check: false,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['STOP', err].join(': '));
        }
      }
    );
  };

  const handleCheck = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHECK',
            data: {
              controls: {
                ...controlsRef.current,
                start: false,
                stop: false,
                check: true,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHECK', err].join(': '));
        }
      }
    );
  };

  const handleIncrement = values => {
    console.log('increment', values);
  };

  const handleGetNextArgs = index => {
    setArgs(argsRef.current[index]);
  };

  const handleChangeParams = nextParams => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: types.CHANGE_PARAMS,
            data: { params: nextParams },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE_PARAMS', err].join(': '));
        }
      }
    );
  };

  const handleChangeInputValue = value => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHANGE',
            data: {
              controls: {
                ...controlsRef.current,
                value,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE VALUE', err].join(': '));
        }
      }
    );
  };

  const handleShowParamsBlock = () => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHANGE',
            data: {
              controls: {
                ...controlsRef.current,
                open: !open,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['TOGGLE OPEN PARAMS BLOCK', err].join(': '));
        }
      }
    );
  };

  const handleAbacusChange = (rodData, rodLable, nextAbacusState) => {
    // this.setState({ [rodLable]: rodData }, () => {
    //   const { user, send, chatroom } = this.props;
    //   const { state } = this;
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_POSITION,
          payload: {
            to: user,
            type: 'CHANGE',
            data: { abacusState: nextAbacusState },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE POSITION ABACUS', err].join(': '));
        }
      }
    );

    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHANGE',
            data: {
              controls: {
                ...controlsRef.current,
                value: sumAbacus(nextAbacusState),
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE VALUE', err].join(': '));
        }
      }
    );
  };

  const handleToggleSetting = (event, name) => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_CONTROL,
          payload: {
            to: user,
            type: 'CHANGE',
            data: {
              controls: {
                ...controlsRef.current,
                [name]: event.target.checked,
              },
            },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE TOGGLE SETTINGS', err].join(': '));
        }
      }
    );
  };

  const handleChangeDictationSetting = time => {
    send(
      {
        chatroomName: chatroom._id,
        message: {
          type: CHANGE_DICTATION,
          payload: {
            to: user,
            type: 'CHANGE',
            data: { dictationState: { ...dictationState, period: time } },
          },
        },
      },
      err => {
        if (err) {
          setHasError(['CHANGE DICTATION STATE', err].join(': '));
        }
      }
    );
  };

  useEffect(() => {
    if (hasError) {
      console.log('hasError=', hasError);
      displayNotifier({
        message: 'Ваша онлайн сессия истекла, перезагрузите страницу',
        options: {
          variant: 'warning',
        },
      });
    }
  }, [hasError]);

  return (
    <>
      <RenderSimulatorParams
        onChangeParams={handleChangeParams}
        params={params}
      />
      <Dictation
        state={state}
        args={args}
        room={chatroom.name}
        onCheck={handleCheck}
        onIncrement={handleIncrement}
        onStop={handleStop}
        onChangeInputValue={handleChangeInputValue}
        onAbacusChange={handleAbacusChange}
        onToggleSetting={handleToggleSetting}
        onChangeDictationSetting={handleChangeDictationSetting}
        request={request}
        fetchArgs={handleGetArgs}
        onShowParamsBlock={handleShowParamsBlock}
        getNextArgs={handleGetNextArgs}
        render={props => <RenderDictation {...props} />}
      />
    </>
  );
};

// <SimulatorParams
//   onChangeParams={handleChangeParams}
//   params={params}
//   room={chatroom.name}
//   fullSize
//   onShowParamsBlock={handleShowParamsBlock}
//   open={open}
// />

PreDictation.propTypes = {
  state: PropTypes.object.isRequired,
  request: PropTypes.object,
  send: PropTypes.func.isRequired,
  displayNotifier: PropTypes.func.isRequired,
  chatroom: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  parentArgs: PropTypes.array,
};

PreDictation.defaultProps = {
  request: {},
  parentArgs: [],
};

const mapStateToProps = (state, props) => {
  const { user } = props;
  const request = state.socket.requests[user._id];
  return {
    request,
  };
};

const mapDispatchToProps = dispatch => ({
  send(msg, cb) {
    dispatch(actions.send(msg, cb));
  },
  displayNotifier(message) {
    dispatch(notifierActions.enqueueSnackbar(message));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PreDictation);
