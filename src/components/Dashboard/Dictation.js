import React, { useState, useEffect, useRef, useReducer } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import Paper from '@material-ui/core/Paper';
import { useEventCallback } from 'rxjs-hooks';
import { takeUntil, tap, switchMap, delay, filter } from 'rxjs/operators';
// import { makeStyles } from '@material-ui/core/styles';

// import RenderRoom from 'components/Dashboard/teacher/RenderRoom';
// import SimulatorParams from '../../../vendors/simulators/src/SimulatorParams';
// import ControlPanel from '../../../vendors/simulators/src/ControlPanel';
import PersonalPanel from '../../../vendors/simulators/src/PersonalPanel';
// import DictationTime from '../../../vendors/simulators/src/DictationTime';
// import AnswerCounter from '../../../vendors/simulators/src/AnswerCounter';
// import LinearProgressCustom from '../../../vendors/simulators/src/LinearProgressCustom';
// import AbacusContainer from '../../../vendors/simulators/src/AbacusContainer';

// import Sample from '../../../vendors/simulators/src/Sample';

export const types = {
  ON_START: 'ON_START',
  ON_STOP: 'ON_STOP',
  SET_STATE: 'SET_STATE',
  SET_DICTATION: 'SET_DICTATION',
  ON_PROGRESS: 'ON_PROGRESS',
  SET_SAMPLES: 'SET_SAMPLES',
  SET_PERIOD: 'SET_PERIOD',
  SET_AMOUNT: 'SET_AMOUNT',
};

// const useStyles = makeStyles(theme => ({
//   paper: {
//     padding: theme.spacing(2),
//     textAlign: 'center',
//     color: theme.palette.text.secondary,
//     borderRadius: 0,
//     marginTop: '10px',
//     boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
//   },
// }));

function dictationEpic(events$) {
  return events$.pipe(
    filter(e => e.type === 'dictation_start'),
    switchMap(action =>
      events$.pipe(
        filter(e => e.type === 'sample_stop'),
        delay(1000 * Number(action.payload.interval)),
        tap(() => action.deps.clear()),
        tap(() => action.deps.run()),

        takeUntil(events$.pipe(filter(e => e.type === 'dictation_stop')))
      )
    )
  );
}

const initialState = {
  dictation: false,
  amount: 3,
  start: false,
  progress: false,
  stop: false,
  period: 5,
  samples: [],
};

function reducer(state, action) {
  switch (action.type) {
    case types.SET_AMOUNT:
    case types.SET_PERIOD:
    case types.SET_SAMPLES:
    case types.SET_DICTATION: {
      return {
        ...state,
        [action.meta]: action.payload,
      };
    }
    case types.ON_START: {
      return {
        ...state,
        start: true,
        progress: false,
        stop: false,
        samples: [],
      };
    }
    case types.ON_PROGRESS: {
      return {
        ...state,
        start: false,
        progress: true,
        stop: false,
      };
    }
    case types.ON_STOP: {
      return {
        ...state,
        start: false,
        progress: false,
        stop: true,
      };
    }
    case types.SET_STATE: {
      return action.payload;
    }
    default:
      return state;
  }
}

const Dictation = ({
  fetchArgs,
  args: parentArgs,
  request,
  getNextArgs,
  state,
  onStop,
  room,
  onCheck,
  onIncrement,
  onShowParamsBlock,
  onChangeInputValue,
  // onAbacusChange,
  onToggleSetting,
  onChangeDictationSetting,
  render,
}) => {
  // const classes = useStyles();

  const { controls, params, dictationState: nextDictationState } = state;

  const { start, stop } = controls;

  const [dictationState, dispatch] = useReducer(reducer, initialState);

  const { dictation } = dictationState;

  useEffect(() => {
    dispatch({
      type: types.SET_STATE,
      payload: nextDictationState,
    });
  }, [nextDictationState]);

  useEffect(() => {
    if (stop) {
      dispatch({
        type: types.ON_STOP,
      });
    }
  }, [stop]);

  useEffect(() => {
    if (start && dictation) {
      dispatch({
        type: types.ON_START,
      });
    }
  }, [start, dictation]);

  /*
   * Ref
   */
  const dictationStateRef = useRef(dictationState);
  useEffect(() => {
    dictationStateRef.current = dictationState;
  }, [dictationState]);

  const dictationIndexRef = useRef(0);
  useEffect(() => {
    dictationIndexRef.current = dictationState.samples.length;
  }, [dictationState.samples]);

  const [onEvent] = useEventCallback(dictationEpic);

  const [args, setArgs] = useState(parentArgs);

  useEffect(() => {
    setArgs(parentArgs);
  }, [parentArgs]);

  useEffect(() => {
    if (args.length > 0) {
      dispatch({
        type: types.SET_SAMPLES,
        meta: 'samples',
        payload: [...dictationState.samples, args],
      });
    }
  }, [args]);

  const handleBegin = () => console.log('');

  function handleEnd() {
    const { amount: nextAmount, period } = dictationStateRef.current;

    if (dictationIndexRef.current === +nextAmount) {
      // TODO сделать без timeout

      setTimeout(() => {
        onEvent({
          type: 'dictation_stop',
        });
        dispatch({
          type: types.ON_STOP,
        });
      }, 1000 * period);
    } else {
      onEvent({
        type: 'sample_stop',
        deps: {
          run: () => getNextArgs(dictationIndexRef.current),
          clear: () => setArgs([]),
        },
      });
    }
  }

  useEffect(() => {
    const { period } = dictationStateRef.current;
    if (dictationState.start) {
      onEvent({
        type: 'dictation_start',
        payload: { interval: period },
        deps: {
          run: () => getNextArgs(dictationIndexRef.current),
          clear: () => setArgs([]),
        },
      });
      dispatch({
        type: types.ON_PROGRESS,
      });
    }
  }, [dictationState.start]);

  useEffect(() => {
    if (dictationState.stop) {
      onEvent({
        type: 'dictation_stop',
      });
    }
  }, [dictationState.stop]);

  function handleStart(nextParams) {
    const {
      dictation: nextDictation,
      amount: nextAmount,
    } = dictationStateRef.current;
    fetchArgs(nextParams, nextDictation, nextAmount);
    if (nextDictation) {
      dispatch({
        type: types.ON_START,
      });
    }
  }

  const handleStop = () => {
    onStop();
  };

  // const key = 0;

  const iconmin = true;

  return (
    <>
      <PersonalPanel
        args={args}
        request={request}
        controls={controls}
        params={params}
        room={room}
        onStart={handleStart}
        iconmin={iconmin}
        setfocus={false}
        onStop={handleStop}
        onChangeInputValue={onChangeInputValue}
        onEnd={handleEnd}
        onBegin={handleBegin}
        onCheck={onCheck}
        onIncrement={onIncrement}
      >
        {render({
          state,
          room,
          dictationState,
          onShowParamsBlock,
          onToggleSetting,
          onChangeDictationSetting,
        })}
      </PersonalPanel>
    </>
  );
};

const mapStateToProps = () => ({});
// <Paper className={classes.paper}>
//   {dictationState.dictation && dictationState.progress && (
//     <LinearProgressCustom period={dictationState.period} />
//   )}

//   {((dictationState.dictation && dictationState.progress) ||
//     !dictationState.dictation) && (
//     <>
//       <RenderRoom room={room} />
//     </>
//   )}
//   <Sample
//     samples={dictationState.dictation ? dictationState.samples : [args]}
//   />
//   {showAnswers &&
//     dictationState.stop &&
//     dictationState.samples.map((sample, idx) => (
//       <div
//         key={(key += 1)}
//         style={{
//           fontSize: `${Math.round(10 / dictationState.amount)}vh`,
//           whiteSpace: 'nowrap',
//         }}
//       >
//         №{idx + 1}: {sample.reduce((acc, item) => acc + item, 0)}
//       </div>
//     ))}

//   {dictation &&
//     dictationState.stop &&
//     dictationState.samples.length >= 0 && (
//       <div style={{ fontSize: '5vh', whiteSpace: 'nowrap' }}>
//         Готово!
//       </div>
//     )}
//   {dictation ? (
//     <DictationTime
//       onChange={onChangeDictationSetting}
//       period={dictationState.period}
//       samples={dictationState.samples}
//     />
//   ) : (
//     <ControlPanel />
//   )}
//   <AnswerCounter
//     onShowParamsBlock={onShowParamsBlock}
//     visible={visible.answerCounter}
//     room={room}
//     onToggleSetting={onToggleSetting}
//   />
//   {abacus && (
//     <AbacusContainer
//       state={state.abacusState}
//       handleAbacusChange={onAbacusChange}
//     />
//   )}
// </Paper>

Dictation.propTypes = {
  request: PropTypes.object.isRequired,
  render: PropTypes.func.isRequired,
  room: PropTypes.string.isRequired,
  state: PropTypes.object.isRequired,
  onStop: PropTypes.func.isRequired,
  onCheck: PropTypes.func.isRequired,
  // onAbacusChange: PropTypes.func.isRequired,
  onShowParamsBlock: PropTypes.func.isRequired,
  getNextArgs: PropTypes.func.isRequired,
  args: PropTypes.array,
  onChangeInputValue: PropTypes.func,
  fetchArgs: PropTypes.func.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onToggleSetting: PropTypes.func.isRequired,
  onChangeDictationSetting: PropTypes.func.isRequired,
};

Dictation.defaultProps = {
  args: [],
  onChangeInputValue: null,
};

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Dictation);
