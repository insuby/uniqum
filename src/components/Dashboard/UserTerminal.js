import React, { useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { isReceiver } from 'modules/Simulators';
import PreDictation from 'components/Dashboard/PreDictation';

const types = {
  SET_STOP: 'SET_STOP',
  SET_VALUE: 'SET_VALUE',
  SET_START: 'SET_START',
  SET_PARAMS: 'SET_PARAMS',
  SET_CONTROLS: 'SET_CONTROLS',
  SET_ABACUS_STATE: 'SET_ABACUS_STATE',
  SET_DICTATION_STATE: 'SET_DICTATION_STATE',
};

const initialState = {
  params: {
    section: 5,
    steps: 3,
    interval: 1,
    mode: 1,
    sign: 0,
    bound: 0,
    under: 1,
    over: 1,
    numbers: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
  },
  controls: {
    start: false,
    stop: false,
    check: false,
    clear: false,
    abacus: false,
    settings: true,
    repeat: false,
    correct: false,
    open: false,
  },
  room: 'roomB',
  dictationState: {
    dictation: false,
    amount: 3,
    start: false,
    progress: false,
    stop: false,
    period: 5,
    samples: [],
  },
  visible: {
    answerCounter: {
      sound: false,
      settings: true,
      sample: true,
      play: true,
      params: true,
      abacus: true,
    },
  },
  abacusState: {
    a: [0, 0, 0, 0, 0],
    b: [0, 0, 0, 0, 0],
    c: [0, 0, 0, 0, 0],
    d: [0, 0, 0, 0, 0],
    e: [0, 0, 0, 0, 0],
  },
};

function reducer(state, action) {
  switch (action.type) {
    case types.SET_PARAMS:
    case types.SET_CONTROLS:
    case types.SET_ABACUS_STATE:
    case types.SET_DICTATION_STATE: {
      return {
        ...state,
        [action.meta]: action.payload,
      };
    }

    default:
      return state;
  }
}

const UserTerminal = ({ user, simulators, chatroom, socket }) => {
  const { params, controls, room, dictationState } = simulators;
  const [state, dispatch] = useReducer(reducer, {
    ...initialState,
    params,
    controls,
    dictationState,
    room,
  });

  const {
    control: { message, user: sender },
    position: { message: positionMessage, user: positionSender },
    dictation: { message: dictationMessage, user: dictationSender },
  } = socket;

  useEffect(() => {
    dispatch({
      type: types.SET_CONTROLS,
      meta: 'controls',
      payload: controls,
    });
  }, [controls]);

  useEffect(() => {
    dispatch({
      type: types.SET_PARAMS,
      meta: 'params',
      payload: params,
    });
  }, [params]);

  useEffect(() => {
    dispatch({
      type: types.SET_DICTATION_STATE,
      meta: 'dictationState',
      payload: dictationState,
    });
  }, [dictationState]);

  useEffect(() => {
    if (positionMessage && positionMessage.data && positionMessage.to) {
      if (isReceiver(user, positionSender, positionMessage.to)) {
        if (positionMessage.data.abacusState) {
          dispatch({
            type: types.SET_CONTROLS,
            meta: 'abacusState',
            payload: positionMessage.data.abacusState,
          });
        }
      }
    }

    if (dictationMessage && dictationMessage.data && dictationMessage.to) {
      if (isReceiver(user, dictationSender, dictationMessage.to)) {
        if (dictationMessage.data.dictationState) {
          dispatch({
            type: types.SET_CONTROLS,
            meta: 'dictationState',
            payload: dictationMessage.data.dictationState,
          });
        }
      }
    }

    if (message && message.data && message.to) {
      if (isReceiver(user, sender, message.to)) {
        if (message.data.controls) {
          dispatch({
            type: types.SET_CONTROLS,
            meta: 'controls',
            payload: message.data.controls,
          });
        }
        if (message.data.params) {
          dispatch({
            type: types.SET_PARAMS,
            meta: 'params',
            payload: message.data.params,
          });
        }
      }
    }
  }, [socket]);

  return (
    <>
      {user && <PreDictation chatroom={chatroom} user={user} state={state} />}
    </>
  );
};

UserTerminal.propTypes = {
  chatroom: PropTypes.object.isRequired,
  simulators: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  socket: state.socket,
  simulators: state.simulators,
});
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UserTerminal);
