/* eslint-disable indent */
const CreateColorProxy =
  process.env.NODE_ENV === 'development' && typeof Proxy !== 'undefined'
    ? (obj, colorName) =>
        new Proxy(obj, {
          get(target, name) {
            if (
              typeof name === 'number' &&
              !(name in target) &&
              process.env.NODE_ENV !== 'production'
            ) {
              console.error(
                `You are trying to get color ${colorName}[${name}] that doesn't exist`
              );
            }

            return target[name];
          },
        })
    : x => x;
/* eslint-enable indent */

export default CreateColorProxy;
