import { css } from 'styled-components';

export const screens = {
  tablet: 767,
  tabletHorizontal: 1023,
  desktop1200: 1199,
  desktop1280: 1279,
  desktop1366: 1365,
  desktop1440: 1439,
  desktop1680: 1679,
};

export const query = Object.keys(screens).reduce((acc, label) => {
  acc[label] = `(max-width: ${screens[label] / 16}em)`;
  return acc;
}, {});

const mq = Object.keys(screens).reduce((acc, label) => {
  acc[label] = (...args) =>
    css`
      @media ${query[label]} {
        ${css(...args)};
      }
    `;

  return acc;
}, {});

export default mq;
